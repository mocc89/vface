package viomi.com.viomiaiface.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.activity.FrigeControlActivity;
import viomi.com.viomiaiface.activity.ShowScreenActivity;
import viomi.com.viomiaiface.activity.WaterpuriX5Activity;
import viomi.com.viomiaiface.base.BaseFragment;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

public class WaterpuriFragment extends BaseFragment implements View.OnClickListener {
    final int REFRESH_PROPS = 122;
    final int REFRESH_TIME = 124;

    final int REQUEST_WATER = 125;

    final int DELAY_TIME = 3000;
    //[16,50]
    String[] mParams = {};//15-17
    String[] mParams1 = {"tds_out"};//15-17
    int[] mResults = {-1};
    @InjectView(R.id.tv_device_name)
    TextView tvDeviceName;
    @InjectView(R.id.tv_time)
    TextView tvTime;
    @InjectView(R.id.tv_big)
    TextView tvBig;
    @InjectView(R.id.tv_tds)
    TextView tvTds;
    @InjectView(R.id.tv_small)
    TextView tvSmall;

    @InjectView(R.id.layout_content)
    LinearLayout layoutContent;

    private void refreshView() {
//TDS=0~50               纯度高             可放心饮用
//• TDS=50~100           纯度较高           可放心饮用
//• TDS=100~300         水质一般           可饮用
//• TDS=300~600         会结水垢          不建议直饮
//• TDS=600~1000        口感较差          不建议直饮
//• TDS=1000以上         不适合饮用       不适合饮用
        if (mDevice == null)
            return;
        tvDeviceName.setText(mDevice.getName());
        if (mResults[0] < 0)
            return;
        tvTds.setText(mResults[0] + "");
        if (mResults[0] < 50) {
            tvSmall.setText(getString(R.string.water_drink0));
            tvBig.setText(getString(R.string.water_pure0));
        } else if (mResults[0] < 100) {
            tvSmall.setText(getString(R.string.water_drink1));
            tvBig.setText(getString(R.string.water_pure1));
        } else if (mResults[0] < 300) {
            tvSmall.setText(getString(R.string.water_drink2));
            tvBig.setText(getString(R.string.water_pure2));
        } else if (mResults[0] < 600) {
            tvSmall.setText(getString(R.string.water_drink3));
            tvBig.setText(getString(R.string.water_pure3));
        } else if (mResults[0] < 1000) {
            tvSmall.setText(getString(R.string.water_drink4));
            tvBig.setText(getString(R.string.water_pure4));
        } else {
            tvSmall.setText(getString(R.string.water_drink5));
            tvBig.setText(getString(R.string.water_pure5));
        }

    }


    boolean mIsVisibleToUser;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        mIsVisibleToUser = isVisibleToUser;
        if (isVisibleToUser) {
            startGetProps();
        } else {
            mHandler.removeCallbacksAndMessages(null);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        startGetProps();
    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacksAndMessages(null);
    }

    void startGetProps() {
        mHandler.removeCallbacksAndMessages(null);
        mHandler.sendEmptyMessage(REFRESH_TIME);
        mHandler.sendEmptyMessage(REFRESH_PROPS);
    }


    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        if (!mIsVisibleToUser)
            return;
        switch (msg.what) {
            case REFRESH_PROPS:
                if (mDevice == null) {
                    mHandler.sendEmptyMessageDelayed(REFRESH_PROPS, 500);
                } else
                    getProps();
                break;
            case REFRESH_TIME:
                if (mDevice != null)
                    tvDeviceName.setText(mDevice.getName());
                SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm");
                tvTime.setText(sdf1.format(new Date()));
                mHandler.sendEmptyMessageDelayed(REFRESH_TIME, 1000);
                break;
        }
    }

    private void getProps() {
        Operation.getInstance().sendPurifierCommand0(mDevice.getDeviceId(), Miconfig.MI_OAUTH_APP_ID + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                try {
                    JSONArray jsonArray = JsonUitls.getJSONArray(new JSONObject(result), "result");
                    if (jsonArray != null && jsonArray.length() > 0)
                        for (int i = 0; i < mResults.length; i++) {
                            mResults[i] = jsonArray.optInt(i, -1);
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        refreshView();
                                    } catch (Throwable e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mHandler.sendEmptyMessageDelayed(REFRESH_PROPS, DELAY_TIME);
            }

            @Override
            public void onFail(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                mHandler.sendEmptyMessageDelayed(REFRESH_PROPS, DELAY_TIME);
            }
        }, mParams1);
    }


    @Override
    protected View initView(LayoutInflater inflater) {
        View view = View.inflate(getActivity(), R.layout.activity_waterpuri, null);
        ButterKnife.inject(this, view);
        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm");
        tvTime.setText(sdf1.format(new Date()));
        refreshView();
        return view;
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void init() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.inject(this, rootView);
        layoutContent.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public void onClick(View v) {
        if (v == layoutContent) {
            if (mDevice == null)
                return;
            if (((ShowScreenActivity) getActivity()).vpScrollState != ViewPager.SCROLL_STATE_IDLE) {
                return;
            }
            Intent intent = new Intent(getActivity(), WaterpuriX5Activity.class);
            intent.putExtra("device", mDevice);
            startActivityForResult(intent, REQUEST_WATER);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK)
            if (requestCode == REQUEST_WATER) {
                mResults[0] = data.getIntExtra("tds", -1);
                refreshView();
            }
    }
}
