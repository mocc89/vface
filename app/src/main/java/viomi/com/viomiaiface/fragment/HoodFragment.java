package viomi.com.viomiaiface.fragment;

import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.miot.api.MiotManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseFragment;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

/**
 * Created by hailang on 2018/1/31 0031.
 */

public class HoodFragment extends BaseFragment {
    final int REFRESH_PROPS = 135;
    final int REFRESH_USERDATA = 133;
    final int REFRESH_TIME = 134;

    final int DELAY_TIME = 3000;
    @InjectView(R.id.tv_device_name)
    TextView tvDeviceName;
    @InjectView(R.id.tv_time)
    TextView tvTime;
    @InjectView(R.id.tv_runtime)
    TextView tvRuntime;
    @InjectView(R.id.tv_runcount)
    TextView tvRuncount;
    @InjectView(R.id.tv_switch)
    TextView tvSwitch;
    @InjectView(R.id.tv_mode)
    TextView tvMode;
    @InjectView(R.id.tv_connect_left)
    TextView tvConnectLeft;
    @InjectView(R.id.tv_connect_right)
    TextView tvConnectRight;
    @InjectView(R.id.tv_connect_light)
    TextView tvConnectLight;
    String[] mParams = {"power_state", "wind_state", "link_state", "light_state"};
    int[] mResults = new int[mParams.length + 2];
    @InjectView(R.id.iv_mode)
    ImageView ivMode;
    @InjectView(R.id.iv_connect_left)
    ImageView ivConnectLeft;
    @InjectView(R.id.iv_connect_right)
    ImageView ivConnectRight;
    @InjectView(R.id.iv_connect_light)
    ImageView ivConnectLight;

    {
        mResults[mResults.length - 2] = -1;
        mResults[mResults.length - 1] = -1;
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void init() {

    }


    void refreshView() {
        if (mDevice == null)
            return;
        tvDeviceName.setText(mDevice.getName());
        tvSwitch.setText(getString(R.string.hood_off));
        tvSwitch.setBackgroundDrawable(getResources().getDrawable(R.drawable.hood_swith_off));
        tvMode.setText(getString(R.string.hood_mode));
        ivMode.setImageResource(R.drawable.hood_wink_low_off);
        ivConnectLeft.setImageResource(R.drawable.hood_link_left_off);
        ivConnectRight.setImageResource(R.drawable.hood_link_right_off);
        ivConnectLight.setImageResource(R.drawable.hood_light_off);

        if (mResults[0] != 0) {
            tvSwitch.setText(getString(R.string.hood_on));
            tvSwitch.setBackgroundDrawable(getResources().getDrawable(R.drawable.hood_switch_on));
            if (mResults[1] == 1) {//抵挡
                tvMode.setText(getString(R.string.hood_mode_low));
                ivMode.setImageResource(R.drawable.hood_wink_low_on);
            } else if (mResults[1] == 16) {//高档
                tvMode.setText(getString(R.string.hood_mode_high));
                ivMode.setImageResource(R.drawable.hood_wink_high_on);
            } else if (mResults[1] == 4) {//爆炒
                tvMode.setText(getString(R.string.hood_mode_bao));
                ivMode.setImageResource(R.drawable.hood_wink_bao_on);
            }
        } else {
            if (mResults[1] == 1) {//抵挡
                ivMode.setImageResource(R.drawable.hood_wink_low_off);
                tvMode.setText(getString(R.string.hood_mode_low));
            } else if (mResults[1] == 16) {//高档
                tvMode.setText(getString(R.string.hood_mode_high));
                ivMode.setImageResource(R.drawable.hood_wink_high_off);
            } else if (mResults[1] == 4) {//爆炒
                tvMode.setText(getString(R.string.hood_mode_bao));
                ivMode.setImageResource(R.drawable.hood_wink_bao_off);
            }
        }

        if (mResults[2] == 2) {
            ivConnectLeft.setImageResource(R.drawable.hood_link_left_on);
            ivConnectRight.setImageResource(R.drawable.hood_link_right_on);
        }
        if (mResults[3] == 1)
            ivConnectLight.setImageResource(R.drawable.hood_light_on);
        if (mResults[mResults.length - 2] > -1)
            tvRuncount.setText(mResults[mResults.length - 2] + "");
        if (mResults[mResults.length - 1] > -1)
            tvRuntime.setText(mResults[mResults.length - 1] + "");
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        if (!mIsVisibleToUser)
            return;
        switch (msg.what) {
            case REFRESH_PROPS:
                if (mDevice == null) {
                    mHandler.sendEmptyMessageDelayed(REFRESH_PROPS, 500);
                } else
                    getProps();
                break;
            case REFRESH_USERDATA:
                if (mDevice == null) {
                    mHandler.sendEmptyMessageDelayed(REFRESH_USERDATA, 1000);
                } else
                    getUserData();
                break;
            case REFRESH_TIME:
                if (mDevice != null)
                    tvDeviceName.setText(mDevice.getName());
                SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm");
                tvTime.setText(sdf1.format(new Date()));
                mHandler.sendEmptyMessageDelayed(REFRESH_TIME, 1000);
                break;
        }
    }

    void getProps() {
        Operation.getInstance().sendCookerHoodCommand0(mDevice.getDeviceId(), Miconfig.MI_OAUTH_APP_ID + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                try {
                    JSONArray jsonArray = JsonUitls.getJSONArray(new JSONObject(result), "result");
                    if (jsonArray != null && jsonArray.length() > 0)
                        for (int i = 0; i < mResults.length - 2; i++) {
                            mResults[i] = jsonArray.optInt(i);
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        refreshView();
                                    } catch (Throwable e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mHandler.sendEmptyMessageDelayed(REFRESH_PROPS, DELAY_TIME);
            }

            @Override
            public void onFail(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                mHandler.sendEmptyMessageDelayed(REFRESH_PROPS, DELAY_TIME);
            }
        }, mParams);

    }


    void getUserData() {
        Operation.getInstance().sendCookerHoodGetUserData(mDevice.getDeviceId(), Miconfig.MI_OAUTH_APP_ID + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result1) {
                LogUtils.d(TAG, "onresult:" + result1);
                try {
                    JSONArray result = new JSONObject(result1).optJSONArray("result");
                    if (result == null || result.length() == 0)
                        return;
                    try {
                        JSONObject object = (JSONObject) result.get(0);
                        if (object != null) {
                            String value = (String) object.get("value");
                            if (value != null) {
                                String[] values = value.replace("[", "").replace("]", "").split(",");
                                if (values != null && values.length > 1) {
                                    final int run_count = Integer.parseInt(values[0]);
                                    final int run_time_total = Integer.parseInt(values[1]);
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mResults[mResults.length - 2] = run_count;
                                            mResults[mResults.length - 1] = run_time_total;
                                            refreshView();
                                        }
                                    });
                                }
                            }
                        }

                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                mHandler.sendEmptyMessageDelayed(REFRESH_USERDATA, DELAY_TIME);
            }

            @Override
            public void onFail(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                mHandler.sendEmptyMessageDelayed(REFRESH_USERDATA, DELAY_TIME);
            }
        });

    }


    boolean mIsVisibleToUser;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        mIsVisibleToUser = isVisibleToUser;
        if (isVisibleToUser) {
            startGetProps();
        } else {
            mHandler.removeCallbacksAndMessages(null);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        startGetProps();
    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacksAndMessages(null);
    }

    void startGetProps() {
        mHandler.removeCallbacksAndMessages(null);
        mHandler.sendEmptyMessage(REFRESH_TIME);
        mHandler.sendEmptyMessage(REFRESH_PROPS);
        mHandler.sendEmptyMessage(REFRESH_USERDATA);
    }


    @Override
    protected View initView(LayoutInflater inflater) {
        View view = View.inflate(getActivity(), R.layout.activity_hood, null);
        ButterKnife.inject(this, view);
        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm");
        tvTime.setText(sdf1.format(new Date()));
        refreshView();
        return view;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.inject(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }
}
