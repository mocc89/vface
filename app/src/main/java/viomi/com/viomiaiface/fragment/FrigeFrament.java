package viomi.com.viomiaiface.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.miot.api.MiotManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.activity.FrigeControlActivity;
import viomi.com.viomiaiface.activity.ShowScreenActivity;
import viomi.com.viomiaiface.base.BaseFragment;
import viomi.com.viomiaiface.config.DeviceModels;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.utils.FrigeUtil;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

public class FrigeFrament extends BaseFragment implements View.OnClickListener {
    final int REFRESH_PROPS = 112;
    final int REFRESH_USERDATA = 113;
    final int REFRESH_TIME = 114;

    final int REQUEST_FRIGE = 115;

    final int DELAY_TIME = 3000;
    @InjectView(R.id.tv_device_name)
    TextView tvDeviceName;
    @InjectView(R.id.tv_time)
    TextView tvTime;
    @InjectView(R.id.tv_cang)
    TextView tvCang;
    @InjectView(R.id.tv_bian)
    TextView tvBian;
    @InjectView(R.id.tv_dong)
    TextView tvDong;

    String[] mParams = {"RCSetTemp", "CCSetTemp", "FCSetTemp"};

    String[] mResults = new String[mParams.length];
    @InjectView(R.id.layout_content)
    LinearLayout layoutContent;
    @InjectView(R.id.v_weight_21)
    View vWeight21;
    @InjectView(R.id.layout_2)
    LinearLayout layout2;
    @InjectView(R.id.v_weight_22)
    View vWeight22;
    @InjectView(R.id.v_divider2)
    View vDivider2;


    @Override
    protected View initView(LayoutInflater inflater) {
        View view = View.inflate(getActivity(), R.layout.activity_frige2, null);
        ButterKnife.inject(this, view);
        layoutContent.setOnClickListener(this);
        if (mDevice != null)
            tvDeviceName.setText(mDevice.getName());
        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm");
        tvTime.setText(sdf1.format(new Date()));
        refreshView();
//        tvDong.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((ShowScreenActivity)getActivity()).mService.voiceFilter("烟机灯光打开");
//            }
//        });
        return view;
    }

    /**
     * 是否3仓，包含变温室
     */
    boolean cang3 = true;

    private void refreshView() {
        if (mDevice == null)
            return;
        tvDeviceName.setText(mDevice.getName());
        cang3 = FrigeUtil.containBianwen(mDevice.getDeviceModel());
        if (cang3) {
            vDivider2.setVisibility(View.VISIBLE);
            vWeight21.setVisibility(View.VISIBLE);
            vWeight22.setVisibility(View.VISIBLE);
            layout2.setVisibility(View.VISIBLE);
        } else {
            vDivider2.setVisibility(View.GONE);
            vWeight21.setVisibility(View.GONE);
            vWeight22.setVisibility(View.GONE);
            layout2.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(mResults[0]))
            return;
        tvCang.setText(mResults[0]);
        tvBian.setText(mResults[1]);
        tvDong.setText(mResults[2]);
    }


    @Override
    protected void initListener() {
    }

    @Override
    protected void init() {

    }


    boolean mIsVisibleToUser;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        mIsVisibleToUser = isVisibleToUser;

        if (isVisibleToUser) {
            startGetProps();
        } else {
            mHandler.removeCallbacksAndMessages(null);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        startGetProps();
    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacksAndMessages(null);
    }


    void startGetProps() {
        mHandler.removeCallbacksAndMessages(null);
        mHandler.sendEmptyMessage(REFRESH_TIME);
        mHandler.sendEmptyMessage(REFRESH_PROPS);
    }


    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        if (!mIsVisibleToUser)
            return;
        switch (msg.what) {
            case REFRESH_PROPS:
                if (mDevice == null) {
                    mHandler.sendEmptyMessageDelayed(REFRESH_PROPS, 500);
                } else
                    getProps();
                break;
            case REFRESH_USERDATA:
                break;
            case REFRESH_TIME:
                if (mDevice != null)
                    tvDeviceName.setText(mDevice.getName());
                SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm");
                tvTime.setText(sdf1.format(new Date()));
                mHandler.sendEmptyMessageDelayed(REFRESH_TIME, 1000);
                break;
        }
    }


    private void getProps() {
        Operation.getInstance().sendFridgeCommand0(mDevice.getDeviceId(), Miconfig.MI_OAUTH_APP_ID + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                try {
                    JSONArray jsonArray = JsonUitls.getJSONArray(new JSONObject(result), "result");
                    if (jsonArray != null && jsonArray.length() > 0)
                        for (int i = 0; i < mResults.length; i++) {
                            mResults[i] = jsonArray.optString(i);
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        refreshView();
                                    } catch (Throwable e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mHandler.sendEmptyMessageDelayed(REFRESH_PROPS, DELAY_TIME);
            }

            @Override
            public void onFail(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                mHandler.sendEmptyMessageDelayed(REFRESH_PROPS, DELAY_TIME);
            }
        }, mParams);
    }


    @Override
    public void onClick(View v) {
        if (v == layoutContent) {
            if (mDevice == null)
                return;
            if (((ShowScreenActivity) getActivity()).vpScrollState != ViewPager.SCROLL_STATE_IDLE) {
                return;
            }
            Intent intent = new Intent(getActivity(), FrigeControlActivity.class);
            intent.putExtra("device", mDevice);
            startActivityForResult(intent, REQUEST_FRIGE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK)
            if (requestCode == REQUEST_FRIGE) {
                mResults = data.getStringArrayExtra("result");
                refreshView();
            }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.inject(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }
}
