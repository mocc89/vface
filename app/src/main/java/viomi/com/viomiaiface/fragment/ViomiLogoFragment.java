package viomi.com.viomiaiface.fragment;


import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViomiLogoFragment extends BaseFragment {

    @Override
    protected View initView(LayoutInflater inflater) {

        View view = inflater.inflate(R.layout.fragment_viomi_logo, null);
        return view;
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void init() {

    }

}
