package viomi.com.viomiaiface.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseFragment;
import viomi.com.viomiaiface.config.BroadcastAction;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.config.MURL;
import viomi.com.viomiaiface.model.WeatherBean;
import viomi.com.viomiaiface.utils.HttpApi;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiaiface.utils.SharedPreferencesUtil;
import viomi.com.viomiaiface.utils.WeatherIconUtil;


public class WeatherFragment extends BaseFragment {

    private ImageView weather_icon;
    private TextView weather_temp;
    private TextView weather_txt1;
    private TextView weather_txt2;
    private WeatherBean bean;
    private BroadcastReceiver cityReceiver;

    @Override
    protected View initView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.fragment_weather, null);
        weather_icon = view.findViewById(R.id.weather_icon);
        weather_temp = view.findViewById(R.id.weather_temp);
        weather_txt1 = view.findViewById(R.id.weather_txt1);
        weather_txt2 = view.findViewById(R.id.weather_txt2);
        return view;
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void init() {
        updateWeather();
        cityReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (BroadcastAction.WEATHERCITY.equals(intent.getAction())) {
                    LogUtils.e(TAG, "cityReceiver");
                    mHandler.removeCallbacksAndMessages(null);
                    updateWeather();
                }
            }
        };
        IntentFilter filter = new IntentFilter(BroadcastAction.WEATHERCITY);
        getActivity().registerReceiver(cityReceiver, filter);
    }

    private void updateWeather() {
        String cityName = SharedPreferencesUtil.getWeatherCity();
        LogUtils.e(TAG, cityName + "");
        getWeather(cityName);
    }

    private void getWeather(String cityName) {
        Map<String, String> map = new HashMap<>();
        map.put("key", "185c9117f8138");
        map.put("city", cityName);
        HttpApi.getRequestHandler(MURL.MOB_WEATHER, map, mHandler, HandlerMsgWhat.MSG0, HandlerMsgWhat.MSG1);
    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case HandlerMsgWhat.MSG0: {
                showResult(msg, true);
                //获取成功后四小时更新
                mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG2, 4 * 60 * 60 * 1000);
                break;
            }
            case HandlerMsgWhat.MSG1: {
                showResult(msg, false);
                //获取失败后15秒后重复请求
                mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG2, 15 * 1000);
                break;
            }

            case HandlerMsgWhat.MSG2: {
                updateWeather();
                break;
            }

            default:
                break;
        }
    }

    private void showResult(Message msg, boolean isSuccess) {
        if (isSuccess) {
            String result = (String) msg.obj;
            JSONObject jsonObject = JsonUitls.getJSONObject(result);
            JSONArray resultArray = JsonUitls.getJSONArray(jsonObject, "result");
            if (resultArray.length() > 0) {
                JSONObject item = JsonUitls.getJSONObject(resultArray, 0);
                String date = JsonUitls.getString(item, "date");
                String distrct = JsonUitls.getString(item, "distrct");
                String pm25 = JsonUitls.getString(item, "pollutionIndex");
                String temperature = JsonUitls.getString(item, "temperature");
                temperature = temperature.replace("℃", "");
                JSONArray future = JsonUitls.getJSONArray(item, "future");
                if (future.length() > 0) {
                    JSONObject future_item = JsonUitls.getJSONObject(future, 0);
                    String weather_dayTime = JsonUitls.getString(future_item, "dayTime");
                    String weather_night = JsonUitls.getString(future_item, "night");
                    String weather = "".equals(weather_dayTime) ? weather_night : weather_dayTime;
                    bean = new WeatherBean(date, distrct, pm25, temperature, weather);
                    setView();
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        LogUtils.e(TAG, "onResume");
        setView();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void setView() {
        if (bean != null) {
            weather_temp.setText(bean.getTemperature());
            weather_txt1.setText(bean.getDistrct() + "   " + bean.getWeather_dayTime() + "   PM2.5: " + bean.getPm25());
            weather_txt2.setText(bean.getDate());
            WeatherIconUtil.setWeatherIcon(weather_icon, bean.getWeather_dayTime());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(cityReceiver);
    }
}
