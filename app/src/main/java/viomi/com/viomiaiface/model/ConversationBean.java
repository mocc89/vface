package viomi.com.viomiaiface.model;

/**
 * Created by Mocc on 2018/1/26
 */

public class ConversationBean {
    private String text;
    private ConversationType type;
    private Object imgSrc;
    private int puri_temp;

    public void setPuri_temp(int puri_temp) {
        this.puri_temp = puri_temp;
    }

    public int getPuri_temp() {
        return puri_temp;
    }


    public ConversationBean() {
    }

    public ConversationBean(String text, ConversationType type, Object imgSrc) {
        this.text = text;
        this.type = type;
        this.imgSrc = imgSrc;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ConversationType getType() {
        return type;
    }

    public void setType(ConversationType type) {
        this.type = type;
    }

    public Object getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(Object imgSrc) {
        this.imgSrc = imgSrc;
    }
}
