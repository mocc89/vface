package viomi.com.viomiaiface.model;

/**
 * Created by Mocc on 2018/1/26
 */

public enum ConversationType {
    user_txt, robot_txt, robot_img, puri_temp
}
