package viomi.com.viomiaiface.model;

/**
 * Created by Mocc on 2018/1/29
 */

public class WeatherBean {

    private String date;
    private String distrct;
    private String pm25;
    private String temperature;
    private String weather_dayTime;

    public WeatherBean() {
    }

    public WeatherBean(String date, String distrct, String pm25, String temperature, String weather_dayTime) {
        this.date = date;
        this.distrct = distrct;
        this.pm25 = pm25;
        this.temperature = temperature;
        this.weather_dayTime = weather_dayTime;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDistrct() {
        return distrct;
    }

    public void setDistrct(String distrct) {
        this.distrct = distrct;
    }

    public String getPm25() {
        return pm25;
    }

    public void setPm25(String pm25) {
        this.pm25 = pm25;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getWeather_dayTime() {
        return weather_dayTime;
    }

    public void setWeather_dayTime(String weather_dayTime) {
        this.weather_dayTime = weather_dayTime;
    }
}
