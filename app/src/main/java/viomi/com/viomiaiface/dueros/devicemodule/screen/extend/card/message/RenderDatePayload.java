package viomi.com.viomiaiface.dueros.devicemodule.screen.extend.card.message;


import java.io.Serializable;

import viomi.com.viomiaiface.dueros.framework.message.Payload;

public class RenderDatePayload extends Payload implements Serializable {
    public String datetime;
    public String timeZoneName;
    public String day;
}
