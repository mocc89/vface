/*
 * Copyright (c) 2017 Baidu, Inc. All Rights Reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package viomi.com.viomiaiface.dueros.androidapp;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.dueros.api.BaiduDialog;
import viomi.com.viomiaiface.dueros.api.BaiduDialogError;
import viomi.com.viomiaiface.dueros.api.BaiduException;
import viomi.com.viomiaiface.dueros.api.BaiduOauthImplicitGrant;
import viomi.com.viomiaiface.dueros.util.LogUtil;
import viomi.com.viomiaiface.service.VoiceService;

/**
 * 用户认证界面
 * <p>
 * Created by zhangyan42@baidu.com on 2017/5/18.
 */
public class DcsSampleOAuthActivity extends DcsSampleBaseActivity implements View.OnClickListener {
    // 需要开发者自己申请client_id
    // client_id，就是oauth的client_id
    private static final String CLIENT_ID = "48KYKRkNnoViR7bZIcH1VzvusGnfyLvU";
    // 是否每次授权都强制登陆
    private boolean isForceLogin = false;
    // 是否每次都确认登陆
    private boolean isConfirmLogin = false;
    private EditText editTextClientId;
    private Button oauthLoginButton;
    private BaiduOauthImplicitGrant baiduOauthImplicitGrant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dcs_sample_activity_oauth);
        initMyView();
        setOnClickListener();

        Log.e("aaaaaaaaaaaaaaaaa", "DcsSampleOAuthActivity-onCreate");
    }

    private void setOnClickListener() {
        oauthLoginButton.setOnClickListener(this);
    }

    private void initMyView() {
        editTextClientId = (EditText) findViewById(R.id.edit_client_id);
        oauthLoginButton = (Button) findViewById(R.id.btn_login);

        editTextClientId.setText(CLIENT_ID);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                String clientId = editTextClientId.getText().toString();
                if (!TextUtils.isEmpty(clientId) && !TextUtils.isEmpty(clientId)) {
                    baiduOauthImplicitGrant = new BaiduOauthImplicitGrant(clientId, DcsSampleOAuthActivity.this.getApplication());
                    baiduOauthImplicitGrant.authorize(DcsSampleOAuthActivity.this, isForceLogin, isConfirmLogin, new BaiduDialog
                            .BaiduDialogListener() {
                        @Override
                        public void onComplete(Bundle values) {
                            Toast.makeText(DcsSampleOAuthActivity.this.getApplicationContext(),
                                    getResources().getString(R.string.login_succeed),
                                    Toast.LENGTH_SHORT).show();
                            startMainActivity();
                        }

                        @Override
                        public void onBaiduException(BaiduException e) {

                        }

                        @Override
                        public void onError(BaiduDialogError e) {
                            if (null != e) {
                                String toastString = TextUtils.isEmpty(e.getMessage())
                                        ? DcsSampleOAuthActivity.this.getResources()
                                        .getString(R.string.err_net_msg) : e.getMessage();
                                Toast.makeText(DcsSampleOAuthActivity.this.getApplicationContext(), toastString,
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancel() {
                            LogUtil.d("cancle", "I am back");
                        }
                    });
                } else {
                    Toast.makeText(DcsSampleOAuthActivity.this.getApplicationContext(),
                            getResources().getString(R.string.client_id_empty),
                            Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    private void startMainActivity() {
//        Intent intent = new Intent(DcsSampleOAuthActivity.this, DcsSampleMainActivity.class);
//        intent.putExtra("baidu", baiduOauthImplicitGrant);
//        startActivity(intent);
        Intent intent = new Intent(this, VoiceService.class);
        startService(intent);
        finish();
    }
}