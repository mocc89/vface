package viomi.com.viomiaiface.dueros.devicemodule.screen.message;


import java.io.Serializable;
import java.util.List;

import viomi.com.viomiaiface.dueros.framework.message.Payload;

public class RenderHintPayload extends Payload implements Serializable {
    public List<String> cueWords;
}
