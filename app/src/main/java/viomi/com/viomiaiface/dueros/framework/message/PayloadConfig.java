/*
 * Copyright (c) 2017 Baidu, Inc. All Rights Reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package viomi.com.viomiaiface.dueros.framework.message;


import java.util.HashMap;

import viomi.com.viomiaiface.dueros.devicemodule.alerts.message.DeleteAlertPayload;
import viomi.com.viomiaiface.dueros.devicemodule.alerts.message.SetAlertPayload;
import viomi.com.viomiaiface.dueros.devicemodule.audioplayer.message.ClearQueuePayload;
import viomi.com.viomiaiface.dueros.devicemodule.audioplayer.message.PlayPayload;
import viomi.com.viomiaiface.dueros.devicemodule.audioplayer.message.StopPayload;
import viomi.com.viomiaiface.dueros.devicemodule.screen.message.HtmlPayload;
import viomi.com.viomiaiface.dueros.devicemodule.screen.message.RenderVoiceInputTextPayload;
import viomi.com.viomiaiface.dueros.devicemodule.speakcontroller.message.AdjustVolumePayload;
import viomi.com.viomiaiface.dueros.devicemodule.speakcontroller.message.SetMutePayload;
import viomi.com.viomiaiface.dueros.devicemodule.speakcontroller.message.SetVolumePayload;
import viomi.com.viomiaiface.dueros.devicemodule.system.message.SetEndPointPayload;
import viomi.com.viomiaiface.dueros.devicemodule.system.message.ThrowExceptionPayload;
import viomi.com.viomiaiface.dueros.devicemodule.voiceinput.message.ListenPayload;
import viomi.com.viomiaiface.dueros.devicemodule.voiceoutput.message.SpeakPayload;

/**
 * 利用namespace和name做为key存储不同payload子类class并且find
 * <p>
 * Created by wuruisheng on 2017/6/1.
 */
public class PayloadConfig {
    private final HashMap<String, Class<?>> payloadClass;

    private static class PayloadConfigHolder {
        private static final PayloadConfig instance = new PayloadConfig();
    }

    public static PayloadConfig getInstance() {
        return PayloadConfigHolder.instance;
    }

    private PayloadConfig() {
        payloadClass = new HashMap<>();

        // AudioInputImpl
        String namespace = viomi.com.viomiaiface.dueros.devicemodule.voiceinput.ApiConstants.NAMESPACE;
        String name = viomi.com.viomiaiface.dueros.devicemodule.voiceinput.ApiConstants.Directives.Listen.NAME;
        insertPayload(namespace, name, ListenPayload.class);

        // VoiceOutput
        namespace = viomi.com.viomiaiface.dueros.devicemodule.voiceoutput.ApiConstants.NAMESPACE;
        name = viomi.com.viomiaiface.dueros.devicemodule.voiceoutput.ApiConstants.Directives.Speak.NAME;
        insertPayload(namespace, name, SpeakPayload.class);

        // audio
        namespace = viomi.com.viomiaiface.dueros.devicemodule.audioplayer.ApiConstants.NAMESPACE;
        name = viomi.com.viomiaiface.dueros.devicemodule.audioplayer.ApiConstants.Directives.Play.NAME;
        insertPayload(namespace, name, PlayPayload.class);

        name = viomi.com.viomiaiface.dueros.devicemodule.audioplayer.ApiConstants.Directives.Stop.NAME;
        insertPayload(namespace, name, StopPayload.class);

        name = viomi.com.viomiaiface.dueros.devicemodule.audioplayer.ApiConstants.Directives.ClearQueue.NAME;
        insertPayload(namespace, name, ClearQueuePayload.class);

        //  alert
        namespace = viomi.com.viomiaiface.dueros.devicemodule.alerts.ApiConstants.NAMESPACE;
        name = viomi.com.viomiaiface.dueros.devicemodule.alerts.ApiConstants.Directives.SetAlert.NAME;
        insertPayload(namespace, name, SetAlertPayload.class);

        name = viomi.com.viomiaiface.dueros.devicemodule.alerts.ApiConstants.Directives.DeleteAlert.NAME;
        insertPayload(namespace, name, DeleteAlertPayload.class);

        // SpeakController
        namespace = viomi.com.viomiaiface.dueros.devicemodule.speakcontroller.ApiConstants.NAMESPACE;
        name = viomi.com.viomiaiface.dueros.devicemodule.speakcontroller.ApiConstants.Directives.SetVolume.NAME;
        insertPayload(namespace, name, SetVolumePayload.class);

        name = viomi.com.viomiaiface.dueros.devicemodule.speakcontroller.ApiConstants.Directives.AdjustVolume.NAME;
        insertPayload(namespace, name, AdjustVolumePayload.class);

        name = viomi.com.viomiaiface.dueros.devicemodule.speakcontroller.ApiConstants.Directives.SetMute.NAME;
        insertPayload(namespace, name, SetMutePayload.class);

        // System
        namespace = viomi.com.viomiaiface.dueros.devicemodule.system.ApiConstants.NAMESPACE;
        name = viomi.com.viomiaiface.dueros.devicemodule.system.ApiConstants.Directives.SetEndpoint.NAME;
        insertPayload(namespace, name, SetEndPointPayload.class);

        name = viomi.com.viomiaiface.dueros.devicemodule.system.ApiConstants.Directives.ThrowException.NAME;
        insertPayload(namespace, name, ThrowExceptionPayload.class);

        // Screen
        namespace = viomi.com.viomiaiface.dueros.devicemodule.screen.ApiConstants.NAMESPACE;
        name = viomi.com.viomiaiface.dueros.devicemodule.screen.ApiConstants.Directives.HtmlView.NAME;
        insertPayload(namespace, name, HtmlPayload.class);
        name = viomi.com.viomiaiface.dueros.devicemodule.screen.ApiConstants.Directives.RenderVoiceInputText.NAME;
        insertPayload(namespace, name, RenderVoiceInputTextPayload.class);
    }

    void insertPayload(String namespace, String name, Class<?> type) {
        final String key = namespace + name;
        payloadClass.put(key, type);
    }

    Class<?> findPayloadClass(String namespace, String name) {
        final String key = namespace + name;
        return payloadClass.get(key);
    }
}