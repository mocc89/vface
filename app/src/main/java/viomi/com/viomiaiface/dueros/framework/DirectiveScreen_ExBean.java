package viomi.com.viomiaiface.dueros.framework;

import viomi.com.viomiaiface.dueros.framework.message.Directive;

/**
 * Created by Mocc on 2018/1/31
 */

public class DirectiveScreen_ExBean {

    private Directive RenderPlayerInfo;//媒体播放
    private Directive RenderAudioList;//播放列表
    private Directive RenderStock;//股票
    private Directive Renderdefault;

    public DirectiveScreen_ExBean() {
    }

    public Directive getRenderPlayerInfo() {
        return RenderPlayerInfo;
    }

    public Directive getRenderAudioList() {
        return RenderAudioList;
    }

    public Directive getRenderStock() {
        return RenderStock;
    }

    public Directive getRenderdefault() {
        return Renderdefault;
    }

    public void setDirective(Directive directive) {

        String name = directive.header.getName();

        switch (name) {
            //语音输入
            case "RenderPlayerInfo":
                this.RenderPlayerInfo = directive;
                break;

            //语音输出
            case "RenderAudioList":
                this.RenderAudioList = directive;
                break;

            //扬声器控制
            case "RenderStock":
                this.RenderStock = directive;
                break;
            default:
                this.Renderdefault = directive;
                break;
        }
    }

}
