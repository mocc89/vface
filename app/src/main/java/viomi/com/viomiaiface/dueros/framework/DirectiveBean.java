package viomi.com.viomiaiface.dueros.framework;

import viomi.com.viomiaiface.dueros.framework.message.Directive;

/**
 * Created by Mocc on 2018/1/24
 */

public class DirectiveBean {

    private Directive voice_input;
    private Directive voice_output;
    private Directive speaker_controller;
    private Directive audio_player;
    private Directive playback_controller;
    private Directive alerts;
    private Directive screen;
    private Directive system;
    private DirectiveScreen_ExBean screen_extended_card;

    public DirectiveBean() {
    }

    public Directive getVoice_input() {
        return voice_input;
    }

    public Directive getVoice_output() {
        return voice_output;
    }

    public Directive getSpeaker_controller() {
        return speaker_controller;
    }

    public Directive getAudio_player() {
        return audio_player;
    }

    public Directive getPlayback_controller() {
        return playback_controller;
    }

    public Directive getAlerts() {
        return alerts;
    }

    public Directive getScreen() {
        return screen;
    }

    public Directive getSystem() {
        return system;
    }

    public DirectiveScreen_ExBean getScreen_extended_card() {
        return screen_extended_card;
    }

    public void setDirective(Directive directive) {

        String namespace = directive.header.getNamespace();

        switch (namespace) {
            //语音输入
            case "ai.dueros.device_interface.voice_input":
                this.voice_input = directive;
                break;

            //语音输出
            case "ai.dueros.device_interface.voice_output":
                this.voice_output = directive;
                break;

            //扬声器控制
            case "ai.dueros.device_interface.speaker_controller":
                this.speaker_controller = directive;
                break;

            //音频播放器
            case "ai.dueros.device_interface.audio_player":
                this.audio_player = directive;
                break;

            //播放控制
            case "ai.dueros.device_interface.playback_controller":
                this.playback_controller = directive;
                break;

            //闹钟
            case "ai.dueros.device_interface.alerts":
                this.alerts = directive;
                break;

            //屏幕展示
            case "ai.dueros.device_interface.screen":
                this.screen = directive;
                break;

            //系统
            case "ai.dueros.device_interface.system":
                this.system = directive;
                break;

            //扩展内容
            case "ai.dueros.device_interface.screen_extended_card":
                if (screen_extended_card == null) {
                    screen_extended_card = new DirectiveScreen_ExBean();
                }
                screen_extended_card.setDirective(directive);
                break;

            default:
                break;
        }
    }

}
