package viomi.com.viomiaiface.dueros.devicemodule.screen.extend.card.message;


import java.io.Serializable;

import viomi.com.viomiaiface.dueros.framework.message.Payload;

public class RenderAirQualityPayload extends Payload implements Serializable {
    public String city;
    public String currentTemperature;
    public String pm25;
    public String airQuality;
    public String day;
    public String date;
    public String dateDescription;
    public String tips;
}
