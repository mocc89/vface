package viomi.com.viomiaiface.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.model.ConversationBean;
import viomi.com.viomiaiface.model.ConversationType;

/**
 * Created by Mocc on 2018/1/26
 */

public class ConversationAdapter extends BaseAdapter {

    private List<ConversationBean> list;
    private Context context;
    private LayoutInflater inflater;

    public ConversationAdapter(List<ConversationBean> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return ConversationType.values().length;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position).getType().ordinal();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ConversationBean conversationBean = list.get(position);

        ViewHolder1 holder1 = null;
        ViewHolder2 holder2 = null;
        ViewHolder3 holder3 = null;
        ViewHolder4 holder4 = null;

        if (convertView == null) {
            switch (conversationBean.getType()) {

                case user_txt:
                    convertView = inflater.inflate(R.layout.conversation_layout_item1, null);
                    holder1 = new ViewHolder1();
                    holder1.txt = convertView.findViewById(R.id.txt);
                    convertView.setTag(holder1);
                    break;

                case robot_txt:
                    convertView = inflater.inflate(R.layout.conversation_layout_item2, null);
                    holder2 = new ViewHolder2();
                    holder2.txt = convertView.findViewById(R.id.txt);
                    convertView.setTag(holder2);
                    break;

                case robot_img:
                    convertView = inflater.inflate(R.layout.conversation_layout_item3, null);
                    holder3 = new ViewHolder3();
                    holder3.img = convertView.findViewById(R.id.img);
                    convertView.setTag(holder3);
                    break;
                case puri_temp:
                    convertView = inflater.inflate(R.layout.conversation_layout_item4, null);
                    holder4 = new ViewHolder4();
                    holder4.txt = convertView.findViewById(R.id.txt);
                    holder4.textView = convertView.findViewById(R.id.tv_dong_indicator);
                    holder4.seekBar = convertView.findViewById(R.id.sb_dong);
                    holder4.seekBar.setEnabled(false);
                    convertView.setTag(holder4);
                    break;

                default:
                    break;
            }
        } else {
            switch (conversationBean.getType()) {

                case user_txt:
                    holder1 = (ViewHolder1) convertView.getTag();
                    break;

                case robot_txt:
                    holder2 = (ViewHolder2) convertView.getTag();
                    break;

                case robot_img:
                    holder3 = (ViewHolder3) convertView.getTag();
                    break;
                case puri_temp:
                    holder4 = (ViewHolder4) convertView.getTag();
                    break;

                default:
                    break;
            }
        }


        switch (conversationBean.getType()) {
            case user_txt:
                holder1.txt.setText(conversationBean.getText());
                break;

            case robot_txt:
                holder2.txt.setText(conversationBean.getText());
                break;

            case robot_img:
                Glide.with(context).load(conversationBean.getImgSrc()).into(holder3.img);
                break;
            case puri_temp:
                int temp = conversationBean.getPuri_temp();
                holder4.txt.setText(conversationBean.getText());
                holder4.textView.setText(temp + "℃");
                final int max = 50;
                final int process = temp - 40;
                holder4.seekBar.setMax(max);
                holder4.seekBar.setProgress(process);
                final SeekBar seekBar = holder4.seekBar;
                final TextView textView = holder4.textView;
                holder4.textView.post(new Runnable() {
                    @Override
                    public void run() {
                        setIndicator(seekBar, textView, process, max);
                    }
                });

                break;

            default:
                break;
        }

        return convertView;
    }

    void setIndicator(SeekBar seekBar, TextView textView, int process, int max) {
        int margin = (int) ((seekBar.getWidth() - seekBar.getPaddingRight() * 2) * process / (max * 1f))
                + seekBar.getPaddingLeft()
                - textView.getWidth() / 2;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) textView.getLayoutParams();
        params.leftMargin = margin;
        textView.setLayoutParams(params);
    }

    static class ViewHolder1 {
        TextView txt;
    }

    static class ViewHolder2 {
        TextView txt;
    }

    static class ViewHolder3 {
        ImageView img;
    }

    static class ViewHolder4 {
        TextView txt;
        TextView textView;
        SeekBar seekBar;
    }
}
