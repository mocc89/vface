package viomi.com.viomiaiface.callback;

import viomi.com.viomiaiface.dueros.framework.message.Directive;

/**
 * Created by Mocc on 2018/1/30
 */

public interface DcsEventCallback {

    void onCallback(Directive directive);
}
