package viomi.com.viomiaiface.callback;

/**
 * Created by Mocc on 2018/2/7
 */

public interface VoiceSendOrderCallback {
    void onSuccess(String result);
    void onFail(String result);
}
