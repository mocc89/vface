package viomi.com.viomiaiface.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;

public class WelcomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
                Intent intent = new Intent(WelcomeActivity.this, ShowScreenActivity.class);
                startActivity(intent);
                finish();
            }
        }, 2 * 1000);
    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_welcome);
    }

    @Override
    protected void initListener() {
//
    }

    @Override
    protected void init() {

    }

    @Override
    public void onBackPressed() {

    }
}
