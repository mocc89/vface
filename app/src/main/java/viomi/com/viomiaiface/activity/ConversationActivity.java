package viomi.com.viomiaiface.activity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.adapter.ConversationAdapter;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.callback.ConversationUpdateCallback;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.model.ConversationBean;
import viomi.com.viomiaiface.service.VoiceService;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiaiface.widget.MyClockView;

public class ConversationActivity extends BaseActivity {

    private ImageView back_icon;
    private ListView conlist;
    private ImageView statusIcon;
    private Intent serviceIntent;
    private ServiceConnection serviceConnection;
    private VoiceService voiceService;
    private List<ConversationBean> datalist;
    private ConversationAdapter adapter;
    private static final long AUTOCLOSETIME = 15 * 1000L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_conversation);
        back_icon = findViewById(R.id.back_icon);
        statusIcon = findViewById(R.id.statusIcon);
        conlist = findViewById(R.id.conlist);
        MyClockView clock_view = findViewById(R.id.clock_view);
        clock_view.setVisibility(View.GONE);
        back_icon.setVisibility(View.GONE);
    }

    @Override
    protected void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void init() {
        serviceIntent = new Intent(this, VoiceService.class);
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                LogUtils.e(TAG, "onServiceConnected");
                voiceService = ((VoiceService.MyBinder) service).getService();
                afterConnect();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        };
        bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);
        showVSpeaking();
    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case HandlerMsgWhat.MSG0: {
                finish();
                break;
            }

            default:
                break;
        }
    }

    private void afterConnect() {
        voiceService.setConversationUpdateCallback(new ConversationUpdateCallback() {
            @Override
            public void update(List<ConversationBean> conversationList) {
                if (datalist != null) {
                    datalist.clear();
                    datalist.addAll(conversationList);
                    adapter.notifyDataSetChanged();
                    if (datalist.size() > 0) {
                        conlist.setSelection(datalist.size() - 1);
                    }
                }
            }

            @Override
            public void onListeningStart() {
                showVListening();
            }

            @Override
            public void onListeningStop() {
                showSearching();
            }

            @Override
            public void onListenFail() {
                showVFree();
                mHandler.removeCallbacksAndMessages(null);
                mHandler.sendEmptyMessageDelayed(0, AUTOCLOSETIME);
            }

            @Override
            public void onSpeakingStart() {
                showVSpeaking();
            }

            @Override
            public void onSpeakingStop() {
                showVFree();
                mHandler.removeCallbacksAndMessages(null);
                mHandler.sendEmptyMessageDelayed(0, AUTOCLOSETIME);
            }

            @Override
            public void onWakeup() {
                mHandler.removeCallbacksAndMessages(null);
            }

        });

        datalist = new ArrayList<>();
        datalist.addAll(voiceService.getConversationList());
        adapter = new ConversationAdapter(datalist, this);
        conlist.setAdapter(adapter);
        if (datalist.size() > 0) {
            conlist.setSelection(datalist.size() - 1);
        }
    }

    private void showVFree() {
        Glide.get(this).clearMemory();
        Glide.with(this).load(R.drawable.conversation_free_v).into(statusIcon);
    }

    private void showVSpeaking() {
        Glide.get(this).clearMemory();
        Glide.with(this).load(R.drawable.conversation_speeaking_v).into(statusIcon);
    }

    private void showSearching() {
        Glide.get(this).clearMemory();
        Glide.with(this).load(R.drawable.conversation_searching_v).into(statusIcon);
    }


    private void showVListening() {
        Glide.get(this).clearMemory();
        Glide.with(this).load(R.drawable.conversation_listening_v).into(statusIcon);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (voiceService != null) {
            voiceService.clearConversationList();
            voiceService.removeConversationUpdateCallback();
        }
        unbindService(serviceConnection);
    }
}
