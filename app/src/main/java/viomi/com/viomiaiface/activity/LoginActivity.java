package viomi.com.viomiaiface.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.miot.api.MiotManager;
import com.miot.common.exception.MiotException;
import com.miot.common.people.People;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.config.FaceConfig;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.config.MURL;
import viomi.com.viomiaiface.model.UserEntity;
import viomi.com.viomiaiface.utils.FileUtil;
import viomi.com.viomiaiface.utils.HttpApi;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiaiface.utils.ResponeCode;
import viomi.com.viomiaiface.utils.ToastUtil;
import viomi.com.viomiaiface.mijia.XiaomiAccountGetPeopleInfoTask;
import viomi.com.viomiaiface.widget.LoadingView;

public class LoginActivity extends BaseActivity {

    private ImageView back_icon;
    private TextView title_view;
    private MyHandler mHandler;
    private String device_id;
    private LoadingView loadingView;
    private ImageView qecode_img;
    private CheckThread checkThread;

    private static class MyHandler extends Handler {

        WeakReference<LoginActivity> weakReference;

        public MyHandler(LoginActivity activity) {
            this.weakReference = new WeakReference<LoginActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {

            LoginActivity mActivity = weakReference.get();

            if (mActivity != null) {
                switch (msg.what) {
                    case HandlerMsgWhat.MSG0: {
                        String result = (String) msg.obj;
                        LogUtils.e(mActivity.TAG, "MSG0=" + result);
                        mActivity.showQRCodeReslut(true, result, null);
                        break;
                    }

                    case HandlerMsgWhat.MSG1: {
                        Object result = msg.obj;
                        LogUtils.e(mActivity.TAG, "MSG1=" + result);
                        mActivity.showQRCodeReslut(false, null, result);
                        break;
                    }

                    case HandlerMsgWhat.MSG2: {
                        String result = (String) msg.obj;
                        LogUtils.e(mActivity.TAG, "MSG2=" + result);
                        mActivity.checkLoginResult(true, result, null);
                        break;
                    }

                    case HandlerMsgWhat.MSG3: {
                        Object result = msg.obj;
                        LogUtils.e(mActivity.TAG, "MSG3=" + result);
                        mActivity.checkLoginResult(false, null, result);
                        break;
                    }
                }
            }
        }
    }


    /*
    * 轮询线程 每3秒发起一次
    * */

    private static class CheckThread extends Thread {
        private WeakReference<LoginActivity> weakReference;

        public CheckThread(LoginActivity activity) {
            this.weakReference = new WeakReference(activity);
        }

        @Override
        public void run() {
            while (!isInterrupted()) {
                LoginActivity mActivity = weakReference.get();
                if (mActivity != null) {
                    mActivity.checkLogin();
                    SystemClock.sleep(3000);
                }
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_login);
        back_icon = findViewById(R.id.back_icon);
        title_view = findViewById(R.id.title_view);
        loadingView = findViewById(R.id.loadingView);
        qecode_img = findViewById(R.id.qecode_img);
    }

    @Override
    protected void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void init() {
        title_view.setText(R.string.login_title);
        mHandler = new MyHandler(this);
        device_id = Settings.System.getString(getContentResolver(), Settings.Secure.ANDROID_ID) + System.currentTimeMillis() / 1000;
        requestQRImg();
    }

    //请求二维码
    private void requestQRImg() {
        Map<String, String> map = new HashMap<>();
        map.put("type", "1");
        map.put("clientID", device_id);
        loadingView.setVisibility(View.VISIBLE);
        HttpApi.getRequestHandler(MURL.QRCODE_URL, map, mHandler, HandlerMsgWhat.MSG0, HandlerMsgWhat.MSG1);
    }

    //二维码请求结果
    private void showQRCodeReslut(boolean success, String result, Object error) {
        loadingView.setVisibility(View.GONE);
        if (!success) {
            ResponeCode.onErrorHint(error);
            return;
        }

        JSONObject json = JsonUitls.getJSONObject(result);
        JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
        int code = JsonUitls.getInt(mobBaseRes, "code");
        String desc = JsonUitls.getString(mobBaseRes, "desc");

        if (ResponeCode.isSuccess(code, desc, true)) {
            String imgResult = JsonUitls.getString(mobBaseRes, "result");
            Glide.with(this).load(imgResult).into(qecode_img);
            startCheckThread();
        }
    }


    private void startCheckThread() {
        //启动线程查询是否扫描成功
        if (checkThread != null) {
            checkThread.weakReference.clear();
            checkThread.interrupt();
        }
        checkThread = new CheckThread(this);
        checkThread.start();
    }


    //查询是否登录成功
    private void checkLogin() {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("clientID", device_id);
        HttpApi.postRequestHandler(MURL.CHECK_LOGIN, paramsMap, mHandler, HandlerMsgWhat.MSG2, HandlerMsgWhat.MSG3);
    }


    //解析登录结果
    private void checkLoginResult(boolean success, String result, Object error) {
        if (!success) {
            ResponeCode.onErrorHint(error);
            return;
        }

        JSONObject json = JsonUitls.getJSONObject(result);

        JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
        int code = JsonUitls.getInt(mobBaseRes, "code");
        String desc = JsonUitls.getString(mobBaseRes, "desc");

        if (ResponeCode.isSuccess(code, desc, false)) {

            String viomiToken = JsonUitls.getString(mobBaseRes, "token");
            JSONObject loginData = JsonUitls.getJSONObject(mobBaseRes, "loginData");
            String viomiUserId = JsonUitls.getString(loginData, "userId");
            String viomiUserCode = JsonUitls.getString(loginData, "userCode");
            String weChatHeadImg = JsonUitls.getString(loginData, "headImg");

            String appendAttr = JsonUitls.getString(mobBaseRes, "appendAttr");
            JSONObject appendAttrJson = JsonUitls.getJSONObject(appendAttr);

            String viomiNikeName = JsonUitls.getString(appendAttrJson, "nikeName");
            String viomiHeadImg = JsonUitls.getString(appendAttrJson, "headImg");
            String viomiAccount = JsonUitls.getString(appendAttrJson, "account");

            JSONObject miJson = JsonUitls.getJSONObject(appendAttrJson, "xiaomi");

            String miId = JsonUitls.getString(miJson, "miId");
            String miAccessToken = JsonUitls.getString(miJson, "accessToken");
            String miUserId = JsonUitls.getString(miJson, "userId");
            String scanType = JsonUitls.getString(miJson, "type");
            String miExpiresIn = JsonUitls.getString(miJson, "mExpiresIn");
            String miMacKey = JsonUitls.getString(miJson, "macKey");
            String miMacAlgorithm = JsonUitls.getString(miJson, "macAlgorithm");

            UserEntity user = new UserEntity(viomiToken, viomiUserId, viomiUserCode, weChatHeadImg, viomiNikeName, viomiHeadImg, viomiAccount, miId, miAccessToken, miUserId, scanType, miExpiresIn, miMacKey, miMacAlgorithm);
            LogUtils.e(TAG, user.toString());

            FileUtil.saveObject(this, FaceConfig.USERFILENAME, user);

            new XiaomiAccountGetPeopleInfoTask(miAccessToken, miExpiresIn, miMacKey, miMacAlgorithm,
                    new XiaomiAccountGetPeopleInfoTask.Handler() {
                        @Override
                        public void onSucceed(People people) {
                            Log.d(TAG, "XiaomiAccountGetPeopleInfoTask OK");
                            try {
                                MiotManager.getPeopleManager().savePeople(people);
                                ToastUtil.show(getString(R.string.login_success));
                                finish();
                            } catch (MiotException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailed() {
                            Log.d(TAG, "XiaomiAccountGetPeopleInfoTask Failed");
                        }
                    }).execute();
        }

        //过期取消轮询
        if (915 == code) {
            if (checkThread != null) {
                checkThread.weakReference.clear();
                checkThread.interrupt();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (checkThread != null) {
            checkThread.weakReference.clear();
            checkThread.interrupt();
        }
        if (mHandler != null) {
            mHandler.weakReference.clear();
            mHandler.removeCallbacksAndMessages(null);
        }
    }
}
