package viomi.com.viomiaiface.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.adapter.ShowFragmentAdapter;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.base.BaseFragment;
import viomi.com.viomiaiface.config.FaceConfig;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.config.MURL;
import viomi.com.viomiaiface.dueros.api.IOauth;
import viomi.com.viomiaiface.dueros.api.OauthImpl;
import viomi.com.viomiaiface.dueros.http.HttpConfig;
import viomi.com.viomiaiface.fragment.ClockFragment;
import viomi.com.viomiaiface.fragment.FrigeFrament;
import viomi.com.viomiaiface.fragment.HoodFragment;
import viomi.com.viomiaiface.fragment.ViomiLogoFragment;
import viomi.com.viomiaiface.fragment.WaterpuriFragment;
import viomi.com.viomiaiface.fragment.WeatherFragment;
import viomi.com.viomiaiface.mijia.MiDeviceManager;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.service.DownloadService;
import viomi.com.viomiaiface.service.VoiceService;
import viomi.com.viomiaiface.utils.HttpApi;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiaiface.utils.ToastUtil;

public class ShowScreenActivity extends BaseActivity {
    private MiDeviceManager mMiDeviceManager;
    private LocalBroadcastManager mBroadcastManager;
    private ViewPager viewPager;
    //滚动时间间隔
    private static long SCROLLTIME = 10 * 1000;
    private List<Fragment> fragmentList;
    public int vpScrollState;
    public VoiceService mService;
    ServiceConnection mConn;
    /**
     * 0 净水，1 冰箱，2 烟机
     */
    public AbstractDevice[] devicesArray = new AbstractDevice[3];
    private String version;
    private String downlink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_show_screen);
        viewPager = findViewById(R.id.viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                vpScrollState = state;

            }
        });

    }

    private void registerReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Miconfig.ACTION_BIND_SERVICE_SUCCEED);
        filter.addAction(Miconfig.ACTION_BIND_SERVICE_FAILED);
        filter.addAction(Miconfig.ACTION_DISCOVERY_DEVICE_SUCCEED);
        filter.addAction(Miconfig.ACTION_DISCOVERY_DEVICE_FAILED);
        mBroadcastManager.registerReceiver(mReceiver, filter);
    }

    @Override
    protected void initListener() {
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        startScroll();
                        break;
                    default:
                        stopScroll();
                        break;
                }
                return false;
            }
        });
    }

    private void initBaiduOauth() {
        IOauth baiduOauth = new OauthImpl();
        if (baiduOauth.isSessionValid()) {
            HttpConfig.setAccessToken(baiduOauth.getAccessToken());
            Intent intent = new Intent(this, VoiceService.class);
            startService(intent);
        } else {
            baiduOauth.authorize();
        }
    }

    @Override
    protected void init() {
        initBaiduOauth();

//        Intent intent = new Intent(this, VideoChatService.class);
//        startService(intent);

        fragmentList = new ArrayList<>();
        mBroadcastManager = LocalBroadcastManager.getInstance(this);
        mMiDeviceManager = MiDeviceManager.getInstance();
        setFragmentList();

        mHandler.sendEmptyMessage(HandlerMsgWhat.MSG4);
    }

    private void setFragmentList() {
        fragmentList.clear();
        fragmentList.add(new WeatherFragment());
        fragmentList.add(new ClockFragment());
        fragmentList.add(new ViomiLogoFragment());
        fragmentList.add(new WaterpuriFragment());
        fragmentList.add(new FrigeFrament());
        fragmentList.add(new HoodFragment());
        setVPAdapter();
    }

    private void setVPAdapter() {
        fragmentList.size();//必须保证大于等于4 否则报错
        if (fragmentList.size() < 4) {
            setFragmentList();
            return;
        }
        FragmentManager fm = getSupportFragmentManager();
        ShowFragmentAdapter adapter = new ShowFragmentAdapter(fm, fragmentList);
        viewPager.setAdapter(adapter);

    }


    @Override
    protected void onResume() {
        super.onResume();
        startScroll();
        if (new OauthImpl().isSessionValid() && mConn == null) {
            mConn = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName name, IBinder service) {
                    mService = ((VoiceService.MyBinder) service).getService();
                    mService.devicesArray = devicesArray;

                }

                @Override
                public void onServiceDisconnected(ComponentName name) {
                    mService = null;
                }
            };
            bindService(new Intent(activity, VoiceService.class), mConn, Context.BIND_AUTO_CREATE);
        }
        registerReceiver();
        mMiDeviceManager.queryWanDeviceList();
    }

    @Override
    protected void onDestroy() {
        if (mConn != null) {
            unbindService(mConn);
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopScroll();
        mBroadcastManager.unregisterReceiver(mReceiver);
    }


    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "action: " + intent.getAction());
            switch (intent.getAction()) {
                //TODO: for service restart
                case Miconfig.ACTION_BIND_SERVICE_SUCCEED:
                    break;
                case Miconfig.ACTION_BIND_SERVICE_FAILED:
                    break;

                case Miconfig.ACTION_DISCOVERY_DEVICE_SUCCEED:
                    LogUtils.e(TAG, "discovery device succeed");
                    List<AbstractDevice> devices = mMiDeviceManager.getWanDevices();
                    if (devices != null && devices.size() > 0) {
                        for (int i = 0; i < devices.size(); i++) {
                            AbstractDevice device = devices.get(i);
                            if (device.isOnline()) {
                                if (device.getDeviceModel().startsWith(Miconfig.YUNMI_WATERPURI_X5)) {
                                    for (Fragment f : fragmentList) {
                                        if (f instanceof WaterpuriFragment) {
                                            ((BaseFragment) f).setDevice(device);
                                        }
                                    }
                                    if (devicesArray[0] == null)
                                        devicesArray[0] = device;
//                                    Intent intent1 = new Intent(activity, ConversationActivity.class);
//                                    intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    startActivity(intent1);
//                                    mService.voiceFilter("关闭烟机灯光");
                                } else if (device.getDeviceModel().startsWith("viomi.fridge")) {
                                    for (Fragment f : fragmentList) {
                                        if (f instanceof FrigeFrament) {
                                            ((BaseFragment) f).setDevice(device);
                                        }
                                    }
                                    if (devicesArray[1] == null)
                                        devicesArray[1] = device;
                                } else if (device.getDeviceModel().startsWith("viomi.hood")) {
                                    for (Fragment f : fragmentList) {
                                        if (f instanceof HoodFragment) {
                                            ((BaseFragment) f).setDevice(device);
                                        }
                                    }
                                    if (devicesArray[2] == null)
                                        devicesArray[2] = device;
                                } else if (device.getDeviceModel().startsWith("viomi.dishwasher")) {
                                    if (devicesArray[3] == null)
                                        devicesArray[3] = device;
                                } else if (device.getDeviceModel().startsWith("viomi.washer")) {
                                    if (devicesArray[4] == null)
                                        devicesArray[4] = device;
                                } else if (device.getDeviceModel().startsWith("viomi.oven")) {
                                    if (devicesArray[5] == null)
                                        devicesArray[5] = device;
                                }
                                mService.devicesArray = devicesArray;
                            }
                        }
                    }

                    break;
                case Miconfig.ACTION_DISCOVERY_DEVICE_FAILED:
                    LogUtils.e(TAG, "discovery device failed");
                    break;
            }
        }
    };

    private void startScroll() {
        mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG0, SCROLLTIME);
    }

    private void stopScroll() {
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case HandlerMsgWhat.MSG0:
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG0, SCROLLTIME);
                break;

            case HandlerMsgWhat.MSG2: {
                LogUtils.e(TAG, "HandlerMsgWhat.MSG2=" + msg.obj.toString());
                parseJson(msg.obj.toString());
            }
            break;
            case HandlerMsgWhat.MSG3: {

            }
            break;
            case HandlerMsgWhat.MSG4: {
                reqUpdate();
                mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG4, 4 * 60 * 60 * 1000);
            }
            default:
                break;
        }
    }


    private float y;

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        int action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                y = ev.getY();
                break;
            case MotionEvent.ACTION_UP:
                float y1 = ev.getY();

                if (y1 - y > 300) {
                    LogUtils.e(TAG, "main-page");
                    Intent intent = new Intent(ShowScreenActivity.this, MainActivity.class);
                    startActivity(intent);
                    return true;
                }
                break;
        }
        return super.dispatchTouchEvent(ev);
    }


    private void reqUpdate() {
        Map<String, String> map = new HashMap<>();
        map.put("type", "version");
        map.put("package", "viomi.com.viomiaiface");
        map.put("channel", FaceConfig.UPDATEENVIRONMENT);
        map.put("p", "1");
        map.put("l", "1");
        HttpApi.getRequestHandler(MURL.UPDATE, map, mHandler, HandlerMsgWhat.MSG2, HandlerMsgWhat.MSG3);
    }

    private void parseJson(String result) {
        try {
            JSONObject json = new JSONObject(result);
            JSONArray data = JsonUitls.getJSONArray(json, "data");
            if (data != null && data.length() > 0) {
                JSONObject item = data.getJSONObject(0);
                String detail = JsonUitls.getString(item, "detail");
                version = JsonUitls.getString(item, "code");
                downlink = JsonUitls.getString(item, "url");
                if (!FaceConfig.CURRENTVERSION.equals(version)) {
                    showUpdateDialog(detail);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showUpdateDialog(String code) {
        final Dialog dialog = new Dialog(this, R.style.selectorDialog);
        View view = LayoutInflater.from(this).inflate(R.layout.update_dialog_layout, null);
        dialog.setContentView(view);

        TextView update_title = (TextView) view.findViewById(R.id.update_title);
        ListView tv_list = (ListView) view.findViewById(R.id.tv_list);
        TextView no_thanks = (TextView) view.findViewById(R.id.no_thanks);
        TextView ok_update = (TextView) view.findViewById(R.id.ok_update);

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < version.length(); i++) {
            sb.append(version.charAt(i));
            if (i != version.length() - 1) {
                sb.append(".");
            }
        }
        String version_a = sb.toString();
        update_title.setText("V" + version_a + "版本发布啦，马上下载体验吧！");

        no_thanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ok_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadUpdate();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void downloadUpdate() {
        ToastUtil.show("正在后台下载，请稍后...");
        Intent intent = new Intent(this, DownloadService.class);
        intent.putExtra("downlink", downlink);
        intent.putExtra("version", version);
        startService(intent);
    }

}
