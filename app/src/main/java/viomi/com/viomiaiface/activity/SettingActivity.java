package viomi.com.viomiaiface.activity;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.config.FaceConfig;
import viomi.com.viomiaiface.model.UserEntity;
import viomi.com.viomiaiface.utils.FileUtil;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.wifilibrary.wifimodel.WifiScanActivity;

public class SettingActivity extends BaseActivity {

    private ImageView back_icon;
    private RelativeLayout login_layout;
    private ImageView user_icon;
    private TextView user_name;
    private RelativeLayout family_member;
    private SeekBar lightBar;
    private SeekBar sysVoiceBar;
    private SeekBar mediaVoiceBar;
    private RelativeLayout wifiSetting;
    private RelativeLayout outdoorSetting;
    private RelativeLayout systemUpdate;

    private AudioManager mAudioManager;
    private TextView title_view;
    private boolean isLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_setting);
        back_icon = findViewById(R.id.back_icon);
        title_view = findViewById(R.id.title_view);

        login_layout = findViewById(R.id.login_layout);
        user_icon = findViewById(R.id.user_icon);
        user_name = findViewById(R.id.user_name);

        family_member = findViewById(R.id.family_member);

        lightBar = findViewById(R.id.lightBar);
        sysVoiceBar = findViewById(R.id.sysVoiceBar);
        mediaVoiceBar = findViewById(R.id.mediaVoiceBar);

        wifiSetting = findViewById(R.id.wifiSetting);
        outdoorSetting = findViewById(R.id.outdoorSetting);
        systemUpdate = findViewById(R.id.systemUpdate);

    }

    @Override
    protected void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        login_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLogin) {
                    Intent intent = new Intent(SettingActivity.this, LoginOutActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(SettingActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        family_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        lightBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                saveBrightness(SettingActivity.this, progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sysVoiceBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mediaVoiceBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        wifiSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, WifiScanActivity.class);
                startActivity(intent);
            }
        });

        outdoorSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, OutdoorSettingActivity.class);
                startActivity(intent);
            }
        });

        systemUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    protected void init() {
        title_view.setText(R.string.system_title);
        Glide.with(this).load(R.drawable.default_user).apply(new RequestOptions().circleCrop()).into(user_icon);

        //亮度相关
        int systemBrightness = 0;
        try {
            systemBrightness = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        lightBar.setMax(255);
        lightBar.setProgress(systemBrightness);


        //音量相关
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        //当前系统音量
        int currentSysVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
        //最大音量
        int maxSysVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
        sysVoiceBar.setMax(maxSysVolume);
        sysVoiceBar.setProgress(currentSysVolume);

        //当前媒体音量
        int currentMediaVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        //最大音量
        int maxMediaVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        mediaVoiceBar.setMax(maxMediaVolume);
        mediaVoiceBar.setProgress(currentMediaVolume);
    }

    private void saveBrightness(Context context, int brightness) {
        Uri uri = Settings.System.getUriFor(Settings.System.SCREEN_BRIGHTNESS);
        Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, brightness);
        context.getContentResolver().notifyChange(uri, null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserEntity user = (UserEntity) FileUtil.getObject(this, FaceConfig.USERFILENAME);
        if (user != null) {
            LogUtils.e(TAG, user.toString());
            isLogin = true;
            Glide.with(this).load(user.getViomiHeadImg()).apply(new RequestOptions().circleCrop()).into(user_icon);
            user_name.setText(user.getViomiNikeName());
        } else {
            isLogin = false;
            Glide.with(this).load(R.drawable.default_user).apply(new RequestOptions().circleCrop()).into(user_icon);
            user_name.setText(getString(R.string.not_login_in));
        }

    }
}
