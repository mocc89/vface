package viomi.com.viomiaiface.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.config.BroadcastAction;

public class VideoRingActivity extends BaseActivity {

    private ImageView answer;
    private ImageView ring;
    private ImageView reject;
    private int uid;
    private BroadcastReceiver receiver;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_video_ring);
        answer = findViewById(R.id.answer);
        ring = findViewById(R.id.ring);
        reject = findViewById(R.id.reject);
    }

    @Override
    protected void initListener() {
        answer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VideoRingActivity.this, VideoChatViewActivity.class);
                intent.putExtra("uid", uid);
                startActivity(intent);
                onBackPressed();
            }
        });

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void init() {
        Glide.with(this).load(R.drawable.video_ring).into(ring);
        Intent intent = getIntent();
        uid = intent.getIntExtra("uid", 0);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (BroadcastAction.VIDEOCHATUSERLEFT.equals(intent.getAction())) {
                    onBackPressed();
                }
            }
        };
        IntentFilter filter = new IntentFilter(BroadcastAction.VIDEOCHATUSERLEFT);
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
