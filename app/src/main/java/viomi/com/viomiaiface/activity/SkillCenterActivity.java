package viomi.com.viomiaiface.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;

/**
 * Created by linhs on 2018/1/5 0006.
 */

public class SkillCenterActivity extends BaseActivity {

    private static final String TAG = SkillCenterActivity.class.getName();
    private final Timer timer = new Timer();
    private TimerTask task;
    TextView title_view;
    ImageView sys_dot;
    private ImageView back_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initView() {
        setContentView(R.layout.skillcenter_activity);
        title_view = (TextView) findViewById(R.id.title_view);
        back_icon = findViewById(R.id.back_icon);
    }

    @Override
    protected void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void init() {
        title_view.setText(this.getResources().getString(R.string.skill_centener_txt));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    public void btnBack(View view) {
        finish();
    }
}
