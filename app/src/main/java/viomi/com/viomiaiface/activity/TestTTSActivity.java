package viomi.com.viomiaiface.activity;

import android.os.Bundle;
import android.view.View;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.utils.BaiduTTSTool;

public class TestTTSActivity extends BaseActivity {

    private BaiduTTSTool baiduTTSTool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_tts);

        baiduTTSTool = BaiduTTSTool.getInstance(mHandler);
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void init() {

    }

    public void btnClick1(View view) {
        baiduTTSTool.speak("123456789");
    }

    public void btnClick2(View view) {
        baiduTTSTool.pause();
    }

    public void btnClick3(View view) {
        baiduTTSTool.resume();
    }

    public void btnClick4(View view) {
        baiduTTSTool.stop();
    }
}
