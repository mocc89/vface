package viomi.com.viomiaiface.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.config.DeviceModels;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.utils.FrigeUtil;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_OFF;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

public class FrigeControlActivity extends BaseActivity {
    final int REFRESH_PROPS = 12;

    final int DELAY_TIME = 3000;

    AbstractDevice mDevice;
    String[] mParams = {"RCSetTemp", "CCSetTemp", "FCSetTemp", "RCMaxTemp", "RCMinTemp"//0-4
            , "CCMaxTemp", "CCMinTemp", "FCMaxTemp", "FCMinTemp", "RCSet", "CCSet"};//5-10
    String[] mResults = new String[mParams.length];
    @InjectView(R.id.tv_cang)
    TextView tvCang;
    @InjectView(R.id.tv_bian)
    TextView tvBian;
    @InjectView(R.id.tv_dong)
    TextView tvDong;
    @InjectView(R.id.sb_cang)
    SeekBar sbCang;
    @InjectView(R.id.switch_cang)
    Switch switchCang;
    @InjectView(R.id.sb_bian)
    SeekBar sbBian;
    @InjectView(R.id.switch_bian)
    Switch switchBian;
    @InjectView(R.id.sb_dong)
    SeekBar sbDong;
    @InjectView(R.id.switch_dong)
    Switch switchDong;
    @InjectView(R.id.tv_cang_indicator)
    TextView tvCangIndicator;
    @InjectView(R.id.layout_cang)
    LinearLayout layoutCang;
    @InjectView(R.id.tv_bian_indicator)
    TextView tvBianIndicator;
    @InjectView(R.id.layout_bian)
    LinearLayout layoutBian;
    @InjectView(R.id.tv_dong_indicator)
    TextView tvDongIndicator;
    @InjectView(R.id.layout_dong)
    LinearLayout layoutDong;
    boolean isSetting;
    @InjectView(R.id.v_devider2)
    View vDevider2;
    @InjectView(R.id.layout_2)
    LinearLayout layout2;
    @InjectView(R.id.layout_seebar2)
    RelativeLayout layoutSeebar2;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_frige_control);
        ButterKnife.inject(this);
        mDevice = getIntent().getParcelableExtra("device");
        tvDongIndicator.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (tvDongIndicator.getWidth() != 0) {
                    refreshView(true);
                    tvDongIndicator.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
    }

    @Override
    protected void initListener() {
        switchCang.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TextUtils.isEmpty(mResults[9]))
                        return true;
                    Switch_ON_OFF switchOnOff;
                    if (mResults[9].equals("on")) {
                        mResults[9] = "off";
                        switchOnOff = Switch_ON_OFF.off;
                    } else {
                        mResults[9] = "on";
                        switchOnOff = Switch_ON_OFF.on;
                    }
                    refreshView(true);
                    setSwitchCang(switchOnOff);
                }
                return true;
            }
        });
        switchBian.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TextUtils.isEmpty(mResults[10]))
                        return true;
                    Switch_ON_OFF switchOnOff;
                    if (mResults[10].equals("on")) {
                        mResults[10] = "off";
                        switchOnOff = Switch_ON_OFF.off;
                    } else {
                        mResults[10] = "on";
                        switchOnOff = Switch_ON_OFF.on;
                    }
                    refreshView(true);
                    setSwitchBian(switchOnOff);
                }
                return true;
            }
        });
        sbCang.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!fromUser)
                    return;
                if (TextUtils.isEmpty(mResults[4]))
                    return;
                mResults[0] = Integer.parseInt(mResults[4]) + progress + "";
                LogUtils.d(TAG, "onProgressChanged  " + mResults[4] + "  " + progress + "   " + mResults[0]);
                refreshView(true);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (TextUtils.isEmpty(mResults[4]))
                    return;
                setTempCang();
            }
        });
        sbBian.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!fromUser)
                    return;
                if (TextUtils.isEmpty(mResults[6]))
                    return;
                mResults[1] = Integer.parseInt(mResults[6]) + progress + "";
                refreshView(true);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (TextUtils.isEmpty(mResults[6]))
                    return;
                setTempBian();
            }
        });
        sbDong.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!fromUser)
                    return;
                if (TextUtils.isEmpty(mResults[8]))
                    return;
                mResults[2] = Integer.parseInt(mResults[8]) + seekBar.getProgress() + "";
                refreshView(true);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (TextUtils.isEmpty(mResults[8]))
                    return;
                setTempDong();
            }
        });
    }

    void setSwitchCang(Switch_ON_OFF switchOnOff) {
        isSetting = true;
        Operation.getInstance().sendFridgeCommand2(mDevice.getDeviceId(), Miconfig.MI_OAUTH_APP_ID + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                isSetting = false;
            }

            @Override
            public void onFail(String result) {
                isSetting = false;
            }
        }, switchOnOff);
    }

    void setSwitchBian(Switch_ON_OFF switchOnOff) {
        isSetting = true;
        Operation.getInstance().sendFridgeCommand3(mDevice.getDeviceId(), Miconfig.MI_OAUTH_APP_ID + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                isSetting = false;
            }

            @Override
            public void onFail(String result) {
                isSetting = false;
            }
        }, switchOnOff);
    }

    void setTempCang() {
        isSetting = true;
        Operation.getInstance().sendFridgeCommand5(mDevice.getDeviceId(), Miconfig.MI_OAUTH_APP_ID + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                isSetting = false;
            }

            @Override
            public void onFail(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                isSetting = false;
            }
        }, Integer.parseInt(mResults[0]));
    }

    void setTempBian() {
        isSetting = true;
        Operation.getInstance().sendFridgeCommand6(mDevice.getDeviceId(), Miconfig.MI_OAUTH_APP_ID + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                isSetting = false;
            }

            @Override
            public void onFail(String result) {
                isSetting = false;
            }
        }, Integer.parseInt(mResults[1]));
    }

    void setTempDong() {
        isSetting = true;
        Operation.getInstance().sendFridgeCommand7(mDevice.getDeviceId(), Miconfig.MI_OAUTH_APP_ID + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                isSetting = false;
            }

            @Override
            public void onFail(String result) {
                isSetting = false;
            }
        }, Integer.parseInt(mResults[2]));
    }

    /**
     * 是否3仓，包含变温室
     */
    boolean cang3 = true;

    /**
     * @param isByUser 是否手动
     */
    private void refreshView(boolean isByUser) {
        cang3 = FrigeUtil.containBianwen(mDevice.getDeviceModel());
        if (cang3) {
            layout2.setVisibility(View.VISIBLE);
            layoutSeebar2.setVisibility(View.VISIBLE);
            vDevider2.setVisibility(View.VISIBLE);
        } else {
            layout2.setVisibility(View.GONE);
            layoutSeebar2.setVisibility(View.GONE);
            vDevider2.setVisibility(View.GONE);
        }


        if (isByUser) {

        } else if (isSetting)
            return;
        boolean isNull = false;
        for (String s : mResults) {
            if (s == null || s.equals("")) {
                isNull = true;
                break;
            }
        }
        if (isNull)
            return;
        try {
            setCangTemp();
            setBianTemp();
            setDongTemp();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void init() {


    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mDevice != null && mDevice.isOnline()) {
            mHandler.sendEmptyMessage(REFRESH_PROPS);
        }
    }

    @Override
    public void onBackPressed() {
        if (!TextUtils.isEmpty(mResults[0]) && !TextUtils.isEmpty(mResults[1]) && !TextUtils.isEmpty(mResults[2])) {
            String[] result = {mResults[0], mResults[1], mResults[2]};
            setResult(RESULT_OK, new Intent().putExtra("result", result));
            finish();
        } else super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        switch (msg.what) {
            case REFRESH_PROPS:
                getProps();
                break;
        }
    }

    void setCangTemp() throws Throwable {
        tvCang.setText(mResults[0]);
        tvCangIndicator.setText(mResults[0]);
        int max = Integer.parseInt(mResults[3]) - Integer.parseInt(mResults[4]);
        int process = Integer.parseInt(mResults[0]) - Integer.parseInt(mResults[4]);
        sbCang.setMax(max);
        sbCang.setProgress(process);
        int margin = (int) ((sbCang.getWidth() - sbCang.getPaddingRight() * 2) * process / (max * 1f))
                + sbCang.getPaddingLeft()
                - tvCangIndicator.getWidth() / 2;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tvCangIndicator.getLayoutParams();
        params.leftMargin = margin;
        tvCangIndicator.setLayoutParams(params);
        if (mResults[9].equals("on")) {
            switchCang.setChecked(true);
        } else switchCang.setChecked(false);
    }

    //[6,-4,-18,8,2,8,-18,-15,-25,"on","on"]
    void setBianTemp() throws Throwable {
        tvBian.setText(mResults[1]);
        tvBianIndicator.setText(mResults[1]);
        int max = Integer.parseInt(mResults[5]) - Integer.parseInt(mResults[6]);
        int process = Integer.parseInt(mResults[1]) - Integer.parseInt(mResults[6]);
        sbBian.setMax(max);
        sbBian.setProgress(process);
        int margin = (int) ((sbBian.getWidth() - sbBian.getPaddingRight() * 2) * process / (max * 1f))
                + sbBian.getPaddingLeft()
                - tvBianIndicator.getWidth() / 2;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tvBianIndicator.getLayoutParams();
        params.leftMargin = margin;
        tvBianIndicator.setLayoutParams(params);
        if (mResults[10].equals("on")) {
            switchBian.setChecked(true);
        } else switchBian.setChecked(false);
    }

    //[6,-4,-18,8,2,8,-18,-15,-25,"on","on"]
    void setDongTemp() throws Throwable {
        tvDong.setText(mResults[2]);
        tvDongIndicator.setText(mResults[2]);
        int max = Integer.parseInt(mResults[7]) - Integer.parseInt(mResults[8]);
        int process = Integer.parseInt(mResults[2]) - Integer.parseInt(mResults[8]);
        sbDong.setMax(max);
        sbDong.setProgress(process);
        int margin = (int) ((sbDong.getWidth() - sbDong.getPaddingRight() * 2) * process / (max * 1f))
                + sbDong.getPaddingLeft()
                - tvDongIndicator.getWidth() / 2;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tvDongIndicator.getLayoutParams();
        params.leftMargin = margin;
        tvDongIndicator.setLayoutParams(params);
    }

    private void getProps() {
        Operation.getInstance().sendFridgeCommand0(mDevice.getDeviceId(), Miconfig.MI_OAUTH_APP_ID + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                try {
                    JSONArray jsonArray = JsonUitls.getJSONArray(new JSONObject(result), "result");
                    if (jsonArray != null && jsonArray.length() > 0)
                        for (int i = 0; i < mResults.length; i++) {
                            if (isSetting)
                                break;
                            mResults[i] = jsonArray.optString(i);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    refreshView(false);
                                }
                            });
                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mHandler.sendEmptyMessageDelayed(REFRESH_PROPS, DELAY_TIME);
            }

            @Override
            public void onFail(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                mHandler.sendEmptyMessageDelayed(REFRESH_PROPS, DELAY_TIME);
            }
        }, mParams);
    }


}
