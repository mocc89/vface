package viomi.com.viomiaiface.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.fragment.HoodFragment;

public class HoodActivity extends BaseActivity {
    private HoodFragment hoodFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hood2);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        hoodFragment = new HoodFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("device", getIntent().getParcelableExtra("device"));
        hoodFragment.setArguments(bundle);
        transaction.replace(R.id.id_content, hoodFragment);
        transaction.commit();
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void init() {

    }


}
