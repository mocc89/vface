package viomi.com.viomiaiface.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.widget.TextView;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

public class WaterpuriX5Activity extends BaseActivity {
    final int REFRESH_PROPS = 12;
    final int REFRESH_USERDATA = 13;
    final int REFRESH_TIME = 14;

    final int DELAY_TIME = 3000;
    @InjectView(R.id.tv_device_name)
    TextView tvDeviceName;
    @InjectView(R.id.tv_time)
    TextView tvTime;
    @InjectView(R.id.tv_switch)
    TextView tvSwitch;
    @InjectView(R.id.tv_puri2_value)
    TextView tvPuri2Value;
    @InjectView(R.id.tv_puri1)
    TextView tvPuri1;
    @InjectView(R.id.tv_puri2)
    TextView tvPuri2;
    @InjectView(R.id.tv_puri3_value)
    TextView tvPuri3Value;
    @InjectView(R.id.tv_puri3)
    TextView tvPuri3;
    @InjectView(R.id.tv_puri4_value)
    TextView tvPuri4Value;
    @InjectView(R.id.tv_puri4)
    TextView tvPuri4;
    AbstractDevice mDevice;
    @InjectView(R.id.tv_puri1_value)
    TextView tvPuri1Value;
    @InjectView(R.id.tv_tds)
    TextView tvTds;
    @InjectView(R.id.tv_temp)
    TextView tvTemp;
    //[16,50]
    String[] mParams = {};//15-17

    String[] mParams1 = {"tds_out", "setup_tempe"
//            , "f1_totalflow", "f1_totaltime", "f1_usedflow", "f1_usedtime"
//            , "f2_totalflow", "f2_totaltime", "f2_usedflow", "f2_usedtime"
//            , "f3_totalflow", "f3_totaltime", "f3_usedflow", "f3_usedtime"
//            ,"f4_totalflow","f4_totaltime","f4_usedflow","f4_usedtime"
    };
    //          2   3   4    5    6    7   8   9
//[137,12,  3170,1477,3170,1574,830,1213,830,1213,
    //10  11  12   13   14   15    16   17
// 3600,4320,3600,8640,7200,17280,3600,8640,0,1,0,1,0]}
    //  [17,99,3600,4320,3170,1477,3600,8640,3170,1574,7200,17280,830,1213]

    int[] mResults = new int[18];

    {
        mResults[0] = -1;
    }

    private void refreshView() throws Exception {
        //TDS=0~50               纯度高             可放心饮用
//• TDS=50~100           纯度较高           可放心饮用
//• TDS=100~300         水质一般           可饮用
//• TDS=300~600         会结水垢          不建议直饮
//• TDS=600~1000        口感较差          不建议直饮
//• TDS=1000以上         不适合饮用       不适合饮用
        if (mResults[0] > -1) {
            if (mResults[0] < 300) {
                tvSwitch.setText(getString(R.string.water_drinkable));
                tvSwitch.setBackgroundDrawable(getResources().getDrawable(R.drawable.puri_switch));
            } else {
                tvSwitch.setText(getString(R.string.water_undrinkable));
                tvSwitch.setBackgroundDrawable(getResources().getDrawable(R.drawable.hood_swith_off));
            }
            tvTds.setText(mResults[0] + "");
            tvTemp.setText(mResults[1] + "");
        }
        //             2  3    4    5       6     7   8    9       10   11    12  13
        //[123,12,  5770,2269,5770,2366,   1400,1939,1400,1939,   3600,4320,3600,8640,  7200,17280,3600,8640,   0,1,0,1,0]
        if (mResults[10] != 0) {
            double flow1 = Double.valueOf(mResults[10] - mResults[2]) / mResults[10];
            double time1 = Double.valueOf(mResults[11] - mResults[3]) / mResults[11];
            double d1 = Math.min(flow1, time1) * 100;
            if (d1 < 0)
                d1 = 0d;
//            BigDecimal value1 = new BigDecimal(d1 ).setScale(0, BigDecimal.ROUND_HALF_UP);
            tvPuri1Value.setText((int) d1 + "%");

            double flow2 = Double.valueOf(mResults[12] - mResults[4]) / mResults[12];
            double time2 = Double.valueOf(mResults[13] - mResults[5]) / mResults[13];
            double d2 = Math.min(flow2, time2) * 100;
            if (d2 < 0)
                d2 = 0d;
//            BigDecimal value2 = new BigDecimal(d2 ).setScale(0, BigDecimal.ROUND_HALF_UP);
            tvPuri2Value.setText((int) d2 + "%");


            double flow3 = Double.valueOf(mResults[14] - mResults[6]) / mResults[14];
            double time3 = Double.valueOf(mResults[15] - mResults[7]) / mResults[15];
            double d3 = Math.min(flow3, time3) * 100;
            if (d3 < 0)
                d3 = 0d;
//            BigDecimal value3 = new BigDecimal(d3 ).setScale(0, BigDecimal.ROUND_HALF_UP);
            tvPuri3Value.setText((int) d3 + "%");

            double flow4 = Double.valueOf(mResults[16] - mResults[8]) / mResults[16];
            double time4 = Double.valueOf(mResults[17] - mResults[9]) / mResults[17];
            double d4 = Math.min(flow4, time4) * 100;
            if (d4 < 0)
                d4 = 0d;
//            BigDecimal value4 = new BigDecimal(d4 ).setScale(0, BigDecimal.ROUND_HALF_UP);
            tvPuri4Value.setText((int) d4 + "%");
        }
    }

    @Override
    public void onBackPressed() {
        if (mResults[0] > -1) {
            Intent it = new Intent();
            it.putExtra("tds", mResults[0]);
            setResult(RESULT_OK, it);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        switch (msg.what) {
            case REFRESH_PROPS:
                getProps();

                break;
            case REFRESH_USERDATA:
                break;
            case REFRESH_TIME:
                tvDeviceName.setText(mDevice.getName());
                SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm");
                tvTime.setText(sdf1.format(new Date()));
                mHandler.sendEmptyMessageDelayed(REFRESH_TIME, 1000);
                break;
        }
    }

    private void getProps() {
        Operation.getInstance().sendPurifierCommand0(mDevice.getDeviceId(), Miconfig.MI_OAUTH_APP_ID + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                try {
                    JSONArray jsonArray = JsonUitls.getJSONArray(new JSONObject(result), "result");
                    if (jsonArray != null && jsonArray.length() > 0)
                        for (int i = 0; i < 2; i++) {
                            mResults[i] = jsonArray.optInt(i);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        refreshView();
                                    } catch (Throwable e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail(String result) {
                LogUtils.d(TAG, "onresult:" + result);
            }
        }, mParams1);
        Operation.getInstance().sendPurifierCommand0(mDevice.getDeviceId(), Miconfig.MI_OAUTH_APP_ID + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                try {
                    JSONArray jsonArray = JsonUitls.getJSONArray(new JSONObject(result), "result");
                    //[123,12,  5770,2269,5770,2366,1400,1939,1400,1939,3600,4320,3600,8640,7200,17280,3600,8640,   0,1,0,1,0]
                    if (jsonArray != null && jsonArray.length() > 0)
                        for (int i = 2; i < mResults.length; i++) {
                            mResults[i] = jsonArray.optInt(i);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        refreshView();
                                    } catch (Throwable e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mHandler.sendEmptyMessageDelayed(REFRESH_PROPS, DELAY_TIME);
            }

            @Override
            public void onFail(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                mHandler.sendEmptyMessageDelayed(REFRESH_PROPS, DELAY_TIME);
            }
        }, mParams);
    }


    @Override
    protected void initView() {
        setContentView(R.layout.activity_waterpuri_x5);
        ButterKnife.inject(this);
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void init() {
        mDevice = getIntent().getParcelableExtra("device");
        mHandler.sendEmptyMessage(REFRESH_TIME);
        if (mDevice.isOnline()) {
            mHandler.sendEmptyMessage(REFRESH_PROPS);
        }
    }

    void test() {
        mResults[0] = 10;
        mResults[1] = 90;
        mResults[2] = 1024;
        mResults[3] = 1024;
        mResults[4] = 206;
        mResults[5] = 369;
        mResults[6] = 1024;
        mResults[7] = 1024;
        mResults[8] = 256;
        mResults[9] = 138;
        mResults[10] = 1024;
        mResults[11] = 1024;
        mResults[12] = 156;
        mResults[13] = 954;
        mResults[14] = 1024;
        mResults[15] = 1024;
        mResults[16] = 326;
        mResults[17] = 468;
        try {
            refreshView();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
