package viomi.com.viomiaiface.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.miot.api.MiotManager;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.utils.ToastUtil;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

public class MainActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getName();
    private TextView main_btn1;
    private TextView main_btn2;
    private TextView main_btn3;
    private TextView main_btn4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_main);

        main_btn1 = findViewById(R.id.main_btn1);
        main_btn2 = findViewById(R.id.main_btn2);
        main_btn3 = findViewById(R.id.main_btn3);
        main_btn4 = findViewById(R.id.main_btn4);

    }

    @Override
    protected void initListener() {
        main_btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(intent);
            }
        });

        main_btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SkillCenterActivity.class));
            }
        });

        main_btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!MiotManager.getPeopleManager().isLogin()) {
                    ToastUtil.show("请先登录账号");
                    return;
                }

                Intent intent = new Intent(MainActivity.this, DeviceListActivity.class);
                startActivity(intent);

            }
        });

        main_btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                test();

//                Intent intent = new Intent(MainActivity.this, DcsSampleOAuthActivity.class);
//                startActivity(intent);

//                Intent intent = new Intent(MainActivity.this, MediaPlayActivity.class);
//                startActivity(intent);

//                Intent intent = new Intent(MainActivity.this, ConversationActivity.class);
//                startActivity(intent);

//                Intent intent = new Intent(MainActivity.this, WeatherActivity.class);
//                startActivity(intent);

//                Intent intent = new Intent(MainActivity.this, ShowScreenActivity.class);
//                startActivity(intent);

//                Intent intent = new Intent(MainActivity.this, VideoChatViewActivity.class);
//                startActivity(intent);

//                Intent intent = new Intent(MainActivity.this, VideoRingActivity.class);
//                startActivity(intent);

//                Intent intent = new Intent(MainActivity.this, SynthActivity.class);
//                startActivity(intent);

                Intent intent = new Intent(MainActivity.this, TestTTSActivity.class);
                startActivity(intent);

            }
        });
    }

    @Override
    protected void init() {
//        Intent intent = new Intent(this, VoiceService.class);
//        startService(intent);
    }

    /*
    * 测试
    * */
    private static String did = "53243802";
    private static String clientId = Miconfig.MI_OAUTH_APP_ID + "";
    //    private static String token = MiotManager.getPeopleManager().getPeople().getAccessToken();
    private static String token = "";
    private static ViomiDeviceCallback callback = new ViomiDeviceCallback() {
        @Override
        public void onSuccess(String result) {
            Log.e(TAG, "onSuccess:" + result);
        }

        @Override
        public void onFail(String result) {
            Log.e(TAG, "onFail:" + result);
        }
    };


    public void test() {

        new Thread(new Runnable() {
            @Override
            public void run() {
//                Operation.getInstance().sendFridgeCommand1(did, clientId, token, callback, Switch_ON_OFF.on);
                Operation.getInstance().sendDrinkBarCommand1(did, clientId, token, callback, 50);
            }
        }).start();
    }
}
