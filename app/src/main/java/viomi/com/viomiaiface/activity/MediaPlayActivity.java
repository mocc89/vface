package viomi.com.viomiaiface.activity;

import android.animation.ObjectAnimator;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONObject;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.dueros.framework.DcsFramework;
import viomi.com.viomiaiface.dueros.framework.DeviceModuleFactory;
import viomi.com.viomiaiface.dueros.framework.DirectiveBean;
import viomi.com.viomiaiface.dueros.framework.DirectiveScreen_ExBean;
import viomi.com.viomiaiface.dueros.framework.IResponseListener;
import viomi.com.viomiaiface.dueros.framework.message.Directive;
import viomi.com.viomiaiface.dueros.systeminterface.IMediaPlayer;
import viomi.com.viomiaiface.dueros.systeminterface.IPlatformFactory;
import viomi.com.viomiaiface.service.VoiceService;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiaiface.utils.ToastUtil;

public class MediaPlayActivity extends BaseActivity {

    private ImageView back_icon;
    private ImageView album_img;
    private ImageView play_control_pre;
    private ImageView play_control_play;
    private ImageView play_control_next;
    private Intent intent;
    private ServiceConnection serviceConnection;
    private VoiceService voiceService;
    private DeviceModuleFactory deviceModuleFactory;
    private IPlatformFactory platformFactory;
    private DcsFramework dcsFramework;
    private boolean isPause;
    private TextView title;
    private TextView sub_title;
    private ObjectAnimator oa;
    private RelativeLayout music_layout;
    private RelativeLayout otherContent_layout;
    private boolean stopByHand;

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_media_play);
        back_icon = findViewById(R.id.back_icon);
        album_img = findViewById(R.id.album_img);
        play_control_pre = findViewById(R.id.play_control_pre);
        play_control_play = findViewById(R.id.play_control_play);
        play_control_next = findViewById(R.id.play_control_next);
        title = findViewById(R.id.title);
        sub_title = findViewById(R.id.sub_title);

        music_layout = findViewById(R.id.music_layout);
        otherContent_layout = findViewById(R.id.otherContent_layout);

//        back_icon.setVisibility(View.GONE);
    }

    @Override
    protected void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        play_control_pre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playPre();
                stopByHand = true;
            }
        });

        play_control_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playOrPause();
                stopByHand = true;
            }
        });

        play_control_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playNext();
                stopByHand = true;
            }
        });
    }

    @Override
    protected void init() {
        intent = new Intent(this, VoiceService.class);
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                LogUtils.e(TAG, "onServiceConnected");
                voiceService = ((VoiceService.MyBinder) service).getService();
                addListner();
                afterConnect();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                LogUtils.e(TAG, "onServiceDisconnected");
            }
        };
        bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE);
    }

    private void afterConnect() {
        deviceModuleFactory = voiceService.getDeviceModuleFactory();
        addListner();
        platformFactory = voiceService.getIPlatformFactory();
        dcsFramework = voiceService.getDcsFramework();
        if (dcsFramework != null) {
            DirectiveBean directiveBean = dcsFramework.getDirectiveBean();
            DirectiveScreen_ExBean screen_extended_card = directiveBean.getScreen_extended_card();
            Directive renderPlayerInfo = screen_extended_card.getRenderPlayerInfo();
            if (screen_extended_card != null && renderPlayerInfo != null) {
                LogUtils.e(TAG, renderPlayerInfo.rawMessage + "");
                updateView(renderPlayerInfo);
                startAnim();
            }

        }
    }

    private void addListner() {
        if (deviceModuleFactory != null) {
            deviceModuleFactory.getAudioPlayerDeviceModule().addAudioPlayListener(mediaPlayerListener);
        }
    }

    private void removeListner() {
        if (deviceModuleFactory != null) {
            deviceModuleFactory.getAudioPlayerDeviceModule().removeAudioPlayListener(mediaPlayerListener);
        }
    }

    private IMediaPlayer.SimpleMediaPlayerListener mediaPlayerListener = new IMediaPlayer.SimpleMediaPlayerListener() {
        @Override
        public void onPaused() {
            super.onPaused();
            LogUtils.e(TAG, "addAudioPlayListener-onPaused");
            isPause = true;
            stopAnim();
            play_control_play.setImageResource(R.drawable.ic_play_arrow_btn);
        }

        @Override
        public void onPlaying() {
            super.onPlaying();
            LogUtils.e(TAG, "addAudioPlayListener-onPlaying");
            isPause = false;
            if (dcsFramework != null) {
                DirectiveBean directiveBean = dcsFramework.getDirectiveBean();

                DirectiveScreen_ExBean screen_extended_card = directiveBean.getScreen_extended_card();
                Directive renderPlayerInfo = screen_extended_card.getRenderPlayerInfo();
                if (screen_extended_card != null && renderPlayerInfo != null) {
                    LogUtils.e(TAG, renderPlayerInfo.rawMessage + "");
                    updateView(renderPlayerInfo);
                    play_control_play.setImageResource(R.drawable.ic_play_pause_btn);
                    startAnim();
                }
            }

        }

        @Override
        public void onCompletion() {
            super.onCompletion();
            LogUtils.e(TAG, "addAudioPlayListener-onCompletion");
            isPause = false;
            play_control_play.setImageResource(R.drawable.ic_play_arrow_btn);
            stopAnim();
        }

        @Override
        public void onStopped() {
            super.onStopped();
            LogUtils.e(TAG, "addAudioPlayListener-onStopped");
            isPause = true;
            play_control_play.setImageResource(R.drawable.ic_play_arrow_btn);
            stopAnim();

            if (!stopByHand) {
                finish();
            }
            stopByHand = false;
        }
    };

    private void playNext() {
        if (platformFactory != null && deviceModuleFactory != null) {
            platformFactory.getPlayback().next(nextPreResponseListener);
            deviceModuleFactory.getSystemProvider().userActivity();
        }
    }

    private void playPre() {
        if (platformFactory != null && deviceModuleFactory != null) {
            platformFactory.getPlayback().previous(nextPreResponseListener);
            deviceModuleFactory.getSystemProvider().userActivity();
        }
    }

    private void playOrPause() {
        if (platformFactory != null && deviceModuleFactory != null) {
            if (isPause) {
                platformFactory.getPlayback().play(playPauseResponseListener);
            } else {
                platformFactory.getPlayback().pause(playPauseResponseListener);
            }
            deviceModuleFactory.getSystemProvider().userActivity();
        }
    }

    private IResponseListener playPauseResponseListener = new IResponseListener() {
        @Override
        public void onSucceed(int statusCode) {
            if (statusCode == 204) {
                ToastUtil.show(getString(R.string.no_directive));
            }
        }

        @Override
        public void onFailed(String errorMessage) {
            ToastUtil.show(getString(R.string.request_error));
        }
    };

    private IResponseListener nextPreResponseListener = new IResponseListener() {
        @Override
        public void onSucceed(int statusCode) {
            if (statusCode == 204) {
                ToastUtil.show(getString(R.string.no_audio));
            }
        }

        @Override
        public void onFailed(String errorMessage) {
            ToastUtil.show(getString(R.string.request_error));
        }
    };

    private void updateView(Directive directive) {
        if (directive != null) {
            String rawMessage = directive.rawMessage;
            JSONObject rawJson = JsonUitls.getJSONObject(rawMessage);
            JSONObject payload = JsonUitls.getJSONObject(rawJson, "payload");
            JSONObject content = JsonUitls.getJSONObject(payload, "content");
            String audioItemType = JsonUitls.getString(content, "audioItemType");
            String titletxt = JsonUitls.getString(content, "title");
            String titleSubtext1 = JsonUitls.getString(content, "titleSubtext1");
            String titleSubtext2 = JsonUitls.getString(content, "titleSubtext2");

            JSONObject art = JsonUitls.getJSONObject(content, "art");
            String imgSrc = JsonUitls.getString(art, "src");

            switch (audioItemType) {
                case "MUSIC":
                    music_layout.setVisibility(View.VISIBLE);
                    otherContent_layout.setVisibility(View.GONE);
                    break;
                default:
                    music_layout.setVisibility(View.GONE);
                    otherContent_layout.setVisibility(View.VISIBLE);
                    break;
            }

            title.setText(titletxt);
            sub_title.setText(titleSubtext1);
            Glide.with(this).load(imgSrc).apply(new RequestOptions().circleCrop()).into(album_img);
        }
    }

    private void startAnim() {
        oa = ObjectAnimator.ofFloat(album_img, "rotation", 0, 359);
        oa.setRepeatCount(ObjectAnimator.INFINITE);
        oa.setRepeatMode(ObjectAnimator.RESTART);
        //线型插值器，动画匀速执行
        oa.setInterpolator(new LinearInterpolator());
        oa.setDuration(10 * 1000);
        oa.start();
    }

    private void stopAnim() {
        if (oa != null) {
            oa.cancel();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(serviceConnection);
        removeListner();
        if (oa != null) {
            oa.cancel();
        }
    }
}
