package viomi.com.viomiaiface.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.SystemClock;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.config.BroadcastAction;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.config.MURL;
import viomi.com.viomiaiface.model.WeatherBean;
import viomi.com.viomiaiface.utils.HttpApi;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiaiface.utils.SharedPreferencesUtil;
import viomi.com.viomiaiface.utils.WeatherIconUtil;
import viomi.com.viomiaiface.widget.LoadingView;

public class WeatherActivity extends BaseActivity {

    private ImageView back_icon;
    private ImageView weather_icon;
    private TextView weather_temp;
    private TextView weather_txt1;
    private TextView weather_txt2;
    private LoadingView loadingView;
    private TimerThread timerThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_weather);
        back_icon = findViewById(R.id.back_icon);
        weather_icon = findViewById(R.id.weather_icon);
        weather_temp = findViewById(R.id.weather_temp);
        weather_txt1 = findViewById(R.id.weather_txt1);
        weather_txt2 = findViewById(R.id.weather_txt2);
        loadingView = findViewById(R.id.loadingView);
        back_icon.setVisibility(View.GONE);
    }

    @Override
    protected void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LogUtils.e(TAG, "onNewIntent");

        String weather_directive = intent.getStringExtra("weather_directive");
        weather_directive = weather_directive == null ? "广州" : weather_directive;

        String cityName = getCityName(weather_directive);
        LogUtils.e(TAG, cityName + "");
        getWeather(cityName);

        if (timerThread != null) {
            timerThread.weakReference.clear();
            timerThread.interrupt();
        }

        timerThread = new TimerThread(this);
        timerThread.start();
    }

    @Override
    protected void init() {
        Intent intent = getIntent();
        String weather_directive = intent.getStringExtra("weather_directive");
        weather_directive = weather_directive == null ? "广州" : weather_directive;
        String cityName = getCityName(weather_directive);

        SharedPreferencesUtil.setWeatherCity(cityName);
        sendBroadcast(new Intent(BroadcastAction.WEATHERCITY));

        LogUtils.e(TAG, cityName + "");
        getWeather(cityName);

        timerThread = new TimerThread(this);
        timerThread.start();
    }

    private void getWeather(String cityName) {
        Map<String, String> map = new HashMap<>();
        map.put("key", "185c9117f8138");
        map.put("city", cityName);
        loadingView.setVisibility(View.VISIBLE);
        HttpApi.getRequestHandler(MURL.MOB_WEATHER, map, mHandler, HandlerMsgWhat.MSG0, HandlerMsgWhat.MSG1);
    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case HandlerMsgWhat.MSG0: {
                showResult(msg, true);
                break;
            }
            case HandlerMsgWhat.MSG1: {
                showResult(msg, false);
                break;
            }
            default:
                break;
        }
    }

    private void showResult(Message msg, boolean isSuccess) {
        loadingView.setVisibility(View.GONE);
        if (isSuccess) {
            String result = (String) msg.obj;
            JSONObject jsonObject = JsonUitls.getJSONObject(result);
            JSONArray resultArray = JsonUitls.getJSONArray(jsonObject, "result");
            if (resultArray.length() > 0) {
                JSONObject item = JsonUitls.getJSONObject(resultArray, 0);
                String date = JsonUitls.getString(item, "date");
                String distrct = JsonUitls.getString(item, "distrct");
                String pm25 = JsonUitls.getString(item, "pollutionIndex");
                String temperature = JsonUitls.getString(item, "temperature");
                temperature = temperature.replace("℃", "");
                JSONArray future = JsonUitls.getJSONArray(item, "future");
                if (future.length() > 0) {
                    JSONObject future_item = JsonUitls.getJSONObject(future, 0);
                    String weather_dayTime = JsonUitls.getString(future_item, "dayTime");
                    String weather_night = JsonUitls.getString(future_item, "night");
                    String weather = "".equals(weather_dayTime) ? weather_night : weather_dayTime;
                    WeatherBean bean = new WeatherBean(date, distrct, pm25, temperature, weather);
                    setView(bean);
                }
            }
        }
    }

    private void setView(WeatherBean bean) {
        weather_temp.setText(bean.getTemperature());
        weather_txt1.setText(bean.getDistrct() + "   " + bean.getWeather_dayTime() + "   PM2.5: " + bean.getPm25());
        weather_txt2.setText(bean.getDate());
        WeatherIconUtil.setWeatherIcon(weather_icon, bean.getWeather_dayTime());
    }


    private String getCityName(String raw) {
        ArrayList<String> cityList = getCityList(loadJson());
        String city1 = null;
        String city2 = null;
        for (int i = 0; i < cityList.size(); i++) {
            String cityName = cityList.get(i);
            if (raw.contains(cityName)) {
                city2 = cityName;
                if (raw.startsWith(cityName)) {
                    city1 = cityName;
                    break;
                }
            }
        }
        String cityName = city1 == null ? city2 : city1;
        LogUtils.e(TAG, "city=" + cityName);
        return cityName;
    }

    private ArrayList<String> getCityList(String cityJson) {
        ArrayList<String> cityList = new ArrayList<>();
        JSONObject json = JsonUitls.getJSONObject(cityJson);
        JSONArray result = JsonUitls.getJSONArray(json, "result");
        for (int i = 0; i < result.length(); i++) {
            JSONObject item = JsonUitls.getJSONObject(result, i);
            JSONArray city = JsonUitls.getJSONArray(item, "city");

            for (int j = 0; j < city.length(); j++) {
                JSONObject item_sub = JsonUitls.getJSONObject(city, j);
                String cityName = JsonUitls.getString(item_sub, "city");
                cityList.add(cityName);
                JSONArray district = JsonUitls.getJSONArray(item_sub, "district");

                for (int k = 0; k < district.length(); k++) {
                    JSONObject item_sub_sub = JsonUitls.getJSONObject(district, k);
                    String districtName = JsonUitls.getString(item_sub_sub, "district");
                    cityList.add(districtName);
                }
            }
        }
        return cityList;
    }

    private String loadJson() {
        InputStream is = null;
        BufferedReader bufr = null;
        StringBuilder builder = null;
        try {
            is = this.getAssets().open("city.json");
            bufr = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String line;
            builder = new StringBuilder();
            while ((line = bufr.readLine()) != null) {
                builder.append(line);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
                if (bufr != null) {
                    bufr.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return builder.toString();
        }
    }

    private static class TimerThread extends Thread {
        private WeakReference<WeatherActivity> weakReference;

        public TimerThread(WeatherActivity activity) {
            this.weakReference = new WeakReference<WeatherActivity>(activity);
        }

        @Override
        public void run() {
            SystemClock.sleep(15 * 1000);
            if (!isInterrupted()) {
                WeatherActivity mActivity = weakReference.get();
                if (mActivity != null) {
                    mActivity.finish();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timerThread != null) {
            timerThread.weakReference.clear();
            timerThread.interrupt();
        }
    }
}
