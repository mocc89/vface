package viomi.com.viomiaiface.activity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.view.SurfaceView;
import android.widget.FrameLayout;

import io.agora.rtc.RtcEngine;
import io.agora.rtc.video.VideoCanvas;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.service.VideoChatService;

public class VideoChat2Activity extends BaseActivity {

    private RtcEngine mRtcEngine;
    private ServiceConnection serviceConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_video_chat2);
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void init() {
        Intent intent = new Intent(this, VideoChatService.class);
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                VideoChatService videoChatService = ((VideoChatService.MyBinder) service).getService();
                mRtcEngine = videoChatService.getRtcEngine();

                enableStream();
                setupLocalVideo();
                setupRemoteVideo();


            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        };

        bindService(intent, serviceConnection, BIND_AUTO_CREATE);
    }

    private void enableStream() {
        mRtcEngine.enableAudio();
        mRtcEngine.enableVideo();
    }

    private void setupLocalVideo() {
        FrameLayout container =  findViewById(R.id.local_video_view_container);
        SurfaceView surfaceView = RtcEngine.CreateRendererView(getBaseContext());
        surfaceView.setZOrderMediaOverlay(true);
        container.addView(surfaceView);
        mRtcEngine.setupLocalVideo(new VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_ADAPTIVE, 0));
    }

    private void setupRemoteVideo() {
        FrameLayout container =  findViewById(R.id.remote_video_view_container);

        if (container.getChildCount() >= 1) {
            return;
        }

        SurfaceView surfaceView = RtcEngine.CreateRendererView(getBaseContext());
        container.addView(surfaceView);
        mRtcEngine.setupRemoteVideo(new VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_FIT, 0));

        surfaceView.setTag(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG0, 1000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRtcEngine.disableAudio();
        mRtcEngine.disableVideo();
        unbindService(serviceConnection);
    }

    @Override
    protected void handleMessage(Message msg) {
        onBackPressed();
    }
}
