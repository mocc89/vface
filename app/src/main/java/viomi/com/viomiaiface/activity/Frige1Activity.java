package viomi.com.viomiaiface.activity;

import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

public class Frige1Activity extends BaseActivity {
    final int REFRESH_PROPS = 12;
    final int REFRESH_USERDATA = 13;
    final int REFRESH_TIME = 14;

    final int DELAY_TIME = 3000;
    @InjectView(R.id.tv_device_name)
    TextView tvDeviceName;
    @InjectView(R.id.tv_time)
    TextView tvTime;
    @InjectView(R.id.tv_cang)
    TextView tvCang;
    @InjectView(R.id.tv_bian)
    TextView tvBian;
    @InjectView(R.id.tv_dong)
    TextView tvDong;
    @InjectView(R.id.tv_puri)
    TextView tvPuri;
    @InjectView(R.id.tv_puri1)
    TextView tvPuri1;
    @InjectView(R.id.tv_jing)
    TextView tvJing;
    @InjectView(R.id.tv_puri2)
    TextView tvPuri2;
    @InjectView(R.id.tv_zhineng)
    TextView tvZhineng;
    @InjectView(R.id.tv_puri3)
    TextView tvPuri3;
    @InjectView(R.id.tv_holiday)
    TextView tvHoliday;
    @InjectView(R.id.tv_puri4)
    TextView tvPuri4;
    AbstractDevice mDevice;
    String[] mParams = {"RCSetTemp", "CCSetTemp", "FCSetTemp", "FilterLifeBase", "FilterLife",
            "OneKeyClea", "Mode"};

    String[] mResults = new String[mParams.length];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void refreshView() {
        tvCang.setText(mResults[0]);
        tvBian.setText(mResults[1]);
        tvDong.setText(mResults[2]);
        int FilterLifeBase = Integer.parseInt(mResults[3]);
        int FilterLife = Integer.parseInt(mResults[4]);
        double left = (FilterLifeBase - FilterLife) / FilterLifeBase;
        BigDecimal value = new BigDecimal(left * 100).setScale(0, BigDecimal.ROUND_HALF_UP);
        tvPuri.setText(value + "%");
        if (mResults[5].equals("on")) {
            tvJing.setBackgroundDrawable(getResources().getDrawable(R.drawable.fri_jing_on));
        } else {
            tvJing.setBackgroundDrawable(getResources().getDrawable(R.drawable.fri_jing_off));
        }
        tvZhineng.setBackgroundDrawable(getResources().getDrawable(R.drawable.fri_zhineng_off));
        tvHoliday.setBackgroundDrawable(getResources().getDrawable(R.drawable.fri_holi_off));
        if (mResults[6].equals("smart")) {
            tvZhineng.setBackgroundDrawable(getResources().getDrawable(R.drawable.fri_zhineng_on));
        } else if (mResults[6].equals("holiday")) {
            tvHoliday.setBackgroundDrawable(getResources().getDrawable(R.drawable.fri_holi_on));
        } else {

        }
    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_frige1);
        ButterKnife.inject(this);
        mDevice = getIntent().getParcelableExtra("device");
    }

    @Override
    protected void initListener() {
    }

    @Override
    protected void init() {

    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        switch (msg.what) {
            case REFRESH_PROPS:
                getProps();
                break;
            case REFRESH_USERDATA:
                break;
            case REFRESH_TIME:
                tvDeviceName.setText(mDevice.getName());
                SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm");
                tvTime.setText(sdf1.format(new Date()));
                mHandler.sendEmptyMessageDelayed(REFRESH_TIME, 1000);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHandler.sendEmptyMessage(REFRESH_TIME);
        if (mDevice.isOnline()) {
            mHandler.sendEmptyMessage(REFRESH_PROPS);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHandler.removeCallbacksAndMessages(null);
    }

    private void getProps() {
        Operation.getInstance().sendFridgeCommand0(mDevice.getDeviceId(), Miconfig.MI_OAUTH_APP_ID + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                try {
                    JSONArray jsonArray = JsonUitls.getJSONArray(new JSONObject(result), "result");
                    if (jsonArray != null && jsonArray.length() > 0)
                        for (int i = 0; i < mResults.length; i++) {
                            mResults[i] = jsonArray.optString(i);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    refreshView();
                                }
                            });
                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mHandler.sendEmptyMessageDelayed(REFRESH_PROPS, DELAY_TIME);
            }

            @Override
            public void onFail(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                mHandler.sendEmptyMessageDelayed(REFRESH_PROPS, DELAY_TIME);
            }
        }, mParams);
    }


}
