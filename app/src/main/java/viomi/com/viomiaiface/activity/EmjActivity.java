package viomi.com.viomiaiface.activity;

import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;

public class EmjActivity extends BaseActivity {


    private ImageView iv;
    private TextView back;
    private List<Integer> list;
    private int i;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_emj);
        iv = findViewById(R.id.iv);
        back = findViewById(R.id.back);

        Glide.get(this).clearMemory();
        Glide.with(this).load(R.drawable.viomi_emj_1).into(iv);
    }

    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void init() {
        list = new ArrayList<>();
        list.add(R.drawable.viomi_emj_1);
        list.add(R.drawable.viomi_emj_2);
        list.add(R.drawable.viomi_emj_3);
        list.add(R.drawable.viomi_emj_4);
        list.add(R.drawable.viomi_emj_5);
        list.add(R.drawable.viomi_emj_6);

        mHandler.sendEmptyMessage(0);
    }

    @Override
    protected void handleMessage(Message msg) {
        i ++;
        Glide.get(this).clearMemory();
        Glide.with(this).load(list.get(i%list.size())).into(iv);
        mHandler.sendEmptyMessageDelayed(0, 5000);
    }
}
