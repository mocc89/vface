package viomi.com.viomiaiface.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.miot.common.abstractdevice.AbstractDevice;

import java.util.ArrayList;
import java.util.List;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.adapter.DeviceAdapter;
import viomi.com.viomiaiface.mijia.MiDeviceManager;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.utils.LogUtils;

public class DeviceListActivity extends BaseActivity {

    private MiDeviceManager mMiDeviceManager;
    private LocalBroadcastManager mBroadcastManager;
    private ImageView back_icon;
    private TextView title_view;
    private ListView deviceList;
    private List<AbstractDevice> dataList;
    private DeviceAdapter deviceAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_device_list);

        back_icon = findViewById(R.id.back_icon);
        title_view = findViewById(R.id.title_view);
        deviceList = findViewById(R.id.deviceList);
    }

    @Override
    protected void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void init() {
        title_view.setText(R.string.device_list);
        mBroadcastManager = LocalBroadcastManager.getInstance(this);
        mMiDeviceManager = MiDeviceManager.getInstance();
        dataList = new ArrayList<>();
        deviceAdapter = new DeviceAdapter(dataList, this);
        deviceList.setAdapter(deviceAdapter);
        deviceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AbstractDevice device = dataList.get(position);
                if (device.getDeviceModel().startsWith("viomi.hood")) {
                    Intent intent = new Intent(activity, HoodActivity.class);
                    intent.putExtra("device", device);
                    startActivity(intent);
                } else if (device.getDeviceModel().equals(Miconfig.YUNMI_WATERPURI_X5)) {

                    Intent intent = new Intent(activity, WaterpuriX5Activity.class);
                    intent.putExtra("device", device);
                    startActivity(intent);
                } else if (device.getDeviceModel().startsWith("viomi.fridge")) {
                    Intent intent = new Intent(activity, Frige1Activity.class);
                    intent.putExtra("device", device);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
        mMiDeviceManager.queryWanDeviceList();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mBroadcastManager.unregisterReceiver(mReceiver);
    }

    private void registerReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Miconfig.ACTION_BIND_SERVICE_SUCCEED);
        filter.addAction(Miconfig.ACTION_BIND_SERVICE_FAILED);
        filter.addAction(Miconfig.ACTION_DISCOVERY_DEVICE_SUCCEED);
        filter.addAction(Miconfig.ACTION_DISCOVERY_DEVICE_FAILED);
        mBroadcastManager.registerReceiver(mReceiver, filter);
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "action: " + intent.getAction());
            switch (intent.getAction()) {
                //TODO: for service restart
                case Miconfig.ACTION_BIND_SERVICE_SUCCEED:
                    break;
                case Miconfig.ACTION_BIND_SERVICE_FAILED:
                    break;

                case Miconfig.ACTION_DISCOVERY_DEVICE_SUCCEED:

                    LogUtils.e(TAG, "discovery device succeed");
                    dataList.clear();
                    dataList.addAll(mMiDeviceManager.getWanDevices());
                    deviceAdapter.notifyDataSetChanged();
                    break;
                case Miconfig.ACTION_DISCOVERY_DEVICE_FAILED:
                    LogUtils.e(TAG, "discovery device failed");
                    break;
            }
        }
    };
}
