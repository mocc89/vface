package viomi.com.viomiaiface.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_OFF;

/**
 * Created by linhs on 2018/1/8 0008.
 */

public class OutdoorSettingActivity extends BaseActivity {

    TextView title_view;
    private ImageView back_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initView() {
        setContentView(R.layout.outdoor_activity);
        title_view = (TextView) findViewById(R.id.title_view);
        back_icon = findViewById(R.id.back_icon);
    }

    @Override
    protected void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void init() {
        title_view.setText(this.getResources().getString(R.string.outdoor_set));
    }

    public void setAddrClik(View view) {

    }

    public void setPianhaoClik(View view) {

    }
}
