package viomi.com.viomiaiface.config;

/**
 * Created by Mocc on 2018/1/9
 */

public class HandlerMsgWhat {

    public static final int MSG0 = 0;
    public static final int MSG1 = 1;
    public static final int MSG2 = 2;
    public static final int MSG3 = 3;
    public static final int MSG4 = 4;
    public static final int MSG5 = 5;
    public static final int MSG6 = 6;
    public static final int MSG7 = 7;
    public static final int MSG8 = 8;
    public static final int MSG9 = 9;
    public static final int MSG10 = 10;
    public static final int MSG11 = 11;
    public static final int MSG12 = 12;
    public static final String FEEBACK = "feeback";
    public static final String TEMP = "temp";

}
