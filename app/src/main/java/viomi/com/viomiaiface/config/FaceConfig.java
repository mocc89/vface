package viomi.com.viomiaiface.config;

/**
 * Created by Mocc on 2018/1/3
 */

public class FaceConfig {

    public final static boolean LOGOUT = true;
    public final static boolean RELEASE = true;

    public final static String USERFILENAME = "userinfo";

    //测试服更新参数
//    public final static String UPDATEENVIRONMENT = "viomi_test";
    //正式服更新参数
    public final static String UPDATEENVIRONMENT = "viomi";

    //当前版本号
    public final static String CURRENTVERSION = "101";

}
