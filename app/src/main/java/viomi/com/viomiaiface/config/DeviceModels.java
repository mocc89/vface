package viomi.com.viomiaiface.config;

/**
 * Created by hailang on 2018/2/24 0024.
 */

public class DeviceModels {
    public static final String YUNMI_WATERPURI_V1 = "yunmi.waterpuri.v1";      // V1乐享版 人工智能系列
    public static final String YUNMI_WATERPURI_V2 = "yunmi.waterpuri.v2";      // V1尊享版 人工智能系列
    public static final String YUNMI_WATERPURI_S1 = "yunmi.waterpuri.s1";      // S1优享版 智能3合1系列
    public static final String YUNMI_WATERPURI_S2 = "yunmi.waterpuri.s2";      // S1标准版 智能3合1系列
    public static final String YUNMI_WATERPURI_C1 = "yunmi.waterpuri.c1";      // C1厨上版 新鲜水系列
    public static final String YUNMI_WATERPURI_C2 = "yunmi.waterpuri.c2";      // C1厨下版 新鲜水系列
    public static final String YUNMI_WATERPURI_X3 = "yunmi.waterpuri.x3";      //即热直饮净水器X3 (100G)
    public static final String YUNMI_WATERPURI_X5 = "yunmi.waterpuri.x5";      //即热直饮净水器X5 (400G)

    public static final String YUNMI_WATERPURIFIER_V1 = "yunmi.waterpurifier.v1";      // 小米净水器厨上版
    public static final String YUNMI_WATERPURIFIER_V2 = "yunmi.waterpurifier.v2";      // 小米净水器厨上版
    public static final String YUNMI_WATERPURIFIER_V3 = "yunmi.waterpurifier.v3";      // 小米净水器厨下
    public static final String YUNMI_WATERPURI_LX2 = "yunmi.waterpuri.lx2";      // 小米净水器厨上版
    public static final String YUNMI_WATERPURI_LX3 = "yunmi.waterpuri.lx3";      //小米净水器厨下版
    public static final String YUNMI_WATERPURI_LX6 = "yunmi.waterpuri.lx6";

    public static final String VIOMI_FRIDGE_U1 = "viomi.fridge.u1";      //小鲜互联
    public static final String VIOMI_FRIDGE_U2 = "viomi.fridge.u2";      // 456小屏冰箱
    public static final String VIOMI_FRIDGE_V1 = "viomi.fridge.v1";      // iLive语音版
    public static final String VIOMI_FRIDGE_V2 = "viomi.fridge.v2";      // 冰箱iLive四门语音版
    public static final String VIOMI_FRIDGE_W1 = "viomi.fridge.w1";      // 米家冰箱
    public static final String VIOMI_FRIDGE_V3 = "viomi.fridge.v3";      //   462大屏金属门冰箱
    public static final String VIOMI_FRIDGE_V4 = "viomi.fridge.v4";      //   455大屏玻璃门冰箱
    public static final String VIOMI_FRIDGE_X1= "viomi.fridge.x1";      //   云米三门大屏冰箱X1
    public static final String VIOMI_FRIDGE_X2 = "viomi.fridge.x2";      //   云米四门大屏冰箱428
    public static final String VIOMI_FRIDGE_X3 = "viomi.fridge.x3";      //   云米大屏冰箱462
    public static final String VIOMI_FRIDGE_X4 = "viomi.fridge.x4";      //   云米对开门大屏冰箱456

    public static final String VIOMI_HOOD_T8 = "viomi.hood.t8";      //标准型T型机
    public static final String VIOMI_HOOD_TA8 = "viomi.hood.ta8";      //标准型塔型机
    public static final String VIOMI_HOOD_C6 = "viomi.hood.c6";      //经济型侧吸机
    public static final String VIOMI_HOOD_C9 = "viomi.hood.c9";      //尊贵型侧吸机
    public static final String VIOMI_HOOD_C1 = "viomi.hood.c1";      //云米智能油烟机Cross
    public static final String VIOMI_HOOD_H1 = "viomi.hood.h1";      //
    public static final String VIOMI_HOOD_H2 = "viomi.hood.h2";      //
    public static final String VIOMI_HOOD_A4 = "viomi.hood.a4";      //
    public static final String VIOMI_HOOD_A5 = "viomi.hood.a5";      //
    public static final String VIOMI_HOOD_A6 = "viomi.hood.a6";      //
    public static final String VIOMI_HOOD_A7 = "viomi.hood.a7";      //

    public static final String YUNMI_KETTLE_R1 = "yunmi.kettle.r1";      //云米智能即热饮水吧（MINI）
    public static final String YUNMI_PLMACHINE_MG2 = "yunmi.plmachine.mg2";      //云米MG2型即热管线机
    public static final String YUNMI_WATERPURI_1X5 = "yunmi.waterpuri.lx5";      //小米净水器2

    public static final String YUNMI_WATERPURI_M2 = "yunmi.waterpuri.m2";      //米家净水器M2

    public static final String YUNMI_DISHWASHER_V1 = "viomi.dishwasher.v01";      //云米智能洗碗机
}
