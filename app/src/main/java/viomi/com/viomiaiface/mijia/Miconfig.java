package viomi.com.viomiaiface.mijia;

/**
 * Created by Mocc on 2018/1/18
 */

public class Miconfig {

    public static final long MI_OAUTH_APP_ID = 2882303761517454408L;
    public static final String MI_OAUTH_APP_KEY = "5891745422408";
//    public static final String MI_OAUTH_REDIRECT_URI = "https://auth.mi-ae.net/services/mi/login/oAuth/callback.json";


    /*
    * 设备型号
    * */
    public static final String YUNMI_WATERPURI_V1 = "yunmi.waterpuri.v1";// V1 乐享版 人工智能系列
    public static final String YUNMI_WATERPURI_V2 = "yunmi.waterpuri.v2";// V1 尊享版 人工智能系列
    public static final String YUNMI_WATERPURI_S1 = "yunmi.waterpuri.s1";// S1 优享版 智能 3 合 1 系列
    public static final String YUNMI_WATERPURI_S2 = "yunmi.waterpuri.s2";// S1 标准版 智能 3 合 1 系列
    public static final String YUNMI_WATERPURI_C1 = "yunmi.waterpuri.c1";// C1 厨上版 新鲜水系列
    public static final String YUNMI_WATERPURI_C2 = "yunmi.waterpuri.c2";// C1 厨下版 新鲜水系列
    public static final String YUNMI_WATERPURI_X3 = "yunmi.waterpuri.x3";// 即热直饮净水器 X3 (100G)
    public static final String YUNMI_WATERPURI_X5 = "yunmi.waterpuri.x5";// 即热直饮净水器 X5 (400G)

    public static final String YUNMI_WATERPURIFIER_V1 = "yunmi.waterpurifier.v1";// 小米净水器厨上版
    public static final String YUNMI_WATERPURIFIER_V2 = "yunmi.waterpurifier.v2";// 小米净水器厨上版
    public static final String YUNMI_WATERPURIFIER_V3 = "yunmi.waterpurifier.v3";// 小米净水器厨下
    public static final String YUNMI_WATERPURI_LX2 = "yunmi.waterpuri.lx2";// 小米净水器厨上版
    public static final String YUNMI_WATERPURI_LX3 = "yunmi.waterpuri.lx3";// 小米净水器厨下版

    public static final String YUNMI_KETTLE_R1 ="yunmi.kettle.r1";// 云米智能即热饮水吧（MINI）
    public static final String YUNMI_PL_MACHINE_MG2 ="yunmi.plmachine.mg2";// 云米 MG2 型即热管线机

    public static final String VIOMI_FRIDGE_V1 = "viomi.fridge.v1";// 小鲜互联 云米智能冰箱三门 绿联主控
    public static final String VIOMI_FRIDGE_V2 = "viomi.fridge.v2";// 小鲜互联 云米智能冰箱四门 双鹿主控
    public static final String VIOMI_FRIDGE_V3 = "viomi.fridge.v3";// 云米 462 大屏金属门冰箱
    public static final String VIOMI_FRIDGE_V4 = "viomi.fridge.v4";// 云米 455 大屏玻璃门冰箱
    public static final String VIOMI_FRIDGE_U1 = "viomi.fridge.u1";// 小鲜互联

    public static final String VIOMI_HOOD_A5 = "viomi.hood.a5";// 标准型 T 型机
    public static final String VIOMI_HOOD_T8 = "viomi.hood.t8";// 标准型 T 型机
    public static final String VIOMI_HOOD_A6 = "viomi.hood.a6";// 标准型塔型机
    public static final String VIOMI_HOOD_C6 = "viomi.hood.c6";// 经济型侧吸机
    public static final String VIOMI_HOOD_A4 = "viomi.hood.a4";// 经济型侧吸机
    public static final String VIOMI_HOOD_A7 = "viomi.hood.a7";// 尊贵型侧吸机
    public static final String VIOMI_HOOD_C1 = "viomi.hood.c1";// 云米智能油烟机 Cross
    public static final String VIOMI_HOOD_H1 = "viomi.hood.h1";// 云米智能油烟机 Hurri 标准
    public static final String VIOMI_HOOD_H2 = "viomi.hood.h2";// 云米智能油烟机 Hurri 尊享


    /*
    * 设备协议url
    * */
    public static final String YUNMI_WATERPURI_V1_URL = "device/ddd_WaterPuri_V1.xml";
    public static final String YUNMI_WATERPURI_V2_URL = "device/ddd_WaterPuri_V2.xml";
    public static final String YUNMI_WATERPURI_S1_URL = "device/ddd_WaterPuri_S1.xml";
    public static final String YUNMI_WATERPURI_S2_URL = "device/ddd_WaterPuri_S2.xml";
    public static final String YUNMI_WATERPURI_C1_URL = "device/ddd_WaterPuri_C1.xml";
    public static final String YUNMI_WATERPURI_C2_URL = "device/ddd_WaterPuri_C2.xml";
    public static final String YUNMI_WATERPURI_X3_URL = "device/ddd_WaterPuri_X3.xml";
    public static final String YUNMI_WATERPURI_X5_URL = "device/ddd_WaterPuri_X5.xml";
    public static final String YUNMI_WATERPURIFIER_URL = "device/ddd_WaterPurifier.xml";
    public static final String VIOMI_FRIDGE_V1_URL = "device/ddd_Fridge_V1.xml";
    public static final String VIOMI_HOOD_T8_URL = "device/Viomi_Hood_T8.xml";
    public static final String VIOMI_HOOD_A6_URL = "device/Viomi_Hood_A6.xml";
    public static final String VIOMI_HOOD_A5_URL = "device/Viomi_Hood_A5.xml";
    public static final String VIOMI_HOOD_A4_URL = "device/Viomi_Hood_A4.xml";
    public static final String VIOMI_HOOD_C6_URL = "device/Viomi_Hood_C6.xml";
    public static final String VIOMI_HOOD_A7_URL = "device/Viomi_Hood_A7.xml";
    public static final String VIOMI_HOOD_C1_URL = "device/Viomi_Hood_C1.xml";
    public static final String VIOMI_HOOD_H1_URL = "device/Viomi_Hood_H1.xml";
    public static final String VIOMI_HOOD_H2_URL = "device/Viomi_Hood_H2.xml";
    public static final String YUNMI_KETTLE_R1_URL = "device/Viomi_Kettle_R1.xml";
    public static final String YUNMI_PL_MACHINE_MG2_URL = "device/Viomi_Plmachine_Mg2.xml";


    public static final String ACTION_BIND_SERVICE_SUCCEED = "viomi.com.viomiaiface.action.BIND_SERVICE_SUCCEED";
    public static final String ACTION_BIND_SERVICE_FAILED = "viomi.com.viomiaiface.action.BIND_SERVICE_FAILED";

    public static final String ACTION_DISCOVERY_DEVICE_SUCCEED = "viomi.com.viomiaiface.action.DISCOVERY_DEVICE_SUCCEED";
    public static final String ACTION_DISCOVERY_DEVICE_FAILED = "viomi.com.viomiaiface.action.DISCOVERY_DEVICE_FAILED";
}
