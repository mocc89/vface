package viomi.com.viomiaiface.utils;

import android.widget.ImageView;

import viomi.com.viomiaiface.R;


/**
 * Created by viomi on 2017/3/6.
 */

public class WeatherIconUtil {

    public static void setWeatherIcon(ImageView iv, String type) {

        switch (type) {


            case "晴": {
                iv.setImageResource(R.drawable.weather_ic_v_01);
                break;
            }
            case "阴": {
                iv.setImageResource(R.drawable.weather_ic_v_02);
                break;
            }
            case "多云": {
                iv.setImageResource(R.drawable.weather_ic_v_03);
                break;
            }
            case "小雨": {
                iv.setImageResource(R.drawable.weather_ic_v_04);
                break;
            }
            case "小到中雨": {
                iv.setImageResource(R.drawable.weather_ic_v_05);
                break;
            }
            case "中雨": {
                iv.setImageResource(R.drawable.weather_ic_v_05);
                break;
            }
            case "阵雨": {
                iv.setImageResource(R.drawable.weather_ic_v_05);
                break;
            }
            case "冻雨": {
                iv.setImageResource(R.drawable.weather_ic_v_05);
                break;
            }
            case "中到大雨": {
                iv.setImageResource(R.drawable.weather_ic_v_06);
                break;
            }
            case "大雨": {
                iv.setImageResource(R.drawable.weather_ic_v_06);
                break;
            }
            case "大到暴雨": {
                iv.setImageResource(R.drawable.weather_ic_v_06);
                break;
            }
            case "暴雨": {
                iv.setImageResource(R.drawable.weather_ic_v_06);
                break;
            }
            case "暴雨到大暴雨": {
                iv.setImageResource(R.drawable.weather_ic_v_06);
                break;
            }
            case "大暴雨": {
                iv.setImageResource(R.drawable.weather_ic_v_06);
                break;
            }
            case "大暴雨到特大暴雨": {
                iv.setImageResource(R.drawable.weather_ic_v_06);
                break;
            }
            case "特大暴雨": {
                iv.setImageResource(R.drawable.weather_ic_v_06);
                break;
            }
            case "雷阵雨": {
                iv.setImageResource(R.drawable.weather_ic_v_07);
                break;
            }
            case "小雪": {
                iv.setImageResource(R.drawable.weather_ic_v_08);
                break;
            }
            case "雨夹雪": {
                iv.setImageResource(R.drawable.weather_ic_v_08);
                break;
            }
            case "小到中雪": {
                iv.setImageResource(R.drawable.weather_ic_v_09);
                break;
            }
            case "中雪": {
                iv.setImageResource(R.drawable.weather_ic_v_09);
                break;
            }
            case "阵雪": {
                iv.setImageResource(R.drawable.weather_ic_v_09);
                break;
            }
            case "中到大雪": {
                iv.setImageResource(R.drawable.weather_ic_v_10);
                break;
            }
            case "大雪": {
                iv.setImageResource(R.drawable.weather_ic_v_10);
                break;
            }
            case "大到暴雪": {
                iv.setImageResource(R.drawable.weather_ic_v_10);
                break;
            }
            case "暴雪": {
                iv.setImageResource(R.drawable.weather_ic_v_10);
                break;
            }
            case "冰雹": {
                iv.setImageResource(R.drawable.weather_ic_v_11);
                break;
            }
            case "雷阵雨伴有冰雹": {
                iv.setImageResource(R.drawable.weather_ic_v_11);
                break;
            }
            case "霾": {
                iv.setImageResource(R.drawable.weather_ic_v_12);
                break;
            }
            case "雾": {
                iv.setImageResource(R.drawable.weather_ic_v_13);
                break;
            }
            case "沙尘暴": {
                iv.setImageResource(R.drawable.weather_ic_v_14);
                break;
            }
            case "扬沙": {
                iv.setImageResource(R.drawable.weather_ic_v_14);
                break;
            }
            case "强沙尘暴": {
                iv.setImageResource(R.drawable.weather_ic_v_14);
                break;
            }
            case "浮尘": {
                iv.setImageResource(R.drawable.weather_ic_v_14);
                break;
            }
            case "": {
                iv.setImageResource(R.drawable.weather_ic_v_00);
                break;
            }
            case "无": {
                iv.setImageResource(R.drawable.weather_ic_v_00);
                break;
            }
            default: {
                if (type == null) {
                    iv.setImageResource(R.drawable.weather_ic_v_00);
                    return;
                }

                if (type.contains("小雪")) {
                    iv.setImageResource(R.drawable.weather_ic_v_08);
                    return;
                }
                if (type.contains("中雪")) {
                    iv.setImageResource(R.drawable.weather_ic_v_09);
                    return;
                }
                if (type.contains("大雪")) {
                    iv.setImageResource(R.drawable.weather_ic_v_10);
                    return;
                }
                if (type.contains("暴雪")) {
                    iv.setImageResource(R.drawable.weather_ic_v_10);
                    return;
                }
                if (type.contains("雪")) {
                    iv.setImageResource(R.drawable.weather_ic_v_09);
                    return;
                }


                if (type.contains("小雨")) {
                    iv.setImageResource(R.drawable.weather_ic_v_04);
                    return;
                }
                if (type.contains("中雨")) {
                    iv.setImageResource(R.drawable.weather_ic_v_05);
                    return;
                }
                if (type.contains("大雨")) {
                    iv.setImageResource(R.drawable.weather_ic_v_06);
                    return;
                }
                if (type.contains("暴雨")) {
                    iv.setImageResource(R.drawable.weather_ic_v_06);
                    return;
                }

                if (type.contains("雨")) {
                    iv.setImageResource(R.drawable.weather_ic_v_05);
                    return;
                }

                if (type.contains("冰雹")) {
                    iv.setImageResource(R.drawable.weather_ic_v_11);
                    return;
                }
                if (type.contains("尘")) {
                    iv.setImageResource(R.drawable.weather_ic_v_14);
                    return;
                }
                if (type.contains("霾")) {
                    iv.setImageResource(R.drawable.weather_ic_v_12);
                    return;
                }
                if (type.contains("雾")) {
                    iv.setImageResource(R.drawable.weather_ic_v_13);
                    return;
                }
                if (type.contains("阴")) {
                    iv.setImageResource(R.drawable.weather_ic_v_03);
                    return;
                }
                if (type.contains("云")) {
                    iv.setImageResource(R.drawable.weather_ic_v_02);
                    return;
                }

                iv.setImageResource(R.drawable.weather_ic_v_00);
                break;
            }
        }
    }
}
