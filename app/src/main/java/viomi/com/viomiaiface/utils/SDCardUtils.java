package viomi.com.viomiaiface.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.StatFs;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class SDCardUtils {

    /**
     * 判断SD卡是否挂载
     *
     * @return
     */
    public static boolean isSDCardMounted() {
        // 判断SD卡是否挂载
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    /**
     * 获得SD卡根目录
     *
     * @return
     */
    public static String getSDCardRootDir() {
        if (isSDCardMounted()) {
            return Environment.getExternalStorageDirectory().getAbsolutePath();
        }
        return null;
    }

    /**
     * 获得SD卡总大小
     *
     * @return 返回MB
     */
    public static long getSDCardTotalSize() {
        if (isSDCardMounted()) {
            StatFs fs = new StatFs(getSDCardRootDir());
            long blockCountLong = fs.getBlockCount();
            long blockSizeLong = fs.getBlockSize();
            return blockCountLong * blockSizeLong / 1024 / 1024;
        }
        return 0;
    }

    /**
     * 获得SD卡有效空间大小，返回MB
     *
     * @return
     */
    public static long getSDCardAvailableSize() {
        if (isSDCardMounted()) {
            StatFs fs = new StatFs(getSDCardRootDir());
            long availableBlocks = fs.getAvailableBlocks();
            long blockSize = fs.getBlockSize();
            return availableBlocks * blockSize / 1024 / 1024;
        }
        return 0;
    }

    /**
     * 获得SD卡剩余空间大小，返回MB
     *
     * @return
     */
    public static long getSDCardFreeSize() {
        return getSDCardFreeSizeBit()/1024/1024;
        //if (isSDCardMounted()) {
        //    StatFs fs = new StatFs(getSDCardRootDir());
        //    long freeBlocks = fs.getFreeBlocks();
        //    long blockSize = fs.getBlockSize();
        //    return freeBlocks * blockSize / 1024 / 1024;
        //}
        //return 0;
    }

    public static long getSDCardFreeSizeBit() {
        if (isSDCardMounted()) {
            StatFs fs = new StatFs(getSDCardRootDir());
            long freeBlocks = fs.getFreeBlocks();
            long blockSize = fs.getBlockSize();
            return freeBlocks * blockSize;
        }
        return 0;
    }

    /**
     * bit转换为Kb/M/GB显示
     *
     * @return
     */
    public static String bit2KbMGB(long bit) {
        String temp = "";
        if (bit > 1024L * 1024 * 1024)
            temp = String.format("%.2f", bit / 1024f / 1024 / 1024) + "GB";
        else if (bit > 1024L * 1024)
            temp = String.format("%.2f", bit / 1024f / 1024) + "M";
        else if (bit > 1024)
            temp = String.format("%.2f", bit / 1024f) + "Kb";
        else
            temp = "0Kb";
        return temp;
    }

    /**
     * 往SD卡公有目录中写文件
     *
     * @param data     数据源
     * @param type     公有目录
     * @param fileName 文件名
     * @return
     */
    public static boolean saveData2PublicDir(byte[] data, String type, String fileName) {
        if (isSDCardMounted()) {
            BufferedOutputStream bos = null;
            File file = new File(Environment.getExternalStoragePublicDirectory(type), fileName);
            try {
                bos = new BufferedOutputStream(new FileOutputStream(file));
                bos.write(data);
                bos.flush();
                return true;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (bos != null) {
                    try {
                        bos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return false;
    }

    /**
     * 往自定义目录保存文件
     *
     * @param data     数据源
     * @param path     文件路径
     * @param fileName 文件名
     * @return
     */
    public static boolean saveData2CustomDir(byte[] data, String path, String fileName) {
        if (isSDCardMounted()) {
            BufferedOutputStream bos = null;
            File folder = new File(getSDCardRootDir() + File.separator + path);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            File file = new File(folder, fileName);
            try {
                bos = new BufferedOutputStream(new FileOutputStream(file));
                bos.write(data);
                bos.flush();
                return true;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (bos != null) {
                    try {
                        bos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        return false;
    }

    /**
     * 往SD卡的私有Files目录下保存文件
     *
     * @param data     数据源
     * @param type     文件夹
     * @param fileName 文件名称
     * @param context  上下文
     * @return
     */
    public static boolean saveData2SDCardPrivateFilesDir(byte[] data, String type, String fileName, Context context) {
        if (isSDCardMounted()) {
            File externalFilesDir = context.getExternalFilesDir(type);
            File file = new File(externalFilesDir, fileName);
            BufferedOutputStream bos = null;
            try {
                bos = new BufferedOutputStream(new FileOutputStream(file));
                bos.write(data);
                bos.flush();
                return true;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (bos != null) {
                    try {
                        bos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return false;
    }

    /**
     * 往SD卡的私有Cache目录下保存文件
     *
     * @param data     数据源
     * @param fileName 文件名
     * @param context  上下文
     * @return
     */
    public static boolean saveData2SDCardPrivateCacheDir(byte[] data, String fileName, Context context) {
        if (isSDCardMounted()) {
            File externalCacheDir = context.getExternalCacheDir();
            File file = new File(externalCacheDir, fileName);
            BufferedOutputStream bos = null;
            try {
                bos = new BufferedOutputStream(new FileOutputStream(file));
                bos.write(data);
                bos.flush();
                return true;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (bos != null) {
                    try {
                        bos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return false;
    }

    /**
     * 往SD卡的私有目录的Cache文件夹下保存Bitmap图像
     *
     * @param data
     * @param fileName
     * @param context
     * @return
     */
    public static boolean saveBitmap2SDCardCacheDir(byte[] data, String fileName, Context context) {
        if (isSDCardMounted()) {
            BufferedOutputStream bos = null;
            try {
                File file = new File(context.getExternalCacheDir(), fileName);
                bos = new BufferedOutputStream(new FileOutputStream(file));
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                if (fileName.contains(".png") || fileName.contains(".PNG")) {
                    // 第一个参数表示保存的图片类型，第二个参数表示图片质量，取值0-100，值越大，质量越高，第三个参数表示图片输出流
                    // 如果第一个参数为CompressFormat.PNG，那么第二个参数无效
                    bitmap.compress(CompressFormat.PNG, 100, bos);
                } else {
                    bitmap.compress(CompressFormat.JPEG, 100, bos);
                }
                return true;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                if (bos != null) {
                    try {
                        bos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return false;
    }

    /**
     * 从SD卡读取指定文件
     *
     * @param filePath
     * @return
     */
    public static byte[] getDataFromSDCard(String filePath) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream is = null;
        File file = new File(filePath);
        try {
            is = new FileInputStream(file);
            int len;
            byte[] buf = new byte[1024];
            while ((len = is.read(buf)) != -1) {
                baos.write(buf, 0, len);
                baos.flush();
            }
            return baos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     * 从SD卡读取Bitmap并返回
     *
     * @param filePath
     * @return
     */
    public static Bitmap getBitmapFromSDCard(String filePath) {
        if (isSDCardMounted()) {
            try {
                return BitmapFactory.decodeStream(new FileInputStream(new File(filePath)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * 获得SD卡公有目录路径
     *
     * @param type
     * @return
     */
    public static String getSDCardPublicDir(String type) {
        if (isSDCardMounted()) {
            return Environment.getExternalStoragePublicDirectory(type).getAbsolutePath();
        }
        return null;
    }

    /**
     * 获取SD卡私有目录Cache文件夹路径
     *
     * @param context
     * @return
     */
    public static String getSDCardPrivateCacheDir(Context context) {
        if (isSDCardMounted()) {
            return context.getExternalCacheDir().getAbsolutePath();
        }
        return null;
    }

    /**
     * 获取SD卡私有目录中Files文件夹路径
     *
     * @param type
     * @param context
     * @return
     */
    public static String getSDCardPrivateFilesDir(String type, Context context) {
        if (isSDCardMounted()) {
            return context.getExternalFilesDir(type).getAbsolutePath();
        }
        return null;
    }

    /**
     * 判断一个文件是否存在
     *
     * @param filePath
     * @return
     */
    public static boolean isFileExists(String filePath) {
        if (isSDCardMounted()) {
            return new File(filePath).exists();
        }
        return false;
    }

    /**
     * 删除一个文件
     *
     * @param filePath
     * @return
     */
    public static boolean removeFileFromSDCard(String filePath) {
        if (isSDCardMounted()) {
            File file = new File(filePath);
            if (file.exists()) {
                return deleteFile(file);
                // return file.delete();
            }
        }
        return false;
    }

    private static boolean deleteFile(File file) {
        if (file.isDirectory()) {
            File[] listFiles = file.listFiles();
            for (int i = 0; i < listFiles.length; i++) {
                if (listFiles[i].isDirectory()) {
                    deleteFile(listFiles[i]);
                } else {
                    listFiles[i].delete();
                }
            }
            file.delete();
        } else {
            return file.delete();
        }
        return false;
    }
}
