package viomi.com.viomiaiface.utils;

import android.content.SharedPreferences;

import viomi.com.viomiaiface.base.BaseApplication;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mocc on 2018/1/10
 */

public class SharedPreferencesUtil {

    private static SharedPreferences sp = BaseApplication.getAppContext().getSharedPreferences("viomiAIFace", MODE_PRIVATE);
    private static SharedPreferences.Editor editor = sp.edit();


    public static void setValue(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public static String getValue(String key,String defalutValue) {
        return sp.getString(key, defalutValue);
    }

    public static void setWeatherCity(String city){
        setValue("WeatherCity", city);
    }

    public static String getWeatherCity(){
        return getValue("WeatherCity", "广州");
    }
}
