package viomi.com.viomiaiface.utils;

import viomi.com.viomiaiface.config.DeviceModels;

/**
 * Created by hailang on 2018/2/25 0025.
 */

public class FrigeUtil {

    public static boolean containBianwen(String model) {
        switch (model) {
            case DeviceModels.VIOMI_FRIDGE_U2:
            case DeviceModels.VIOMI_FRIDGE_V3:
            case DeviceModels.VIOMI_FRIDGE_V4:
            case DeviceModels.VIOMI_FRIDGE_X3:
            case DeviceModels.VIOMI_FRIDGE_X4:
                return false;
        }
        return true;

    }

    public static boolean hasBianwenScene(String model) {
        switch (model) {
            case DeviceModels.VIOMI_FRIDGE_U1:
            case DeviceModels.VIOMI_FRIDGE_U2:
            case DeviceModels.VIOMI_FRIDGE_W1:

            case DeviceModels.VIOMI_FRIDGE_V3:
            case DeviceModels.VIOMI_FRIDGE_V4:
            case DeviceModels.VIOMI_FRIDGE_X3:

            case DeviceModels.VIOMI_FRIDGE_X4:
                return false;
        }
        return true;

    }
}
