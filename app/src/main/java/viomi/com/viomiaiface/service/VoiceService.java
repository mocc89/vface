package viomi.com.viomiaiface.service;

import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.activity.ConversationActivity;
import viomi.com.viomiaiface.activity.EmjActivity;
import viomi.com.viomiaiface.activity.MediaPlayActivity;
import viomi.com.viomiaiface.activity.SettingActivity;
import viomi.com.viomiaiface.activity.ShowScreenActivity;
import viomi.com.viomiaiface.activity.WeatherActivity;
import viomi.com.viomiaiface.base.BaseService;
import viomi.com.viomiaiface.callback.ConversationUpdateCallback;
import viomi.com.viomiaiface.callback.DcsEventCallback;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.dueros.androidsystemimpl.PlatformFactoryImpl;
import viomi.com.viomiaiface.dueros.api.IOauth;
import viomi.com.viomiaiface.dueros.api.OauthImpl;
import viomi.com.viomiaiface.dueros.devicemodule.screen.message.RenderVoiceInputTextPayload;
import viomi.com.viomiaiface.dueros.devicemodule.system.HandleDirectiveException;
import viomi.com.viomiaiface.dueros.devicemodule.voiceinput.VoiceInputDeviceModule;
import viomi.com.viomiaiface.dueros.devicemodule.voiceoutput.VoiceOutputDeviceModule;
import viomi.com.viomiaiface.dueros.framework.BaseDeviceModule;
import viomi.com.viomiaiface.dueros.framework.DcsFramework;
import viomi.com.viomiaiface.dueros.framework.DeviceModuleFactory;
import viomi.com.viomiaiface.dueros.framework.message.Directive;
import viomi.com.viomiaiface.dueros.http.HttpConfig;
import viomi.com.viomiaiface.dueros.systeminterface.IMediaPlayer;
import viomi.com.viomiaiface.dueros.systeminterface.IPlatformFactory;
import viomi.com.viomiaiface.dueros.systeminterface.IWakeUp;
import viomi.com.viomiaiface.dueros.util.CommonUtil;
import viomi.com.viomiaiface.dueros.util.LogUtil;
import viomi.com.viomiaiface.dueros.util.NetWorkUtil;
import viomi.com.viomiaiface.dueros.wakeup.WakeUp;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.model.ConversationBean;
import viomi.com.viomiaiface.model.ConversationType;
import viomi.com.viomiaiface.utils.BaiduTTSTool;
import viomi.com.viomiaiface.utils.FrigeUtil;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiaiface.utils.ToastUtil;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.FridgeMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.FridgeTempMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.HoodWindMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.OvenMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_OFF;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashCusMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashMacProMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashProMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashingDryRmpMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashingTemp;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashingTimes;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

import static viomi.com.viomiaiface.baidutts.MainHandlerConstant.UI_CHANGE_INPUT_TEXT_SELECTION;

public class VoiceService extends BaseService {

    private static String TAG = VoiceService.class.getSimpleName();

    private DcsFramework dcsFramework;
    private DeviceModuleFactory deviceModuleFactory;
    private IPlatformFactory platformFactory;
    private boolean isPause = true;
    private boolean isStopListenReceiving;
    // 唤醒
    private WakeUp wakeUp;
    private List<ConversationBean> conversationList = new ArrayList<>();
    /**
     * 0 净水，1 冰箱，2 烟机，3 洗碗机，4 洗衣机，5 蒸烤箱
     */
    public AbstractDevice[] devicesArray = new AbstractDevice[6];
    private boolean isWeather;
    private ConversationUpdateCallback conversationUpdateCallback;
    private boolean isListenFail;
    private boolean interceptConversation;
    private BaiduTTSTool baiduTTSToolInstance;
    private String wakeupTxt = "";
    private boolean iswakeup;

    public VoiceService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        LogUtils.e(TAG, "onBind");
        return new MyBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        LogUtils.e(TAG, "onUnbind");
        return super.onUnbind(intent);
    }

    public class MyBinder extends Binder {
        public VoiceService getService() {
            return VoiceService.this;
        }
    }

    public DeviceModuleFactory getDeviceModuleFactory() {
        return deviceModuleFactory;
    }

    public DcsFramework getDcsFramework() {
        return dcsFramework;
    }

    public IPlatformFactory getIPlatformFactory() {
        return platformFactory;
    }

    public void setConversationUpdateCallback(ConversationUpdateCallback conversationUpdateCallback) {
        this.conversationUpdateCallback = conversationUpdateCallback;
    }

    public void removeConversationUpdateCallback() {
        this.conversationUpdateCallback = null;
    }

    public List<ConversationBean> getConversationList() {
        return conversationList;
    }

    public void clearConversationList() {
        if (conversationList != null) {
            conversationList.clear();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LogUtils.e(TAG, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtils.e(TAG, "onStartCommand");
        initOauth();
        initFramework();
        return START_STICKY;
    }

    private void initOauth() {
        IOauth baiduOauth = new OauthImpl();
        if (baiduOauth.isSessionValid()) {
            HttpConfig.setAccessToken(baiduOauth.getAccessToken());
        } else {
            baiduOauth.authorize();
        }
    }

    private void initFramework() {
        if (platformFactory == null) {
            platformFactory = new PlatformFactoryImpl(this);
        }

        if (dcsFramework == null) {
            dcsFramework = new DcsFramework(platformFactory);
        }


        dcsFramework.setEventCallback(new DcsEventCallback() {
            @Override
            public void onCallback(Directive directive) {
                handleDirective(directive);
            }
        });

        deviceModuleFactory = dcsFramework.getDeviceModuleFactory();

        deviceModuleFactory.createVoiceOutputDeviceModule();
        deviceModuleFactory.getVoiceOutputDeviceModule().addVoiceOutputListener(new VoiceOutputDeviceModule.IVoiceOutputListener() {
            @Override
            public void onVoiceOutputStarted() {
                LogUtil.d(TAG, "onVoiceOutputStarted");
                if (conversationUpdateCallback != null) {
                    conversationUpdateCallback.onSpeakingStart();
                }
            }

            @Override
            public void onVoiceOutputFinished() {
                LogUtil.d(TAG, "onVoiceOutputFinished");
                if (conversationUpdateCallback != null) {
                    conversationUpdateCallback.onSpeakingStop();
                }
            }
        });

        deviceModuleFactory.createVoiceInputDeviceModule();
        deviceModuleFactory.getVoiceInputDeviceModule().addVoiceInputListener(
                new VoiceInputDeviceModule.IVoiceInputListener() {
                    @Override
                    public void onStartRecord() {
                        LogUtil.d(TAG, "onStartRecord");
                        startRecording();
                        if (conversationUpdateCallback != null) {
                            conversationUpdateCallback.onListeningStart();
                        }
                    }

                    @Override
                    public void onFinishRecord() {
                        LogUtil.d(TAG, "onFinishRecord");
                        stopRecording();

                        if (!isListenFail) {
                            if (conversationUpdateCallback != null) {
                                conversationUpdateCallback.onListeningStop();
                            }
                        }
                        isListenFail = false;
                    }

                    public void onSucceed(int statusCode) {
                        LogUtil.d(TAG, "onSucceed-statusCode:" + statusCode);
                        if (statusCode != 200) {
                            stopRecording();
                            ToastUtil.show(getString(R.string.voice_err_msg));
                            if (conversationUpdateCallback != null) {
                                conversationUpdateCallback.onListenFail();
                                isListenFail = true;
                            }
                        }
                    }

                    @Override
                    public void onFailed(String errorMessage) {
                        LogUtil.d(TAG, "onFailed-errorMessage:" + errorMessage);
                        stopRecording();
                        ToastUtil.show(getString(R.string.voice_err_msg));

                        if (conversationUpdateCallback != null) {
                            conversationUpdateCallback.onListenFail();
                            isListenFail = true;
                        }
                    }
                });

        deviceModuleFactory.createAlertsDeviceModule();

        deviceModuleFactory.createAudioPlayerDeviceModule();
        deviceModuleFactory.getAudioPlayerDeviceModule().addAudioPlayListener(new IMediaPlayer.SimpleMediaPlayerListener() {
            @Override
            public void onPaused() {
                super.onPaused();
                LogUtils.e(TAG, "addAudioPlayListener-onPaused");
                isPause = true;
            }

            @Override
            public void onPlaying() {
                super.onPlaying();
                LogUtils.e(TAG, "addAudioPlayListener-onPlaying");
                isPause = false;
                Intent intent = new Intent(VoiceService.this, MediaPlayActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

            @Override
            public void onCompletion() {
                super.onCompletion();
                LogUtils.e(TAG, "addAudioPlayListener-onCompletion");
                isPause = false;
            }

            @Override
            public void onStopped() {
                super.onStopped();
                LogUtils.e(TAG, "addAudioPlayListener-onStopped");
                isPause = true;
            }
        });

        deviceModuleFactory.createSystemDeviceModule();
        deviceModuleFactory.createSpeakControllerDeviceModule();
        deviceModuleFactory.createPlaybackControllerDeviceModule();

        // init唤醒
        wakeUp = new WakeUp(platformFactory.getWakeUp(),
                platformFactory.getAudioRecord());
        wakeUp.addWakeUpListener(wakeUpListener);
        // 开始录音，监听是否说了唤醒词
        wakeUp.startWakeUp();

        //对话列表
        conversationList = new ArrayList<>();

        //百度tts
        baiduTTSToolInstance = BaiduTTSTool.getInstance(ttsHandler);
    }

    private IWakeUp.IWakeUpListener wakeUpListener = new IWakeUp.IWakeUpListener() {
        @Override
        public void onWakeUpSucceed() {
            ttsStop();
//            ToastUtil.show(getString(R.string.wakeup_succeed));

            wakeupTxt = getWakeupWord();
            ttWakeupsSpeak(wakeupTxt);

            Intent intent = new Intent(VoiceService.this, ConversationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

            if (conversationUpdateCallback != null) {
                conversationUpdateCallback.onWakeup();
            }
        }
    };

    private void afterWakeup() {

        LogUtils.e(TAG, "afterwakeup");

        if (!NetWorkUtil.isNetworkConnected(this)) {
            ToastUtil.show(getString(R.string.err_net_msg));
            return;
        }
        if (CommonUtil.isFastDoubleClick()) {
            return;
        }
        if (isStopListenReceiving) {
            platformFactory.getVoiceInput().stopRecord();
            isStopListenReceiving = false;
            return;
        }
        isStopListenReceiving = true;
        platformFactory.getVoiceInput().startRecord();
        doUserActivity();
    }


    private void stopRecording() {
        wakeUp.startWakeUp();
        isStopListenReceiving = false;
    }

    private void startRecording() {
        wakeUp.stopWakeUp();
        isStopListenReceiving = true;
        deviceModuleFactory.getSystemProvider().userActivity();
    }

    private void doUserActivity() {
        deviceModuleFactory.getSystemProvider().userActivity();
    }


    //在这里处理分发指令
    private void handleDirective(Directive directive) {
        LogUtils.e(TAG, "handleDirective--" + directive.rawMessage);

        String namespace = directive.header.getNamespace();
        if ("ai.dueros.device_interface.screen".equals(namespace)) {
            String name = directive.header.getName();
            if ("RenderVoiceInputText".equals(name)) {
                final RenderVoiceInputTextPayload textPayload = (RenderVoiceInputTextPayload) directive.payload;
                if ("FINAL".equals(textPayload.type.name())) {

                    if (voiceFilter(textPayload.text)) {
                        interceptConversation = true;
                        conversationList.add(new ConversationBean(textPayload.text, ConversationType.user_txt, 0));
                        if (conversationUpdateCallback != null) {
                            conversationUpdateCallback.update(conversationList);
                        }
                    }


                } else {
                    interceptConversation = false;
                }
            }
        }

        if (interceptConversation) {
            if (!"ai.dueros.device_interface.voice_input".equals(namespace)) {
                if (conversationUpdateCallback != null) {
                    conversationUpdateCallback.onSpeakingStop();
                }
                stopRecording();
                return;
            }
        }

        switch (namespace) {
            //语音输入
            case "ai.dueros.device_interface.voice_input":
                break;

            //语音输出
            case "ai.dueros.device_interface.voice_output":
                break;

            //扬声器控制
            case "ai.dueros.device_interface.speaker_controller":
                if (conversationUpdateCallback != null) {
                    conversationUpdateCallback.onSpeakingStop();
                }
                break;

            //音频播放器
            case "ai.dueros.device_interface.audio_player":
                if (conversationUpdateCallback != null) {
                    conversationUpdateCallback.onSpeakingStop();
                }
                break;

            //播放控制
            case "ai.dueros.device_interface.playback_controller":
                break;

            //闹钟
            case "ai.dueros.device_interface.alerts":
                break;

            //系统
            case "ai.dueros.device_interface.system":
                break;

            //屏幕展示
            case "ai.dueros.device_interface.screen": {
                String name = directive.header.getName();
                switch (name) {
                    //语音输入对话文本
                    case "RenderVoiceInputText": {
                        RenderVoiceInputTextPayload textPayload = (RenderVoiceInputTextPayload) directive.payload;
                        if ("FINAL".equals(textPayload.type.name())) {
                            conversationList.add(new ConversationBean(textPayload.text, ConversationType.user_txt, 0));
                            if (textPayload.text.contains("天气")) {
                                isWeather = true;
                            }
                        }
                        break;
                    }

                    case "RenderCard": {

                        JSONObject json = JsonUitls.getJSONObject(directive.rawMessage);
                        JSONObject payloadJson = JsonUitls.getJSONObject(json, "payload");
                        String type = JsonUitls.getString(payloadJson, "type");

                        switch (type) {
                            case "TextCard": {
                                LogUtils.e(TAG, "TextCard");
                                String content = JsonUitls.getString(payloadJson, "content");
                                conversationList.add(new ConversationBean(content, ConversationType.robot_txt, null));

                                if (isWeather) {
                                    isWeather = false;
                                    Intent weatherIntent = new Intent(this, WeatherActivity.class);
                                    weatherIntent.putExtra("weather_directive", content);
                                    weatherIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(weatherIntent);
                                }
                                break;
                            }

                            case "StandardCard": {
                                LogUtils.e(TAG, "StandardCard");
                                String title = JsonUitls.getString(payloadJson, "title");
                                title = "".equals(title) ? title : (title + "\n");
                                String content = JsonUitls.getString(payloadJson, "content");
                                conversationList.add(new ConversationBean((title + content), ConversationType.robot_txt, null));
                                JSONObject image = JsonUitls.getJSONObject(payloadJson, "image");
                                String src = JsonUitls.getString(image, "src");
                                conversationList.add(new ConversationBean(null, ConversationType.robot_img, src));
                                break;
                            }
                            case "ListCard": {
                                JSONArray list = JsonUitls.getJSONArray(payloadJson, "list");
                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject list_item = JsonUitls.getJSONObject(list, i);
                                    String title = JsonUitls.getString(list_item, "title");
                                    conversationList.add(new ConversationBean(title, ConversationType.robot_txt, null));

                                    JSONObject image = JsonUitls.getJSONObject(list_item, "image");
                                    String src = JsonUitls.getString(image, "src");
                                    if (!"".equals(src)) {
                                        conversationList.add(new ConversationBean(null, ConversationType.robot_img, src));
                                    }
                                }

                                break;
                            }
                            case "ImageListCard": {

                                JSONArray imageList = JsonUitls.getJSONArray(payloadJson, "imageList");
                                for (int i = 0; i < imageList.length(); i++) {
                                    JSONObject imageList_item = JsonUitls.getJSONObject(imageList, i);
                                    String src = JsonUitls.getString(imageList_item, "src");
                                    if (!"".equals(src)) {
                                        conversationList.add(new ConversationBean(null, ConversationType.robot_img, src));
                                    }
                                }
                                break;
                            }
                            default:
                                break;
                        }
                        break;
                    }
                }
                if (conversationUpdateCallback != null) {
                    conversationUpdateCallback.update(conversationList);
                }
                break;
            }

            //扩展内容屏幕展示
            case "ai.dueros.device_interface.screen_extended_card":
                String name = directive.header.getName();

                switch (name) {
                    case "RenderPlayerInfo": {

                        break;
                    }

                    case "RenderStock": {
                        JSONObject json = JsonUitls.getJSONObject(directive.rawMessage);
                        JSONObject payload = JsonUitls.getJSONObject(json, "payload");
                        String stockName = JsonUitls.getString(payload, "name");
                        double marketPrice = JsonUitls.getDouble(payload, "marketPrice");
                        double changeInPercentage = JsonUitls.getDouble(payload, "changeInPercentage");
                        String marketStatus = JsonUitls.getString(payload, "marketStatus");

                        String showDialog = stockName + " 现价" + marketPrice + (changeInPercentage >= 0 ? " 涨幅" : " 跌幅") + (changeInPercentage * 100d) + "% 状态：" + marketStatus;
                        conversationList.add(new ConversationBean(showDialog, ConversationType.robot_txt, null));
                        break;
                    }

                    case "RenderDate": {
                        JSONObject json = JsonUitls.getJSONObject(directive.rawMessage);
                        JSONObject payload = JsonUitls.getJSONObject(json, "payload");
                        String datetime = JsonUitls.getString(payload, "datetime");
                        String day = JsonUitls.getString(payload, "day");
                        conversationList.add(new ConversationBean(datetime + " " + day, ConversationType.robot_txt, null));
                        break;
                    }

                    case "RenderAudioList": {
                        break;
                    }

                    default:
                        break;
                }

                if (conversationUpdateCallback != null) {
                    conversationUpdateCallback.update(conversationList);
                }
                break;

            default:
                break;
        }

        try {
            BaseDeviceModule deviceModule = dcsFramework.dispatchDeviceModules.get(namespace);
            if (deviceModule != null) {
                deviceModule.handleDirective(directive);
            } else {
                String message = "No device to handle the directive";
                throw new HandleDirectiveException(
                        HandleDirectiveException.ExceptionType.UNSUPPORTED_OPERATION, message);
            }
        } catch (HandleDirectiveException exception) {
            dcsFramework.getSystemDeviceModule().sendExceptionEncounteredEvent(directive.rawMessage,
                    exception.getExceptionType(), exception.getMessage());
        } catch (Exception exception) {
            dcsFramework.getSystemDeviceModule().sendExceptionEncounteredEvent(directive.rawMessage,
                    HandleDirectiveException.ExceptionType.INTERNAL_ERROR,
                    exception.getMessage()
            );
        }
    }

    //@hailang
    public boolean voiceFilter(String text) {
        LogUtils.d(TAG, "filter:" + text);
        if (text.contains("冰箱"))
            return frige(text);
        if (text.contains("烟机"))
            return yanji(text);
        if (text.contains("洗碗"))
            return dishwaher(text);
        if (text.contains("洗衣"))
            return washer(text);
        if (text.contains("蒸烤"))
            return zhengkao(text);

        if (text.contains("水温") || (text.contains("水") && text.contains("温度"))) {
            //在这里去发送命令
            if (devicesArray[0] == null) {
                //未连接
                return false;
            }
            ViomiDeviceCallback deviceCallback = getRequestCallbackInstance("当前" + devicesArray[0].getName() + "的水温为#度。");
            Operation.getInstance().get_prop(devicesArray[0].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, "setup_tempe");
            return true;
        }
        if ((text.contains("饮水") || text.contains("热水"))
                && text.contains("度")) {
            //在这里去发送命令
//            ToastUtil.show("发送净水器命令");
            if (devicesArray[0] == null) {
                //未连接
                return false;
            }
            //处理回调
            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(text);
            int du;
            if (matcher.find()) {
                du = Integer.parseInt(matcher.group(0));
            } else return false;
            if (du < 40 || du > 90) {
                return false;
            }
            ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，云米即热净水器，一秒即热，已将水温调至" + du + "度。", du);
            Operation.getInstance().sendDrinkBarCommand1(devicesArray[0].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, du);
            return true;
        }
        if (text.contains("热水")) {
            //在这里去发送命令
//            ToastUtil.show("发送净水器命令");
            if (devicesArray[0] == null) {
                //未连接
                return false;
            }
            int du = 50;
            //处理回调
            ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，云米即热净水器，一秒即热。" + devicesArray[0].getName() + "已调到热水模式，水温为" + du + "度。", du);
            Operation.getInstance().sendDrinkBarCommand1(devicesArray[0].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, du);
            return true;
        }

        if (text.contains("净水"))
            return puri(text);

        if ("返回桌面".equals(text)) {
            Intent intent = new Intent(this, ShowScreenActivity.class);
            startActivity(intent);
            ttsSpeak("已返回桌面");
            conversationList.add(new ConversationBean("已返回桌面", ConversationType.robot_txt, null));
            if (conversationUpdateCallback != null) {
                conversationUpdateCallback.update(conversationList);
            }
            return true;
        }

        if (text.contains("打开设置")) {
            Intent intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
            ttsSpeak("已为你打开设置界面");
            conversationList.add(new ConversationBean("已为你打开设置界面", ConversationType.robot_txt, null));
            if (conversationUpdateCallback != null) {
                conversationUpdateCallback.update(conversationList);
            }
            return true;
        }
        return false;
    }

    private boolean zhengkao(String text) {
        if (devicesArray[5] == null) {
            //未连接
            return false;
        }
        if (text.contains("开") || text.contains("暂停") || text.contains("取消")) {
            // 0:开始，1:暂停, 2:取消预约或结束
            String status = "0";
            String message = "0";
            if (text.contains("开")) {
                status = "0";
                message = "好的，" + devicesArray[5] + "开始烹饪。";
            } else if (text.contains("暂停")) {
                status = "1";
                message = "好的，" + devicesArray[5] + "暂停烹饪。";
            } else {
                status = "2";
                message = "好的，" + devicesArray[5] + "取消预约烹饪。";
            }
            ViomiDeviceCallback deviceCallback = getCallbackInstance(message);
            Operation.getInstance().setMethod(devicesArray[5].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, "setpauseStatus", status);
            return true;
        }
        if (text.contains("模式")) {
            Operation.getInstance().get_prop(devicesArray[5].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                        @Override
                        public void onSuccess(String result) {
                            LogUtils.d(TAG, "onresult:" + result);
                            Message msg = mHandler.obtainMessage();
                            msg.what = HandlerMsgWhat.MSG0;
                            Bundle bundle = new Bundle();
                            try {
                                JSONObject object = new JSONObject(result);
                                int number = object.getJSONArray("result").getInt(0);
                                for (OvenMode mode : OvenMode.values()) {
                                    if (mode.value == number) {
                                        String text = "当前" + devicesArray[5] + "正处于" + mode.name + "模式";
                                        bundle.putString(HandlerMsgWhat.FEEBACK, text);
                                        ttsSpeak(text);
                                        msg.setData(bundle);
                                        mHandler.sendMessage(msg);
                                        break;
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFail(String result) {
                            LogUtils.d(TAG, "onresult:" + result);
                            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG1);
                        }
                    }, "mode");
            return true;
        }
        if (text.contains("温度")) {
            ViomiDeviceCallback deviceCallback = getRequestCallbackInstance("当前" + devicesArray[5].getName() + "的烹饪温度为#度。");
            Operation.getInstance().get_prop(devicesArray[5].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, "c_temp");
            return true;
        }
        if (text.contains("久") || text.contains("剩")) {
            Operation.getInstance().get_prop(devicesArray[5].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                        @Override
                        public void onSuccess(String result) {
                            LogUtils.d(TAG, "onresult:" + result);
                            Message msg = mHandler.obtainMessage();
                            msg.what = HandlerMsgWhat.MSG0;
                            Bundle bundle = new Bundle();
                            try {
                                JSONObject object = new JSONObject(result);
                                int status = object.getJSONArray("result").getInt(0);
                                int c_totletime = object.getJSONArray("result").getInt(1);
                                int c_time = object.getJSONArray("result").getInt(2);
                                //0:空闲，1:工作中，2:已预约，3:已完成
                                String text = "";
                                int time = c_totletime - c_time;
                                if (time == 0) {
                                    if (status == 3) {
                                        text = devicesArray[5].getName() + "已烹饪完成，您可以享受美食了。";
                                    } else {
                                        text = devicesArray[5].getName() + "还没开始烹饪哦。";
                                    }
                                } else if (time > 60) {
                                    text = time / 60 + "分钟之后" + devicesArray[5].getName() + "将完成烹饪，您就可以享受美食了。";
                                } else {
                                    text = time + "秒之后" + devicesArray[5].getName() + "将完成烹饪，您就可以享受美食了。";
                                }
                                bundle.putString(HandlerMsgWhat.FEEBACK, text);
                                ttsSpeak(text);
                                msg.setData(bundle);
                                mHandler.sendMessage(msg);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFail(String result) {
                            LogUtils.d(TAG, "onresult:" + result);
                            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG1);
                        }
                    }, "status", "c_totletime", "c_time");
            return true;
        }
        return false;
    }

    boolean puri(String text) {
        if (text.toLowerCase().contains("tds")) {
            //在这里去发送命令
            if (devicesArray[0] == null) {
                //未连接
                return false;
            }
            ViomiDeviceCallback deviceCallback = getRequestCallbackInstance("当前" + devicesArray[0].getName() + "的TDS值为#。");
            Operation.getInstance().get_prop(devicesArray[0].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, "tds_out");
            return true;
        }
        if (text.contains("滤")) {
            //在这里去发送命令
            if (devicesArray[0] == null) {
                //未连接
                return false;
            }
            Operation.getInstance().get_prop(devicesArray[0].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                        @Override
                        public void onSuccess(String result) {
                            LogUtils.d(TAG, "onresult:" + result);
                            Message msg = mHandler.obtainMessage();
                            msg.what = HandlerMsgWhat.MSG0;
                            Bundle bundle = new Bundle();
                            try {
                                int[] mResults = new int[18];
                                JSONArray jsonArray = JsonUitls.getJSONArray(new JSONObject(result), "result");
                                //[123,12,  5770,2269,5770,2366,1400,1939,1400,1939,3600,4320,3600,8640,7200,17280,3600,8640,   0,1,0,1,0]
                                if (jsonArray != null && jsonArray.length() > 0) {
                                    for (int i = 2; i < mResults.length; i++) {
                                        mResults[i] = jsonArray.optInt(i);
                                    }
                                    double d1 = 0, d2 = 0, d3 = 0, d4 = 0;
                                    if (mResults[10] != 0) {
                                        double flow1 = Double.valueOf(mResults[10] - mResults[2]) / mResults[10];
                                        double time1 = Double.valueOf(mResults[11] - mResults[3]) / mResults[11];
                                        d1 = Math.min(flow1, time1) * 100;
                                        if (d1 < 0)
                                            d1 = 0d;

                                        double flow2 = Double.valueOf(mResults[12] - mResults[4]) / mResults[12];
                                        double time2 = Double.valueOf(mResults[13] - mResults[5]) / mResults[13];
                                        d2 = Math.min(flow2, time2) * 100;
                                        if (d2 < 0)
                                            d2 = 0d;


                                        double flow3 = Double.valueOf(mResults[14] - mResults[6]) / mResults[14];
                                        double time3 = Double.valueOf(mResults[15] - mResults[7]) / mResults[15];
                                        d3 = Math.min(flow3, time3) * 100;
                                        if (d3 < 0)
                                            d3 = 0d;

                                        double flow4 = Double.valueOf(mResults[16] - mResults[8]) / mResults[16];
                                        double time4 = Double.valueOf(mResults[17] - mResults[9]) / mResults[17];
                                        d4 = Math.min(flow4, time4) * 100;
                                        if (d4 < 0)
                                            d4 = 0d;
                                    }
                                    String text = devicesArray[0].getName() + "PP棉滤芯剩余" + (int) d1 + "%,"
                                            + "前置活性炭滤芯剩余" + (int) d2 + "%,"
                                            + "RO反渗透滤芯剩余" + (int) d3 + "%,"
                                            + "后置活性炭滤芯剩余" + (int) d4 + "%。";
                                    bundle.putString(HandlerMsgWhat.FEEBACK, text);
                                    msg.setData(bundle);
                                    mHandler.sendMessage(msg);
                                    ttsSpeak(text);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFail(String result) {
                            LogUtils.d(TAG, "onresult:" + result);
                            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG1);
                        }
                    });
            return true;
        }

        if (text.contains("度")) {            //在这里去发送命令
//            ToastUtil.show("发送净水器命令");
            if (devicesArray[0] == null) {
                //未连接
                return false;
            }
            //处理回调
            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(text);
            int du;
            if (matcher.find()) {
                du = Integer.parseInt(matcher.group(0));
            } else return false;
            //40-90
            if (du < 40 || du > 90) {
                return false;
            }
            ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，云米即热净水器，一秒即热，已将水温调至" + du + "度。", du);
            Operation.getInstance().sendPurifierCommand1(devicesArray[0].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, du);
            return true;
        }
        return false;
    }


    boolean frige(String text) {
        if (text.contains("冷藏") && text.contains("多少")) {
            //在这里去发送命令
            if (devicesArray[1] == null) {
                //未连接
                return false;
            }
            ViomiDeviceCallback deviceCallback = getRequestCallbackInstance("当前" + devicesArray[1].getName() + "冷藏室的温度是#度。");
            Operation.getInstance().get_prop(devicesArray[1].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, "RCSetTemp");
            return true;
        }
        if (text.contains("变温") && text.contains("多少")) {
            //在这里去发送命令
            if (devicesArray[1] == null) {
                //未连接
                return false;
            }
            if (FrigeUtil.containBianwen(devicesArray[1].getDeviceModel())) {
                ViomiDeviceCallback deviceCallback = getRequestCallbackInstance("当前" + devicesArray[1].getName() + "变温室的温度是#度。");
                Operation.getInstance().get_prop(devicesArray[1].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                        , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, "CCSetTemp");
            } else {
                ViomiDeviceCallback deviceCallback = getCallbackInstance(devicesArray[1].getName() + "没有变温室哦。");
                deviceCallback.onSuccess("");
                return true;
            }

            return true;
        }
        if (text.contains("冷冻") && text.contains("多少")) {
            //在这里去发送命令
            if (devicesArray[1] == null) {
                //未连接
                return false;
            }
            ViomiDeviceCallback deviceCallback = getRequestCallbackInstance("当前" + devicesArray[1].getName() + "冷冻室的温度是#度。");
            Operation.getInstance().get_prop(devicesArray[1].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, "FCSetTemp");
            return true;
        }
        if (text.contains("变温") && text.contains("什么") && (text.contains("模式") || text.contains("场景"))) {
            //在这里去发送命令
            if (devicesArray[1] == null) {
                //未连接
                return false;
            }
            if (!FrigeUtil.hasBianwenScene(devicesArray[1].getDeviceModel())) {
                ViomiDeviceCallback deviceCallback = getCallbackInstance(devicesArray[1].getName() + "不支持设置变温室场景哦。");
                deviceCallback.onSuccess("");
                return true;
            }
            Operation.getInstance().get_prop(devicesArray[1].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                        @Override
                        public void onSuccess(String result) {
                            LogUtils.d(TAG, "onresult:" + result);
                            Message msg = mHandler.obtainMessage();
                            msg.what = HandlerMsgWhat.MSG0;
                            Bundle bundle = new Bundle();
                            try {
                                JSONObject object = new JSONObject(result);
                                String mode = object.getJSONArray("result").getString(0);
                                String text = "当前" + devicesArray[1].getName() + "冰箱变温室处于" + mode + "模式";
                                bundle.putString(HandlerMsgWhat.FEEBACK, text);
                                msg.setData(bundle);
                                mHandler.sendMessage(msg);
                                ttsSpeak(text);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFail(String result) {
                            LogUtils.d(TAG, "onresult:" + result);
                            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG1);
                        }
                    }, "SceneName");
            return true;
        }
        if (text.contains("什么") && text.contains("模式")) {
            //在这里去发送命令
            if (devicesArray[1] == null) {
                //未连接
                return false;
            }
            Operation.getInstance().get_prop(devicesArray[1].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                        @Override
                        public void onSuccess(String result) {
                            LogUtils.d(TAG, "onresult:" + result);
                            Message msg = mHandler.obtainMessage();
                            msg.what = HandlerMsgWhat.MSG0;
                            Bundle bundle = new Bundle();
                            try {
                                String text = "";
                                JSONObject object = new JSONObject(result);
                                String mode = object.getJSONArray("result").getString(0);
                                for (FridgeMode modeT : FridgeMode.values()) {
                                    if (modeT.toString().equals(mode)) {
                                        text = "当前" + devicesArray[1].getName() + "冰箱处于" + modeT.getName() + "模式";
                                        bundle.putString(HandlerMsgWhat.FEEBACK, text);
                                        break;
                                    }
                                }
                                msg.setData(bundle);
                                mHandler.sendMessage(msg);
                                ttsSpeak(text);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFail(String result) {
                            LogUtils.d(TAG, "onresult:" + result);
                            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG1);
                        }
                    }, "Mode");
            return true;
        }
        if (text.contains("冷藏") && text.contains("度")) {
            //在这里去发送命令
//            ToastUtil.show("发送冰箱冷藏室命令");
            if (devicesArray[1] == null) {
                //未连接
                return false;
            }
            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(text);
            int du;
            if (matcher.find())
                du = Integer.parseInt(matcher.group(0));
            else return false;
            //2-8
            if (du < 2 || du > 8)
                return false;
            //处理回调
            ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，" + devicesArray[1].getName() + "冷藏室已设置到" + du + "度。");
            Operation.getInstance().sendFridgeCommand5(devicesArray[1].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, du);
            LogUtils.e(TAG, "send---" + Thread.currentThread().getName());
            return true;
        }

        if (text.contains("变温") && text.contains("度")) {
            //在这里去发送命令
//            ToastUtil.show("发送冰箱变温室命令");
            if (devicesArray[1] == null) {
                //未连接
                return false;
            }
            //处理回调
            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(text);
            int du;
            if (matcher.find())
                du = Integer.parseInt(matcher.group(0));
            else
                return false;
            if (du > 0 && (text.contains("负") || text.contains("零下")))
                du = -du;
            //-18 - -3
            if (du < -18 || du > 8)
                return false;

            boolean cang3 = true;
            cang3 = FrigeUtil.containBianwen(devicesArray[1].getDeviceModel());
            if (cang3) {
                ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，" + devicesArray[1].getName() + "变温室已设置到" + du + "度。");
                Operation.getInstance().sendFridgeCommand6(devicesArray[1].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                        , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, du);
                return true;
            } else {
                ViomiDeviceCallback deviceCallback = getCallbackInstance(devicesArray[1].getName() + "没有变温室哦。");
                deviceCallback.onSuccess("");
                return true;
            }
        }
        if (text.contains("冷冻") && text.contains("度")) {
            //在这里去发送命令
//            ToastUtil.show("发送冰箱冷冻室命令");
            if (devicesArray[1] == null) {
                //未连接
                return false;
            }
            //处理回调

            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(text);
            int du;
            if (matcher.find())
                du = Integer.parseInt(matcher.group(0));
            else
                return false;
            if (du > 0)
                du = -du;
            //-24 - -16
            if (du < -25 || du > -15)
                return false;
            ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，" + devicesArray[1].getName() + "冷冻室已设置到零下" + Math.abs(du) + "度。");
            Operation.getInstance().sendFridgeCommand7(devicesArray[1].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, du);
            return true;
        }
        if (text.contains("变温") && text.contains("模式")) {
            //在这里去发送命令
//            ToastUtil.show("发送冰箱变温室命令");
            if (devicesArray[1] == null) {
                //未连接
                return false;
            }
            //处理回调
            String mode = null;
            String regex = "模式";
            FridgeTempMode mode1 = null;
            for (FridgeTempMode modeT : FridgeTempMode.values()) {
                mode = text.substring(text.lastIndexOf(regex) - modeT.toString().length(), text.lastIndexOf(regex));
                if (modeT.toString().equals(mode)) {
                    mode1 = modeT;
                    break;
                }
            }
            boolean cang3 = true;
            cang3 = FrigeUtil.containBianwen(devicesArray[1].getDeviceModel());
            if (cang3) {
                if (mode1 != null) {
                    ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，" + devicesArray[1].getName() + "变温室已设置为" + mode1.toString() + "模式。");
                    Operation.getInstance().sendFridgeCommand8(devicesArray[1].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                            , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, mode1);
                    return true;
                }
            } else {
                ViomiDeviceCallback deviceCallback = getCallbackInstance(devicesArray[1].getName() + "没有变温室哦。");
                deviceCallback.onSuccess("");
                return true;
            }
        }
        if (text.contains("模式")) {
            //在这里去发送命令
//            ToastUtil.show("发送冰箱命令");
            if (devicesArray[1] == null) {
                //未连接
                return false;
            }
            //处理回调

            String regex = "模式";
            String mode = null;
            FridgeMode mode1 = null;
            for (FridgeMode modeT : FridgeMode.values()) {
                mode = text.substring(text.lastIndexOf(regex) - modeT.getName().length(), text.lastIndexOf(regex));
                if (modeT.getName().equals(mode)) {
                    mode1 = modeT;
                    break;
                }
            }
            ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，" + devicesArray[1].getName() + "已设置成" + mode1.getName() + "模式。");
            if (mode1 != null) {
                Operation.getInstance().sendFridgeCommand4(devicesArray[1].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                        , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, mode1);
                return true;
            }
        }
        return false;
    }

    boolean yanji(String text) {
        if (text.contains("模式")) {
            //在这里去发送命令
//            ToastUtil.show("发送烟机命令");
            if (devicesArray[2] == null) {
                //未连接
                return false;
            }
            //处理回调

            String regex = "模式";
            String mode = "";
            HoodWindMode mode1 = null;
            for (HoodWindMode modeT : HoodWindMode.values()) {
                mode = text.substring(text.lastIndexOf(regex) - modeT.getText().length(), text.lastIndexOf(regex));
                if (modeT.getText().equals(mode)) {
                    mode1 = modeT;
                    break;
                }
            }
            ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，" + devicesArray[2].getName() + "已调到" + mode1.getText().replace("档", "挡") + "模式。");
            if (mode1 != null) {
                Operation.getInstance().sendCookerHoodCommand2(devicesArray[2].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                        , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, mode1);
                return true;
            }
        }
        if ((text.contains("灯光") || text.contains("照明"))
                && (text.contains("开") || text.contains("关"))) {
            //在这里去发送命令
//            ToastUtil.show("发送烟机命令");
            if (devicesArray[2] == null) {
                //未连接
                return false;
            }
            //处理回调
            Switch_ON_OFF switchOnOff = Switch_ON_OFF.on;
            if (text.contains("关")) {
                switchOnOff = Switch_ON_OFF.off;
            } else {
                switchOnOff = Switch_ON_OFF.on;
            }
            ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，" + devicesArray[2].getName() + "灯光已" + (switchOnOff == Switch_ON_OFF.on ? "打开" : "关闭") + "。");
            Operation.getInstance().sendCookerHoodCommand3(devicesArray[2].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, switchOnOff);
            return true;
        }
        if (text.contains("增压")
                && (text.contains("开") || text.contains("关"))) {
            //在这里去发送命令
//            ToastUtil.show("发送烟机命令");
            if (devicesArray[2] == null) {
                //未连接
                return false;
            }
            //处理回调
            Switch_ON_OFF switchOnOff = Switch_ON_OFF.on;
            if (text.contains("关")) {
                switchOnOff = Switch_ON_OFF.off;
            } else {
                switchOnOff = Switch_ON_OFF.on;
            }
            ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，" + devicesArray[2].getName() + "增压巡航已" + (switchOnOff == Switch_ON_OFF.on ? "打开" : "关闭") + "。");
            Operation.getInstance().sendCookerHoodCommand6(devicesArray[2].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, switchOnOff);
            return true;
        }
        return false;
    }

    boolean dishwaher(String text) {
        if (text.contains("模式")) {
            if (devicesArray[3] == null) {
                //未连接
                return false;
            }
            String regex = "模式";
            String mode = "";
            WashProMode mode1 = null;
            for (WashProMode modeT : WashProMode.values()) {
                mode = text.substring(text.lastIndexOf(regex) - modeT.name.length(), text.lastIndexOf(regex));
                if (modeT.name.equals(mode)) {
                    mode1 = modeT;
                    break;
                }
            }
            WashCusMode mode2 = null;
            if (mode1 == null) {
                for (WashCusMode modeT : WashCusMode.values()) {
                    mode = text.substring(text.lastIndexOf(regex) - modeT.name.length(), text.lastIndexOf(regex));
                    if (modeT.name.equals(mode)) {
                        mode2 = modeT;
                        break;
                    }
                }
            }
            if (mode1 != null) {
                ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，" + "已将" + devicesArray[3].getName() + "设置为" + mode1.name + "模式。");
                Operation.getInstance().sendDishwasherCommand3(devicesArray[3].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                        , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, mode1);
                return true;
            } else if (mode2 != null) {
                Operation.getInstance().sendDishwasherCommand5(devicesArray[3].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                        , MiotManager.getPeopleManager().getPeople().getAccessToken(), null, mode2);

                ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，" + "已将" + devicesArray[3].getName() + "设置为" + mode2.name + "模式。");
                Operation.getInstance().sendDishwasherCommand3(devicesArray[3].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                        , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, WashProMode.mode3);
                return true;
            } else {
                ViomiDeviceCallback deviceCallback = getCallbackInstance(devicesArray[3].getName() + "没有该模式哦。");
                deviceCallback.onSuccess("");
                return true;
            }
        }
        if ((text.contains("开始") || text.contains("暂停"))) {
            if (devicesArray[3] == null) {
                //未连接
                return false;
            }
            Switch_ON_OFF switchOnOff = Switch_ON_OFF.on;
            if (text.contains("开始")) {
                switchOnOff = Switch_ON_OFF.on;
            } else {
                switchOnOff = Switch_ON_OFF.off;
            }
            ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，"
                    + devicesArray[3].getName()
                    + (switchOnOff == Switch_ON_OFF.on ? "已开始洗碗。" : "已暂停。"));
            Operation.getInstance().sendDishwasherCommand4(devicesArray[3].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, switchOnOff);
            return true;
        }
        if ((text.contains("分钟"))) {
            if (devicesArray[3] == null) {
                //未连接
                return false;
            }
            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(text);
            int fen;
            if (matcher.find()) {
                fen = Integer.parseInt(matcher.group(0));
            } else return false;

            Date date = new Date(System.currentTimeMillis() + fen * 60 * 1000);
            SimpleDateFormat formath = new SimpleDateFormat("HH");
            SimpleDateFormat formatm = new SimpleDateFormat("mm");
            String hh = formath.format(date);
            String mm = formatm.format(date);
            ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，"
                    + devicesArray[3].getName()
                    + fen + "分钟后开始洗碗。");
            Operation.getInstance().sendDishwasherCommand6(devicesArray[3].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, hh, mm);
            return true;
        }
        if ((text.contains("状态"))) {
            if (devicesArray[3] == null) {
                //未连接
                return false;
            }
            Operation.getInstance().get_prop(devicesArray[3].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                        @Override
                        public void onSuccess(String result) {
                            LogUtils.d(TAG, "onresult:" + result);
                            Message msg = mHandler.obtainMessage();
                            msg.what = HandlerMsgWhat.MSG0;
                            Bundle bundle = new Bundle();
                            try {
                                JSONObject object = new JSONObject(result);
                                int bespeak_status = object.getJSONArray("result").getInt(0);
                                int wash_process = object.getJSONArray("result").getInt(1);
                                String status = "";
                                if (wash_process == 0) {
                                    if (bespeak_status == 1) {
                                        status = "已预约";
                                    } else {
                                        status = "空闲中";
                                    }
                                } else if (wash_process == 1) {
                                    status = "预洗中";
                                } else if (wash_process == 2) {
                                    status = "主洗中";
                                } else if (wash_process == 3) {
                                    status = "冲洗中";
                                } else if (wash_process == 4) {
                                    status = "漂洗中";
                                } else if (wash_process == 5) {
                                    status = "烘干中";
                                } else if (wash_process == 6) {
                                    status = "洗涤结束";
                                }
                                String text = "当前" + devicesArray[3] + status;
                                bundle.putString(HandlerMsgWhat.FEEBACK, text);
                                ttsSpeak(text);
                                msg.setData(bundle);
                                mHandler.sendMessage(msg);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFail(String result) {
                            LogUtils.d(TAG, "onresult:" + result);
                            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG1);
                        }
                    }, "bespeak_status", "wash_process");
            return true;
        }
        if ((text.contains("剩"))) {
            if (devicesArray[3] == null) {
                //未连接
                return false;
            }
            Operation.getInstance().get_prop(devicesArray[3].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                        @Override
                        public void onSuccess(String result) {
                            LogUtils.d(TAG, "onresult:" + result);
                            Message msg = mHandler.obtainMessage();
                            msg.what = HandlerMsgWhat.MSG0;
                            Bundle bundle = new Bundle();
                            try {
                                JSONObject object = new JSONObject(result);
                                int left_time = object.getJSONArray("result").getInt(0);
                                int wash_process = object.getJSONArray("result").getInt(1);
                                String text = "当前" + devicesArray[3];
                                if (left_time == 0) {
                                    if (wash_process == 0) {
                                        text += "还没开始洗碗呢。";
                                    } else {
                                        text += "已经洗完了。";
                                    }
                                } else {
                                    if (left_time > 60)
                                        text += "还剩" + left_time / 60 + "分钟才能洗完。";
                                    else text += "还剩" + left_time + "秒就洗完了。";
                                }
                                bundle.putString(HandlerMsgWhat.FEEBACK, text);
                                ttsSpeak(text);
                                msg.setData(bundle);
                                mHandler.sendMessage(msg);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFail(String result) {
                            LogUtils.d(TAG, "onresult:" + result);
                            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG1);
                        }
                    }, "left_time", "wash_process");
            return true;
        }
        return false;
    }

    boolean washer(String text) {
        if ((text.contains("开") || text.contains("暂停"))) {
            if (devicesArray[4] == null) {
                //未连接
                return false;
            }
            Switch_ON_OFF switchOnOff = Switch_ON_OFF.on;
            if (text.contains("开")) {
                switchOnOff = Switch_ON_OFF.on;
            } else {
                switchOnOff = Switch_ON_OFF.off;
            }
            ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，"
                    + devicesArray[4].getName()
                    + (switchOnOff == Switch_ON_OFF.on ? "已开始洗衣服。" : "已暂停。"));
            Operation.getInstance().sendWashingMachineCommand2(devicesArray[4].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, switchOnOff);
            return true;
        }
        if (text.contains("模式")) {
            if (devicesArray[4] == null) {
                //未连接
                return false;
            }
            String regex = "模式";
            String mode = "";
            WashMacProMode mode1 = null;
            for (WashMacProMode modeT : WashMacProMode.values()) {
                mode = text.substring(text.lastIndexOf(regex) - modeT.toString().length(), text.lastIndexOf(regex));
                if (modeT.toString().toLowerCase().equals(mode.toLowerCase())) {
                    mode1 = modeT;
                    break;
                }
            }
            if (mode1 == null && text.contains("漂") && text.contains("脱"))
                mode1 = WashMacProMode.漂脱;
            if (mode1 == null)
                return false;
            ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，已将"
                    + devicesArray[4].getName()
                    + "调至" + mode1.toString() + "模式。");
            Operation.getInstance().sendWashingMachineCommand3(devicesArray[4].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, mode1);
            return true;
        }
        if (text.contains("度")) {
            if (devicesArray[4] == null) {
                //未连接
                return false;
            }
            WashingTemp temp = null;
            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(text);
            int du;
            if (matcher.find())
                du = Integer.parseInt(matcher.group(0));
            else
                return false;
            if (du >= 90) {
                temp = WashingTemp.t90;
            } else if (du >= 60) {
                temp = WashingTemp.t60;
            } else if (du >= 40) {
                temp = WashingTemp.t40;
            } else if (du >= 30) {
                temp = WashingTemp.t30;
            } else {
                temp = WashingTemp.normal;
            }
            ViomiDeviceCallback deviceCallback;
            if (temp.vanule == 0) {
                deviceCallback = getCallbackInstance("好的，已将"
                        + devicesArray[4].getName()
                        + "的水温调至常温。");
            } else {
                deviceCallback = getCallbackInstance("好的，已将"
                        + devicesArray[4].getName()
                        + "的水温调至" + temp.vanule + "度。");
            }
            Operation.getInstance().sendWashingMachineCommand4(devicesArray[4].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, temp);
            return true;
        }
        if (text.contains("次数")) {
            if (devicesArray[4] == null) {
                //未连接
                return false;
            }
            WashingTimes time = null;
            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(text);
            int du;
            if (matcher.find())
                du = Integer.parseInt(matcher.group(0));
            else
                return false;
            if (du >= 5) {
                time = WashingTimes.five;
            } else if (du == 4) {
                time = WashingTimes.four;
            } else if (du == 3) {
                time = WashingTimes.three;
            } else if (du == 2) {
                time = WashingTimes.two;
            } else {
                time = WashingTimes.one;
            }
            ViomiDeviceCallback deviceCallback;
            deviceCallback = getCallbackInstance("好的，已将"
                    + devicesArray[4].getName()
                    + "漂洗次数设置为" + time.value + "次。");
            Operation.getInstance().sendWashingMachineCommand5(devicesArray[4].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, time);
            return true;
        }
        if (text.contains("脱水")) {
            if (devicesArray[4] == null) {
                //未连接
                return false;
            }
            String mode = "";
            WashingDryRmpMode mode1 = null;
            for (WashingDryRmpMode modeT : WashingDryRmpMode.values()) {
                mode = text.substring(text.length() - modeT.name.length(), text.length() - 1);
                if (modeT.name.toLowerCase().equals(mode.toLowerCase())) {
                    mode1 = modeT;
                    break;
                }
            }
            if (mode1 == null)
                return false;
            ViomiDeviceCallback deviceCallback = getCallbackInstance("好的，已将"
                    + devicesArray[4].getName()
                    + "脱水转数设为" + mode1.name);
            Operation.getInstance().sendWashingMachineCommand6(devicesArray[4].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), deviceCallback, mode1);
            return true;
        }
        if (text.contains("久") || text.contains("剩")) {
            if (devicesArray[4] == null) {
                //未连接
                return false;
            }
            String mode = "";
            WashingDryRmpMode mode1 = null;
            for (WashingDryRmpMode modeT : WashingDryRmpMode.values()) {
                mode = text.substring(text.length() - modeT.name.length(), text.length() - 1);
                if (modeT.name.toLowerCase().equals(mode.toLowerCase())) {
                    mode1 = modeT;
                    break;
                }
            }
            if (mode1 == null)
                return false;
            Operation.getInstance().get_prop(devicesArray[4].getDeviceId(), Miconfig.MI_OAUTH_APP_ID + ""
                    , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                        @Override
                        public void onSuccess(String result) {
                            LogUtils.d(TAG, "onresult:" + result);
                            Message msg = mHandler.obtainMessage();
                            msg.what = HandlerMsgWhat.MSG0;
                            Bundle bundle = new Bundle();
                            try {
                                JSONObject object = new JSONObject(result);
                                int wash_process = object.getJSONArray("result").getInt(0);
                                int remain_time = object.getJSONArray("result").getInt(0);
                                String text;
                                if (remain_time == 0) {
                                    if (wash_process == 0) {
                                        text = devicesArray[4].getName()
                                                + "还没开始洗衣服呢。";
                                    } else {
                                        text = devicesArray[4].getName()
                                                + "已经洗完衣服了。";
                                    }
                                } else {
                                    text = devicesArray[4].getName()
                                            + "还有" + remain_time + "分钟洗完衣服。";
                                }
                                bundle.putString(HandlerMsgWhat.FEEBACK, text);
                                ttsSpeak(text);
                                msg.setData(bundle);
                                mHandler.sendMessage(msg);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFail(String result) {
                            LogUtils.d(TAG, "onresult:" + result);
                            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG1);
                        }
                    }, "wash_process", "remain_time");
        }
        if (text.contains("打开设置")) {
            Intent intent = new Intent(this, SettingActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            ttsSpeak("已为你打开设置界面");
            conversationList.add(new ConversationBean("已为你打开设置界面", ConversationType.robot_txt, null));
            if (conversationUpdateCallback != null) {
                conversationUpdateCallback.update(conversationList);
            }
            return true;
        }

        if (text.contains("打开隐藏表情")) {
            Intent intent = new Intent(this, EmjActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return true;
        }

        return false;
    }

    private ViomiDeviceCallback getCallbackInstance(final String feeback, final int... temp) {
        return new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                Message msg = mHandler.obtainMessage();
                msg.what = HandlerMsgWhat.MSG0;
                Bundle bundle = new Bundle();
                bundle.putString(HandlerMsgWhat.FEEBACK, feeback);
                if (temp != null && temp.length > 0)
                    bundle.putInt(HandlerMsgWhat.TEMP, temp[0]);
                msg.setData(bundle);
                mHandler.sendMessage(msg);

            }

            @Override
            public void onFail(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                mHandler.sendEmptyMessage(HandlerMsgWhat.MSG1);
            }
        };
    }

    private ViomiDeviceCallback getRequestCallbackInstance(final String feeback) {
        return new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                Message msg = mHandler.obtainMessage();
                msg.what = HandlerMsgWhat.MSG0;
                Bundle bundle = new Bundle();
                try {
                    JSONObject object = new JSONObject(result);
                    int number = object.getJSONArray("result").getInt(0);
                    String text = feeback.replace("#", number + "");
                    bundle.putString(HandlerMsgWhat.FEEBACK, text);
                    ttsSpeak(text);
                    msg.setData(bundle);
                    mHandler.sendMessage(msg);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail(String result) {
                LogUtils.d(TAG, "onresult:" + result);
                mHandler.sendEmptyMessage(HandlerMsgWhat.MSG1);
            }
        };
    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case HandlerMsgWhat.MSG0:
                String text = "处理成功！";
                ConversationBean bean = new ConversationBean(text, ConversationType.robot_txt, 0);
                if (msg.getData() != null) {
                    if (!TextUtils.isEmpty(msg.getData().getString(HandlerMsgWhat.FEEBACK))) {
                        text = msg.getData().getString(HandlerMsgWhat.FEEBACK);
                        bean.setText(text);
                    }
                    if (msg.getData().getInt(HandlerMsgWhat.TEMP, -1) > 0) {
                        bean.setPuri_temp(msg.getData().getInt(HandlerMsgWhat.TEMP, -1));
                        bean.setType(ConversationType.puri_temp);
                    }
                }
                conversationList.add(bean);
                if (conversationUpdateCallback != null) {
                    conversationUpdateCallback.update(conversationList);
                }
                break;
            case HandlerMsgWhat.MSG1:
                conversationList.add(new ConversationBean("处理失败！", ConversationType.robot_txt, 0));
                if (conversationUpdateCallback != null) {
                    conversationUpdateCallback.update(conversationList);
                }
                break;
            default:
                break;
        }
    }

    //将就用着先
    private Handler ttsHandler = new Handler() {
        @Override
        public void handleMessage(final Message msg) {
            switch (msg.what) {
                case UI_CHANGE_INPUT_TEXT_SELECTION:
                    LogUtils.e(TAG, "" + msg.arg1);
                    ttsHandler.removeMessages(100);
                    ttsHandler.sendEmptyMessageDelayed(100, 1000);

                    if (msg.arg1 == wakeupTxt.length() - 2) {
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (iswakeup) {
                                    afterWakeup();
                                    LogUtils.e(TAG, "" + msg.arg1);
                                }
                            }
                        }, 700);
                    }
                    break;

                case 100:
                    LogUtils.e(TAG, "speakover");
                    if (!iswakeup) {
                        if (conversationUpdateCallback != null) {
                            conversationUpdateCallback.onSpeakingStop();
                        }
                    }

                    break;
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        // 先remove listener  停止唤醒,释放资源
        wakeUp.removeWakeUpListener(wakeUpListener);
        wakeUp.stopWakeUp();
        wakeUp.releaseWakeUp();
        baiduTTSToolInstance.destroy();

        if (dcsFramework != null) {
            dcsFramework.release();
        }
    }

    private String getWakeupWord() {
        List<String> wordList = new ArrayList<>();
        wordList.add("我在，能帮您做些什么");
        wordList.add("我来啦");
        wordList.add("又想我了啊，我在");
        wordList.add("有什么能帮到您的");
        wordList.add("哈喽，想要干嘛");
        wordList.add("你好，我听着呢");

        String s = wordList.get(new Random().nextInt(wordList.size()));
        if (conversationUpdateCallback != null) {
            conversationList.clear();
//            conversationList.add(new ConversationBean(s, ConversationType.robot_txt, null));
            conversationUpdateCallback.update(conversationList);
        }
        return s;
    }

    public void ttsSpeak(String text) {
        iswakeup = false;
        baiduTTSToolInstance.speak(text);
        if (conversationUpdateCallback != null) {
            conversationUpdateCallback.onSpeakingStart();
        }
    }

    public void ttWakeupsSpeak(String text) {
        iswakeup = true;
        baiduTTSToolInstance.speak(text);
        if (conversationUpdateCallback != null) {
            conversationUpdateCallback.onSpeakingStart();
        }
    }

    public void ttsPause() {
        baiduTTSToolInstance.pause();
    }

    public void ttsResume() {
        baiduTTSToolInstance.resume();
    }

    public void ttsStop() {
        baiduTTSToolInstance.stop();
    }

}
