package viomi.com.viomiaiface.service;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.activity.VideoRingActivity;
import viomi.com.viomiaiface.base.BaseService;
import viomi.com.viomiaiface.config.BroadcastAction;
import viomi.com.viomiaiface.utils.LogUtils;

public class VideoChatService extends BaseService {

    private RtcEngine mRtcEngine;
    private IRtcEngineEventHandler mRtcEventHandler;

    public VideoChatService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        LogUtils.e(TAG, "onBind");
        return new MyBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        LogUtils.e(TAG, "onUnbind");
        return super.onUnbind(intent);
    }

    public class MyBinder extends Binder {
        public VideoChatService getService() {
            return VideoChatService.this;
        }
    }

    public RtcEngine getRtcEngine() {
        return mRtcEngine;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LogUtils.e(TAG, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtils.e(TAG, "onStartCommand");
        initRtcEventHandler();
        initializeAgoraEngine();
        setupVideoProfile();
        joinChannel();
        return START_STICKY;
    }

    private void initRtcEventHandler() {
        mRtcEventHandler = new IRtcEngineEventHandler() {
            @Override
            public void onFirstRemoteVideoDecoded(final int uid, int width, int height, int elapsed) {

            }

            @Override
            public void onUserOffline(final int uid, int reason) {
                LogUtils.e(TAG, "onUserOffline:uid------" + uid);
                sendBroadcast(new Intent(BroadcastAction.VIDEOCHATUSERLEFT));
            }

            @Override
            public void onUserJoined(final int uid, int elapsed) {
                LogUtils.e(TAG, "onUserJoined:uid------" + uid);

                Intent intent = new Intent(VideoChatService.this, VideoRingActivity.class);
                intent.putExtra("uid", uid);
                startActivity(intent);
            }

            @Override
            public void onUserMuteVideo(final int uid, final boolean muted) {

            }
        };
    }

    private void initializeAgoraEngine() {
        try {
            mRtcEngine = RtcEngine.create(getBaseContext(), getString(R.string.agora_app_id), mRtcEventHandler);
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }
    }

    private void setupVideoProfile() {
        mRtcEngine.setVideoProfile(Constants.VIDEO_PROFILE_360P, false);
    }

    private void joinChannel() {
//        mRtcEngine.leaveChannel();
//        mRtcEngine.joinChannel(null, "ViomiChannel2", "Extra Optional Data", 0); // if you do not specify the uid, we will generate the uid for you
//
//        Intent intent = new Intent(this, VideoChat2Activity.class);
//        startActivity(intent);
    }
}
