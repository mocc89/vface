package viomi.com.viomiconnectsdk.viomiconnect.util;

/**
 * Created by Mocc on 2017/9/18
 */

public interface ViomiDeviceCallback {
    void onSuccess(String result);
    void onFail(String result);
}
