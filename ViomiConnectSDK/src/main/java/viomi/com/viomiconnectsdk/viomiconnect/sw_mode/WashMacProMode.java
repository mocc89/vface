package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2017/12/30
 */

public enum WashMacProMode {

    //设定洗涤程序，
    // 包括："goldenwash","drumclean","spin","antibacterial","super_quick","cottons","shirts","child_cloth",
    // "jeans", "wool", "down", "chiffon", "outdoor", "delicates", "underwears"，"rinse_spin","My Time","cotton Eco"。
    // 分别代表  “黄金洗”程序，“洁筒洗”程序，“单脱水”程序，“除菌螨”程序，“超快洗”程序，“棉织物”程序，
    // “衬衣”程序，“童衣洗”程序, “牛仔”程序，“羊毛”程序，“羽绒服”程序，“雪纺”程序，“运动服”程序，
    // “精细织物”程序，“内衣”程序，“漂+脱”程序，“自定义”程序，“棉Eco"程序

    黄金洗("goldenwash"), 洁筒洗("drumclean"), 单脱水("spin"), 除菌螨("antibacterial"), 超快洗("super_quick"), 棉织物("cottons"),
    衬衣("shirts"), 童衣洗("child_cloth"), 牛仔("jeans"), 羊毛("wool"), 羽绒服("down"), 雪纺("chiffon"), 运动服("outdoor"),
    精细织物("delicates"), 内衣("underwears"), 漂脱("rinse_spin"), 自定义("My Time"), 棉Eco("cotton Eco");


    public String value;

    WashMacProMode(String value) {
        this.value = value;
    }
}
