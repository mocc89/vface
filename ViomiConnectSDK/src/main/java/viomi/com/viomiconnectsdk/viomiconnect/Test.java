package viomi.com.viomiconnectsdk.viomiconnect;


import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_OFF;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

public class Test {

    private static String did = "58067022";
    private static String clientId = "2882303761517454408";
    private static String token = "V2_ElB7at1Ly7xEL5tQbiiM_W2rS-gs0IjxKndrB13dKlbuKxaOISyYrdBIyHkapIMOibLuJlvwP3FF0o7tP-KvJ1SgcHS0AldXM9O0IS_hbIdeeCXdGq7EOhbRYtQQrIoAPpoCNIQ1xtzKePy9HLgblQ";

    private static ViomiDeviceCallback callback = new ViomiDeviceCallback() {
        @Override
        public void onSuccess(String result) {
            System.out.println(result);
        }

        @Override
        public void onFail(String result) {
            System.out.println(result);
        }
    };

    public static void main(String[] args) {
        Operation.getInstance().sendFridgeCommand1(did, clientId, token, callback, Switch_ON_OFF.on);
        Operation.getInstance().sendFridgeCommand5(did, clientId, token, callback, -10 );
    }

}
