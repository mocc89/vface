package viomi.com.viomiconnectsdk.viomiconnect.config;

/**
 * Created by Mocc on 2017/9/18
 */

public class Config {
    public static String MIOTHOST = "https://openapp.io.mi.com/openapp/device/rpc/";
    public static String MIOTHOST_GETUSERDATA = "https://openapp.io.mi.com/openapp/user/get_user_device_data";
}
