package viomi.com.viomiconnectsdk.viomiconnect.protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_OFF;

/**
 * Created by Mocc on 2017/12/29
 */


//管线机
public class PipelineMachine {

    private volatile static PipelineMachine instance;
    private volatile static String did;
    private int id = 1;

    public static PipelineMachine getInstance(String mDid) {
        did = mDid;
        if (instance == null) {
            synchronized (PipelineMachine.class) {
                if (instance == null) {
                    instance = new PipelineMachine();
                }
            }
        }
        return instance;
    }

    private PipelineMachine() {

    }

    // 获取状态get_prop
    public String getCommand0(String[] args) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            if (args != null) {
                for (int i = 0; i < args.length; i++) {
                    jsonArray.put(i, args[i]);
                }
            }
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    //第一个参数，序号，第几个按键；第二个参数，自定义温度
    public String getCommand1(int order,int temp) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_tempe_setup");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(order);
            jsonArray.put(temp);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //ms，设置系统时间，时间戳
    public String getCommand2(long time) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method","set_time" );
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(time);

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //开启/关闭喝水提醒
    public String getCommand3(Switch_ON_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_drink_remind_enable");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设定喝水提醒时间，单位小时
    public String getCommand4(int hour) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method","set_drink_remind_time" );
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(hour);

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

}
