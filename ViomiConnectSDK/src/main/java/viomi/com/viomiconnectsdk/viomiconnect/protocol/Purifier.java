package viomi.com.viomiconnectsdk.viomiconnect.protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_OFF;

/**
 * Created by Mocc on 2017/9/18
 */

//净水器
public class Purifier {

    private volatile static Purifier instance;
    private volatile static String did;
    private int id = 1;

    public static Purifier getInstance(String mDid) {
        did = mDid;
        if (instance == null) {
            synchronized (Purifier.class) {
                if (instance == null) {
                    instance = new Purifier();
                }
            }
        }
        return instance;
    }

    private Purifier() {

    }

    // 获取状态get_prop
    public String getCommand0(String[] args) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            if (args != null) {
                for (int i = 0; i < args.length; i++) {
                    jsonArray.put(i, args[i]);
                }
            }
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    // 净水器设置温度 40-90摄氏度 X5专用
    public String getCommand1(int temperature) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_tempe_setup");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(1);
            jsonArray.put(temperature);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    // 设置净水器出水/停止出水： 0是停水 1是出水 ；仅展会样机X5
    public String getCommand2(Switch_ON_OFF switchOnOff) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_power");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(switchOnOff.ordinal());
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    public String getProp(String... params) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            if (params != null)
                for (String s : params) {
                    jsonArray.put(s);
                }
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设置滤芯1 已经使用流量和已经使用时间
    public String getCommand3(int usedFlow, int usedTime) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_f1_used");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(usedFlow);
            jsonArray.put(usedTime);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设置滤芯2 已经使用流量和已经使用时间
    public String getCommand4(int usedFlow, int usedTime) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_f2_used");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(usedFlow);
            jsonArray.put(usedTime);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    //设置滤芯3 已经使用流量和已经使用时间
    public String getCommand5(int usedFlow, int usedTime) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_f3_used");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(usedFlow);
            jsonArray.put(usedTime);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    //设置滤芯4 已经使用流量和已经使用时间
    public String getCommand6(int usedFlow, int usedTime) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_f4_used");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(usedFlow);
            jsonArray.put(usedTime);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //0 - 禁止TDS监测， 1 - 使能TDS监测
    public String getCommand7(Switch_ON_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_tds_mon");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设置系统当前时间 单位：ms
    public String getCommand8(long time) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_time");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(time);

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    //设置水质指示状态阈值 小米净水器2
    public String getCommand9(int value) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_tds_warn_thd");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(value);

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设置出水流量 参数1表示面板第几个按钮，参数2表示出水流量 最大2000ml
    public String getCommand10(int order, int flow) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_flow_setup");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(order);
            jsonArray.put(flow);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

//    //
//    public String getCommand() {
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("method", );
//            jsonObject.put("did", did);
//            jsonObject.put("id", id);
//            JSONArray jsonArray = new JSONArray();
//            //todo
//            jsonArray.put();
//
//            jsonObject.put("params", jsonArray);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return jsonObject.toString();
//    }

}
