package viomi.com.viomiconnectsdk.viomiconnect.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Mocc on 2017/9/18
 */

public class HTTPUtil {

    public static HTTPUtil getInstance() {
        return new HTTPUtil();
    }

    private static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();//CPU数
    private static final int CORE_POOL_SIZE = CPU_COUNT + 1;
    private static final int MAXIMUM_POOL_SIZE = CPU_COUNT * 2 + 1;
    private static final int KEEP_ALIVE = 1;

    private static final ThreadFactory sThreadFactory = new ThreadFactory() {
        private final AtomicInteger mCount = new AtomicInteger(1);

        public Thread newThread(Runnable r) {
            return new Thread(r, "AsyncTask #" + mCount.getAndIncrement());
        }
    };

    private static final BlockingQueue<Runnable> sPoolWorkQueue =
            new LinkedBlockingQueue<Runnable>(128);

    public static final Executor THREAD_POOL_EXECUTOR
            = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE,
            TimeUnit.SECONDS, sPoolWorkQueue, sThreadFactory);


    public void getRequst(final String url_, final ViomiDeviceCallback callback) {

        if (callback == null) {
            return;
        }
        THREAD_POOL_EXECUTOR.execute(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection con = null;
                InputStream is = null;
                StringBuffer result = new StringBuffer();
                try {
                    URL url = new URL(url_);
                    con = (HttpURLConnection) url.openConnection();
                    con.setConnectTimeout(15 * 1000);
                    con.connect();
                    if (con.getResponseCode() == 200) {
                        is = con.getInputStream();
                        int len;
                        byte[] buf = new byte[1024];
                        while ((len = is.read(buf)) != -1) {
                            result.append(new String(buf, 0, len));
                        }

                        callback.onSuccess(result.toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.toString());
                } finally {
                    if (con != null) {
                        con.disconnect();
                    }
                    if (is != null) {
                        try {
                            is.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }
}
