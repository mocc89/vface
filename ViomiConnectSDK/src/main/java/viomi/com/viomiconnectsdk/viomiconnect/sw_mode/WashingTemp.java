package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2017/12/30
 */

public enum WashingTemp {

    normal(0), t30(30), t40(40), t60(60), t90(90);

    public int vanule;

    WashingTemp(int vanule) {
        this.vanule = vanule;
    }
}
