package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2017/12/30
 */

public enum WashCusMode {

    //设置洗涤自定义模式，3 - 强力洗   4 - 预洗   5 - 玻璃洗    6 - 除菌

    mode3(3, "强力洗"), mode4(4, "预洗"), mode5(5, "玻璃洗"), mode6(6, "除菌");

    public int value;
    public String name;

    WashCusMode(int value, String name) {
        this.value = value;
        this.name = name;
    }
}
