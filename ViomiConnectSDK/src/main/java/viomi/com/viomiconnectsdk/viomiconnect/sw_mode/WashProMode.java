package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2017/12/30
 */


public enum WashProMode {
    mode0(0, "标准洗"), mode1(1, "经济洗"), mode2(2, "快速洗"), mode3(3, "自定义");
    public int value;
    public String name;

    WashProMode(int value, String name) {
        this.value = value;
        this.name = name;
    }
}
