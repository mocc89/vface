package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2017/12/29
 */


// 0:正常工作模式
//1 ：绑定模式
//2 ：主机升级模式
//3:从机升级模式
public enum HoodBleMode {
    mode0, mode1, mode2, mode3;
}
