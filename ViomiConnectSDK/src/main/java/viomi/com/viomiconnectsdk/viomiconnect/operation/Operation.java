package viomi.com.viomiconnectsdk.viomiconnect.operation;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import viomi.com.viomiconnectsdk.viomiconnect.config.Config;
import viomi.com.viomiconnectsdk.viomiconnect.protocol.CookerHood;
import viomi.com.viomiconnectsdk.viomiconnect.protocol.Dishwasher;
import viomi.com.viomiconnectsdk.viomiconnect.protocol.DrinkBar;
import viomi.com.viomiconnectsdk.viomiconnect.protocol.Fridge;
import viomi.com.viomiconnectsdk.viomiconnect.protocol.Kettle;
import viomi.com.viomiconnectsdk.viomiconnect.protocol.PipelineMachine;
import viomi.com.viomiconnectsdk.viomiconnect.protocol.Purifier;
import viomi.com.viomiconnectsdk.viomiconnect.protocol.WashingMachine;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.FridgeMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.FridgeTempMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.HoodBleMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.HoodPowerMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.HoodWindMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.KettleLuanchMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.KettleWorkMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_OFF;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashCusMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashMacProMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashProMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashingDetergentMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashingDryRmpMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashingSoftenerMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashingTemp;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashingTimes;
import viomi.com.viomiconnectsdk.viomiconnect.util.HTTPUtil;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

/**
 * Created by Mocc on 2017/9/18
 */

public class Operation {

    private volatile static Operation instance;

    public static Operation getInstance() {
        if (instance == null) {
            synchronized (Operation.class) {
                if (instance == null) {
                    instance = new Operation();
                }
            }
        }
        return instance;
    }

    /*
    *
    * 冰箱控制
    *
    * */

    // 获取状态get_prop
    public void sendFridgeCommand0(String did, String clientId, String token, ViomiDeviceCallback callback, String[] args) {
        setCommand(did, Fridge.getInstance(did).getCommand0(args), clientId, token, callback);
    }

    // 打开/关闭 一键净化
    public void sendFridgeCommand1(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF switchOnOff) {
        setCommand(did, Fridge.getInstance(did).getCommand1(switchOnOff), clientId, token, callback);
    }

    // 打开/关闭 冷藏室
    public void sendFridgeCommand2(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF switchOnOff) {
        setCommand(did, Fridge.getInstance(did).getCommand2(switchOnOff), clientId, token, callback);
    }

    // 打开/关闭 变温室
    public void sendFridgeCommand3(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF switchOnOff) {
        setCommand(did, Fridge.getInstance(did).getCommand3(switchOnOff), clientId, token, callback);
    }

    // 设置冰箱模式
    public void sendFridgeCommand4(String did, String clientId, String token, ViomiDeviceCallback callback, FridgeMode mode) {
        setCommand(did, Fridge.getInstance(did).getCommand4(mode), clientId, token, callback);
    }

    // 设定冷藏室温度，temperature 温度：[2-8]，默认6
    public void sendFridgeCommand5(String did, String clientId, String token, ViomiDeviceCallback callback, int temperature) {
        setCommand(did, Fridge.getInstance(did).getCommand5(temperature), clientId, token, callback);
    }

    // 设定变温室温度，（-18）- -3，默认4
    public void sendFridgeCommand6(String did, String clientId, String token, ViomiDeviceCallback callback, int temperature) {
        setCommand(did, Fridge.getInstance(did).getCommand6(temperature), clientId, token, callback);
    }

    // 设定冷冻室温度，（-25）-（-15），默认-18
    public void sendFridgeCommand7(String did, String clientId, String token, ViomiDeviceCallback callback, int temperature) {
        setCommand(did, Fridge.getInstance(did).getCommand7(temperature), clientId, token, callback);
    }

    // 设置变温室场景模式 只适用于大屏
    public void sendFridgeCommand8(String did, String clientId, String token, ViomiDeviceCallback callback, FridgeTempMode mode) {
        setCommand(did, Fridge.getInstance(did).getCommand8(mode), clientId, token, callback);
    }

    //开启/关闭速冷  只适用：冰箱iLive四门语音版（viomi.fridge.v2）；  462大屏金属门冰箱（viomi.fridge.v3）
    public void sendFridgeCommand9(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF switchOnOff) {
        setCommand(did, Fridge.getInstance(did).getCommand9(switchOnOff), clientId, token, callback);
    }

    //设置/关闭速冻， 只适用：冰箱iLive四门语音版（viomi.fridge.v2）；  462大屏金属门冰箱（viomi.fridge.v3）
    public void sendFridgeCommand10(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF switchOnOff) {
        setCommand(did, Fridge.getInstance(did).getCommand10(switchOnOff), clientId, token, callback);
    }

    //设置/关闭冰饮功能  只适用： 455大屏玻璃门冰箱（viomi.fridge.v4）
    public void sendFridgeCommand11(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF switchOnOff) {
        setCommand(did, Fridge.getInstance(did).getCommand11(switchOnOff), clientId, token, callback);
    }

    //设置滤芯寿命，单位小时
    public void sendFridgeCommand12(String did, String clientId, String token, ViomiDeviceCallback callback, int hour) {
        setCommand(did, Fridge.getInstance(did).getCommand12(hour), clientId, token, callback);
    }

    //设置城市  只适用：  冰箱ilive（viomi.fridge.u1）,  米家冰箱（viomi.fridge.w1）
    public void sendFridgeCommand13(String did, String clientId, String token, ViomiDeviceCallback callback, String city) {
        setCommand(did, Fridge.getInstance(did).getCommand13(city), clientId, token, callback);
    }


    /*
    *
    * 净水器控制
    *
    * */

    // 获取状态get_prop
    public void sendPurifierCommand0(String did, String clientId, String token, ViomiDeviceCallback callback, String[] args) {
        setCommand(did, Purifier.getInstance(did).getCommand0(args), clientId, token, callback);
    }

    // 净水器设置温度 40-90摄氏度
    public void sendPurifierCommand1(String did, String clientId, String token, ViomiDeviceCallback callback, int temperature) {
        setCommand(did, Purifier.getInstance(did).getCommand1(temperature), clientId, token, callback);
    }

    public void get_prop(String did, String clientId, String token, ViomiDeviceCallback callback, String... params) {
        setCommand(did, Purifier.getInstance(did).getProp(params), clientId, token, callback);
    }

    // 设置净水器出水/停止出水： 0是停水 1是出水 ；仅展会样机X5
    public void sendPurifierCommand2(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF switch_ON_OFF) {
        setCommand(did, Purifier.getInstance(did).getCommand2(switch_ON_OFF), clientId, token, callback);
    }

    //设置滤芯1 已经使用流量和已经使用时间
    public void sendPurifierCommand3(String did, String clientId, String token, ViomiDeviceCallback callback, int usedFlow, int usedTime) {
        setCommand(did, Purifier.getInstance(did).getCommand3(usedFlow, usedTime), clientId, token, callback);
    }

    //设置滤芯2 已经使用流量和已经使用时间
    public void sendPurifierCommand4(String did, String clientId, String token, ViomiDeviceCallback callback, int usedFlow, int usedTime) {
        setCommand(did, Purifier.getInstance(did).getCommand4(usedFlow, usedTime), clientId, token, callback);
    }

    //设置滤芯3 已经使用流量和已经使用时间
    public void sendPurifierCommand5(String did, String clientId, String token, ViomiDeviceCallback callback, int usedFlow, int usedTime) {
        setCommand(did, Purifier.getInstance(did).getCommand5(usedFlow, usedTime), clientId, token, callback);
    }


    //设置滤芯4 已经使用流量和已经使用时间
    public void sendPurifierCommand6(String did, String clientId, String token, ViomiDeviceCallback callback, int usedFlow, int usedTime) {
        setCommand(did, Purifier.getInstance(did).getCommand6(usedFlow, usedTime), clientId, token, callback);
    }

    //0 - 禁止TDS监测， 1 - 使能TDS监测
    public void sendPurifierCommand7(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF on_off) {
        setCommand(did, Purifier.getInstance(did).getCommand7(on_off), clientId, token, callback);
    }

    //设置系统当前时间 单位：ms
    public void sendPurifierCommand8(String did, String clientId, String token, ViomiDeviceCallback callback, long time) {
        setCommand(did, Purifier.getInstance(did).getCommand8(time), clientId, token, callback);
    }

    //设置水质指示状态阈值 小米净水器2
    public void sendPurifierCommand9(String did, String clientId, String token, ViomiDeviceCallback callback, int value) {
        setCommand(did, Purifier.getInstance(did).getCommand9(value), clientId, token, callback);
    }

    //设置出水流量 参数1表示面板第几个按钮，参数2表示出水流量 最大2000ml
    public void sendPurifierCommand10(String did, String clientId, String token, ViomiDeviceCallback callback, int order, int flow) {
        setCommand(did, Purifier.getInstance(did).getCommand10(order, flow), clientId, token, callback);
    }


    /*
    *
    * 烟机控制
    *
    * */

    // 获取状态get_prop
    public void sendCookerHoodCommand0(String did, String clientId, String token, ViomiDeviceCallback callback, String[] args) {
        setCommand(did, CookerHood.getInstance(did).getCommand0(args), clientId, token, callback);
    }

    public void sendCookerHoodGetUserData(String did, String clientId, String token, ViomiDeviceCallback callback) {
        try {
            JSONObject dataObj = new JSONObject();
            dataObj.put("did", did);
            dataObj.put("type", "store");
            dataObj.put("key", "life_record");
            dataObj.put("time_start", "0");
            dataObj.put("time_end", System.currentTimeMillis() / 1000);
            String url = Config.MIOTHOST_GETUSERDATA + "?data=" + dataObj.toString()
                    + "&clientId=" + clientId
                    + "&accessToken=" + token;
            HTTPUtil.getInstance().getRequst(url, callback);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //电源模式：0 - 关机 1 - 待机 2 - 开机 3 - 清洗 4 - 清洗复位
    public void sendCookerHoodCommand1(String did, String clientId, String token, ViomiDeviceCallback callback, HoodPowerMode mode) {
        setCommand(did, CookerHood.getInstance(did).getCommand1(mode), clientId, token, callback);
    }

    //抽风模式
    //mode0 - 低风OFF，mode1 - 低风ON
    //mode2 - 爆炒OFF，mode3 - 爆炒ON
    //mode4 - 高风OFF，mode5 - 高风ON
    public void sendCookerHoodCommand2(String did, String clientId, String token, ViomiDeviceCallback callback, HoodWindMode mode) {
        setCommand(did, CookerHood.getInstance(did).getCommand2(mode), clientId, token, callback);
    }

    //开关灯
    public void sendCookerHoodCommand3(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF on_off) {
        setCommand(did, CookerHood.getInstance(did).getCommand3(on_off), clientId, token, callback);
    }

    //    设置换新风
    //    arg0: 0 - Disable, 1 - Enable
    //    arg1：[100, 2000] 换新风阈值
    //    arg2: [0, 23] 换新风时间小时
    //    arg3: [0, 59] 换新风时间分钟
    //    arg4: [0, 65535] 换新风时长
    public void sendCookerHoodCommand4(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF on_off, int arg1, int arg2, int arg3, int arg4) {
        setCommand(did, CookerHood.getInstance(did).getCommand4(on_off, arg1, arg2, arg3, arg4), clientId, token, callback);
    }

    //不知道是什么鬼
    // 0:正常工作模式
    //1 ：绑定模式
    //2 ：主机升级模式
    //3:从机升级模式
    public void sendCookerHoodCommand5(String did, String clientId, String token, ViomiDeviceCallback callback, HoodBleMode mode) {
        setCommand(did, CookerHood.getInstance(did).getCommand5(mode), clientId, token, callback);
    }

    //0 - OFF, 1 - ON ( 0 - 关闭增压巡航 1 - 打开增压巡航 )
    public void sendCookerHoodCommand6(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF on_off) {
        setCommand(did, CookerHood.getInstance(did).getCommand6(on_off), clientId, token, callback);
    }

    //[0, 180] 延迟关机时间0-3分钟可调 (单位/s)
    public void sendCookerHoodCommand7(String did, String clientId, String token, ViomiDeviceCallback callback, int arg1) {
        setCommand(did, CookerHood.getInstance(did).getCommand7(arg1), clientId, token, callback);
    }

    //0 - OFF, 1 - ON ( 0 - 关机不关灯 1 - 关机关灯 )
    public void sendCookerHoodCommand8(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF on_off) {
        setCommand(did, CookerHood.getInstance(did).getCommand8(on_off), clientId, token, callback);
    }

    //arg1:定时时长（/min）
    public void sendCookerHoodCommand9(String did, String clientId, String token, ViomiDeviceCallback callback, int arg1) {
        setCommand(did, CookerHood.getInstance(did).getCommand9(arg1), clientId, token, callback);
    }


    /*
    *
    *
    * 管线机控制
    *
    * */


    // 获取状态get_prop
    public void sendPipelineMachineCommand0(String did, String clientId, String token, ViomiDeviceCallback callback, String[] args) {
        setCommand(did, PipelineMachine.getInstance(did).getCommand0(args), clientId, token, callback);
    }

    //第一个参数，序号，第几个按键；第二个参数，自定义温度
    public void sendPipelineMachineCommand1(String did, String clientId, String token, ViomiDeviceCallback callback, int order, int temp) {
        setCommand(did, PipelineMachine.getInstance(did).getCommand1(order, temp), clientId, token, callback);
    }

    //ms，设置系统时间，时间戳
    public void sendPipelineMachineCommand2(String did, String clientId, String token, ViomiDeviceCallback callback, long time) {
        setCommand(did, PipelineMachine.getInstance(did).getCommand2(time), clientId, token, callback);
    }

    //开启/关闭喝水提醒
    public void sendPipelineMachineCommand3(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF on_off) {
        setCommand(did, PipelineMachine.getInstance(did).getCommand3(on_off), clientId, token, callback);
    }


    //设定喝水提醒时间，单位小时
    public void sendPipelineMachineCommand4(String did, String clientId, String token, ViomiDeviceCallback callback, int hour) {
        setCommand(did, PipelineMachine.getInstance(did).getCommand4(hour), clientId, token, callback);
    }



    /*
    *
    * wifi水壶控制
    * 尚未上市 协议后期可能会改动
    *
    * */

    // 获取状态get_prop
    public void sendKettleCommand0(String did, String clientId, String token, ViomiDeviceCallback callback, String[] args) {
        setCommand(did, Kettle.getInstance(did).getCommand0(args), clientId, token, callback);
    }

    //设置保温温度  40-90℃
    public void sendKettleCommand1(String did, String clientId, String token, ViomiDeviceCallback callback, int temp) {
        setCommand(did, Kettle.getInstance(did).getCommand1(temp), clientId, token, callback);
    }

    //0：煮沸降至保温温度，防止重复煮沸关；1：煮沸降至保温温度，防止重复煮沸开；2：加热至保温温度
    public void sendKettleCommand2(String did, String clientId, String token, ViomiDeviceCallback callback, KettleWorkMode mode) {
        setCommand(did, Kettle.getInstance(did).getCommand2(mode), clientId, token, callback);
    }

    //保温时间,单位分钟
    public void sendKettleCommand3(String did, String clientId, String token, ViomiDeviceCallback callback, int holdetime) {
        setCommand(did, Kettle.getInstance(did).getCommand3(holdetime), clientId, token, callback);
    }

    //设置按键值：取值为：0x01:加热按键 0x02：保温按键，0xff：取消加热
    public void sendKettleCommand4(String did, String clientId, String token, ViomiDeviceCallback callback, KettleLuanchMode mode) {
        setCommand(did, Kettle.getInstance(did).getCommand4(mode), clientId, token, callback);
    }

    //设置预约时间和启动方式，{时间戳，启动方式}，例如{1485760495，1}，启动方式见set_key_value，0xff代表取消预约
    public void sendKettleCommand5(String did, String clientId, String token, ViomiDeviceCallback callback, long timeStamp, KettleLuanchMode mode) {
        setCommand(did, Kettle.getInstance(did).getCommand5(timeStamp, mode), clientId, token, callback);
    }



    /*
    *
    * 即热饮水吧
    *
    * */

    // 获取状态get_prop
    public void sendDrinkBarCommand0(String did, String clientId, String token, ViomiDeviceCallback callback, String[] args) {
        setCommand(did, DrinkBar.getInstance(did).getCommand0(args), clientId, token, callback);
    }

    //自定义温度
    public void sendDrinkBarCommand1(String did, String clientId, String token, ViomiDeviceCallback callback, int temp) {
        setCommand(did, DrinkBar.getInstance(did).getCommand1(temp), clientId, token, callback);
    }

    //ms，设置系统时间，时间戳
    public void sendDrinkBarCommand2(String did, String clientId, String token, ViomiDeviceCallback callback, long timeStamp) {
        setCommand(did, DrinkBar.getInstance(did).getCommand2(timeStamp), clientId, token, callback);
    }

    //开启/关闭喝水提醒，0不提醒；1提醒
    public void sendDrinkBarCommand3(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF on_off) {
        setCommand(did, DrinkBar.getInstance(did).getCommand3(on_off), clientId, token, callback);
    }

    //设定喝水提醒时间，单位小时
    public void sendDrinkBarCommand4(String did, String clientId, String token, ViomiDeviceCallback callback, int time) {
        setCommand(did, DrinkBar.getInstance(did).getCommand4(time), clientId, token, callback);
    }


    /*
    *
    * 洗碗机
    *
    * */

    // 获取状态get_prop
    public void sendDishwasherCommand0(String did, String clientId, String token, ViomiDeviceCallback callback, String[] args) {
        setCommand(did, Dishwasher.getInstance(did).getCommand0(args), clientId, token, callback);
    }

    //开关童锁功能
    public void sendDishwasherCommand1(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF on_off) {
        setCommand(did, Dishwasher.getInstance(did).getCommand1(on_off), clientId, token, callback);
    }


    //设置当前系统时间，单位s
    public void sendDishwasherCommand2(String did, String clientId, String token, ViomiDeviceCallback callback, long time) {
        setCommand(did, Dishwasher.getInstance(did).getCommand2(time), clientId, token, callback);
    }

    //设置洗涤程序，0 - 标准洗 1 - 经济洗 2 - 快速洗 3 - 自定义
    public void sendDishwasherCommand3(String did, String clientId, String token, ViomiDeviceCallback callback, WashProMode mode) {
        setCommand(did, Dishwasher.getInstance(did).getCommand3(mode), clientId, token, callback);
    }

    //0：暂停；1：启动
    public void sendDishwasherCommand4(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF on_off) {
        setCommand(did, Dishwasher.getInstance(did).getCommand4(on_off), clientId, token, callback);
    }

    //设置洗涤自定义模式，3 - 强力洗   4 - 预洗   5 - 玻璃洗    6 - 除菌
    public void sendDishwasherCommand5(String did, String clientId, String token, ViomiDeviceCallback callback, WashCusMode mode) {
        setCommand(did, Dishwasher.getInstance(did).getCommand5(mode), clientId, token, callback);
    }


    //设置预约洗时间参数并启动预约
    //arg1：预约洗开始的小时  （0~23）
    //arg2：预约洗开始的分钟  （0~59）
    public void sendDishwasherCommand6(String did, String clientId, String token, ViomiDeviceCallback callback, String hour, String min) {
        setCommand(did, Dishwasher.getInstance(did).getCommand6(hour, min), clientId, token, callback);
    }

    //0：取消预约；1：按设备上的保存的预约时间启动预约
    public void sendDishwasherCommand7(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF on_off) {
        setCommand(did, Dishwasher.getInstance(did).getCommand7(on_off), clientId, token, callback);
    }

    //开关自动冲洗干燥功能
    public void sendDishwasherCommand8(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF on_off) {
        setCommand(did, Dishwasher.getInstance(did).getCommand8(on_off), clientId, token, callback);
    }




    /*
    *
    * 洗衣机
    *
    * */

    // 获取状态get_prop
    public void sendWashingMachineCommand0(String did, String clientId, String token, ViomiDeviceCallback callback, String[] args) {
        setCommand(did, WashingMachine.getInstance(did).getCommand0(args), clientId, token, callback);
    }

    //开关童锁功能
    public void sendWashingMachineCommand1(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF on_off) {
        setCommand(did, WashingMachine.getInstance(did).getCommand1(on_off), clientId, token, callback);
    }

    //0：暂停；1：启动/恢复运行
    public void sendWashingMachineCommand2(String did, String clientId, String token, ViomiDeviceCallback callback, Switch_ON_OFF on_off) {
        setCommand(did, WashingMachine.getInstance(did).getCommand2(on_off), clientId, token, callback);
    }

    //设定洗涤程序，
    // 包括："goldenwash","drumclean","spin","antibacterial","super_quick","cottons","shirts","child_cloth",
    // "jeans", "wool", "down", "chiffon", "outdoor", "delicates", "underwears"，"rinse_spin","My Time","cotton Eco"。
    // 分别代表  “黄金洗”程序，“洁筒洗”程序，“单脱水”程序，“除菌螨”程序，“超快洗”程序，“棉织物”程序，
    // “衬衣”程序，“童衣洗”程序, “牛仔”程序，“羊毛”程序，“羽绒服”程序，“雪纺”程序，“运动服”程序，
    // “精细织物”程序，“内衣”程序，“漂+脱”程序，“自定义”程序，“棉Eco"程序
    public void sendWashingMachineCommand3(String did, String clientId, String token, ViomiDeviceCallback callback, WashMacProMode mode) {
        setCommand(did, WashingMachine.getInstance(did).getCommand3(mode), clientId, token, callback);
    }

    //设置水温 0, 30,40,60,90  (0代表常温）
    public void sendWashingMachineCommand4(String did, String clientId, String token, ViomiDeviceCallback callback, WashingTemp mode) {
        setCommand(did, WashingMachine.getInstance(did).getCommand4(mode), clientId, token, callback);
    }

    //设定漂洗次数，1~5次
    public void sendWashingMachineCommand5(String did, String clientId, String token, ViomiDeviceCallback callback, WashingTimes times) {
        setCommand(did, WashingMachine.getInstance(did).getCommand5(times), clientId, token, callback);
    }


    //设定脱水强度，取值"none","gentle","mild","middle","strong"。分别代表  不脱水，柔脱水，轻脱水，中脱水，强脱水
    public void sendWashingMachineCommand6(String did, String clientId, String token, ViomiDeviceCallback callback, WashingDryRmpMode mode) {
        setCommand(did, WashingMachine.getInstance(did).getCommand6(mode), clientId, token, callback);
    }

    //设定是否自动添加洗涤剂，0：不投放；1：少量；2：正常；3：多量
    public void sendWashingMachineCommand7(String did, String clientId, String token, ViomiDeviceCallback callback, WashingDetergentMode mode) {
        setCommand(did, WashingMachine.getInstance(did).getCommand7(mode), clientId, token, callback);
    }

    //设定是否自动添加柔顺剂，0：不投放；1：少量；2：正常；3：多量
    public void sendWashingMachineCommand8(String did, String clientId, String token, ViomiDeviceCallback callback, WashingSoftenerMode mode) {
        setCommand(did, WashingMachine.getInstance(did).getCommand8(mode), clientId, token, callback);
    }

    //设定预约时间，单位小时，如3表示预约3小时后洗涤完成，0代表取消预约
    public void sendWashingMachineCommand9(String did, String clientId, String token, ViomiDeviceCallback callback, int hour) {
        setCommand(did, WashingMachine.getInstance(did).getCommand9(hour), clientId, token, callback);
    }

    public void setMethod(String did, String clientId, String token, ViomiDeviceCallback callback, String method, String... params) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", method);
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            if (params != null) {
                for (String s : params) {
                    jsonArray.put(s);
                }
            }
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setCommand(did, jsonObject.toString(), clientId, token, callback);
    }


    /*
    *
    * 命令发送
    *
    * */
    public void setCommand(String did, String agreement, String clientId, String token, ViomiDeviceCallback callback) {
        String url = Config.MIOTHOST + did + "?data=" + agreement + "&clientId=" + clientId + "&accessToken=" + token;
        // get请求
        HTTPUtil.getInstance().getRequst(url, callback);
    }

}