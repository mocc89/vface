package viomi.com.viomiconnectsdk.viomiconnect.protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.FridgeMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.FridgeTempMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_OFF;

/**
 * Created by Mocc on 2017/9/18
 */

//冰箱
public class Fridge {

    private volatile static Fridge instance;
    private volatile static String did;
    private int id = 1;

    public static Fridge getInstance(String mDid) {
        did = mDid;
        if (instance == null) {
            synchronized (Fridge.class) {
                if (instance == null) {
                    instance = new Fridge();
                }
            }
        }
        return instance;
    }

    private Fridge() {

    }


    // 获取状态get_prop
    public String getCommand0(String[] args) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            if (args != null) {
                for (int i = 0; i < args.length; i++) {
                    jsonArray.put(i, args[i]);
                }
            }
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    // 1.打开/关闭 一键净化
    public String getCommand1(Switch_ON_OFF switchOnOff) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "setOneKeyClean");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(switchOnOff.toString());
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    // 打开/关闭 冷藏室
    public String getCommand2(Switch_ON_OFF switchOnOff) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "setRCSet");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(switchOnOff.toString());
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    // 打开/关闭 变温室
    public String getCommand3(Switch_ON_OFF switchOnOff) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "setCCSet");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(switchOnOff.toString());
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    // 设置冰箱模式
    // "smart",""holiday","quick_cold","quick_freeze","humid"。
    // 分别代表智能模式，假日模式，速冷模式，速冻模式，加湿模式
    public String getCommand4(FridgeMode mode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "setMode");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(mode.toString());
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    // 设定冷藏室温度，[（-18）-8]，默认6
    public String getCommand5(int temperature) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "setRCSetTemp");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(temperature);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    // 设定变温室温度，（-18）-8，默认4
    public String getCommand6(int temperature) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "setCCSetTemp");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(temperature);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    // 设定冷冻室温度，（-25）-（-15），默认-18
    public String getCommand7(int temperature) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "setFCSetTemp");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(temperature);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    // 设置变温室场景模式 只适用于大屏
    public String getCommand8(FridgeTempMode mode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "setSceneName");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(mode.toString());
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    //开启/关闭速冷  只适用：冰箱iLive四门语音版（viomi.fridge.v2）；  462大屏金属门冰箱（viomi.fridge.v3）
    public String getCommand9(Switch_ON_OFF switchOnOff) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "setSmartCool");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(switchOnOff.toString());
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设置/关闭速冻， 只适用：冰箱iLive四门语音版（viomi.fridge.v2）；  462大屏金属门冰箱（viomi.fridge.v3）
    public String getCommand10(Switch_ON_OFF switchOnOff) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "setSmartFreeze");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(switchOnOff.toString());
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设置/关闭冰饮功能  只适用： 455大屏玻璃门冰箱（viomi.fridge.v4）
    public String getCommand11(Switch_ON_OFF switchOnOff) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "setCoolBeverage");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(switchOnOff.toString());
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    //设置滤芯寿命，单位小时
    public String getCommand12(int hour) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "setFilterLife");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(hour);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设置城市  只适用：  冰箱ilive（viomi.fridge.u1）,  米家冰箱（viomi.fridge.w1）
    public String getCommand13(String city) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "setCity");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(city);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

}
