package viomi.com.viomiconnectsdk.viomiconnect.protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_OFF;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashMacProMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashingDetergentMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashingDryRmpMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashingSoftenerMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashingTemp;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashingTimes;

/**
 * Created by Mocc on 2017/12/30
 */


//洗衣机
public class WashingMachine {

    private volatile static WashingMachine instance;
    private volatile static String did;
    private int id = 1;

    public static WashingMachine getInstance(String mDid) {
        did = mDid;
        if (instance == null) {
            synchronized (WashingMachine.class) {
                if (instance == null) {
                    instance = new WashingMachine();
                }
            }
        }
        return instance;
    }

    private WashingMachine() {

    }


    // 获取状态get_prop
    public String getCommand0(String[] args) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            if (args != null) {
                for (int i = 0; i < args.length; i++) {
                    jsonArray.put(i, args[i]);
                }
            }
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //开关童锁功能
    public String getCommand1(Switch_ON_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_child_lock");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //0：暂停；1：启动/恢复运行
    public String getCommand2(Switch_ON_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_wash_action");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设定洗涤程序，
    // 包括："goldenwash","drumclean","spin","antibacterial","super_quick","cottons","shirts","child_cloth",
    // "jeans", "wool", "down", "chiffon", "outdoor", "delicates", "underwears"，"rinse_spin","My Time","cotton Eco"。
    // 分别代表  “黄金洗”程序，“洁筒洗”程序，“单脱水”程序，“除菌螨”程序，“超快洗”程序，“棉织物”程序，
    // “衬衣”程序，“童衣洗”程序, “牛仔”程序，“羊毛”程序，“羽绒服”程序，“雪纺”程序，“运动服”程序，
    // “精细织物”程序，“内衣”程序，“漂+脱”程序，“自定义”程序，“棉Eco"程序
    public String getCommand3(WashMacProMode mode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_wash_program");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(mode.value);

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设置水温 0, 30,40,60,90  (0代表常温）
    public String getCommand4(WashingTemp mode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_wash_temp");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(mode.vanule);

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设定漂洗次数，1~5次
    public String getCommand5(WashingTimes times) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_rinse_time");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(times.value);

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设定脱水强度，取值"none","gentle","mild","middle","strong"。分别代表  不脱水，柔脱水，轻脱水，中脱水，强脱水
    public String getCommand6(WashingDryRmpMode mode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_spin_level");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(mode.name());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设定是否自动添加洗涤剂，0：不投放；1：少量；2：正常；3：多量
    public String getCommand7(WashingDetergentMode mode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_detergent");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(mode.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设定是否自动添加柔顺剂，0：不投放；1：少量；2：正常；3：多量
    public String getCommand8(WashingSoftenerMode mode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_softener");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(mode.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设定预约时间，单位小时，如3表示预约3小时后洗涤完成，0代表取消预约
    public String getCommand9(int hour) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_appoint_time");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(hour);

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }
}
