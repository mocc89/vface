package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2017/9/18
 */

//"smart",""holiday","quick_cold","quick_freeze","humid"。
// 分别代表 智能模式，假日模式，速冷模式，速冻模式，加湿模式
public enum FridgeMode {
    smart("智能"), holiday("假日"), quick_cold("速冷"), quick_freeze("速冻"), humid("加湿"), none("无");
    private String mode;

    FridgeMode(String mode) {
        this.mode = mode;
    }

    public String getName() {
        return mode;
    }

    public void setName(String mode) {
        this.mode = mode;
    }
}
